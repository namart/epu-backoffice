package at.namart.model;

import java.util.ArrayList;

import at.namart.lib.Model;
import at.namart.lib.NotiEvent;
import at.namart.model.bo.Projekt;
import at.namart.model.bo.Zeit;
import at.namart.model.services.DALException;
import at.namart.model.services.DALFactory;
import at.namart.model.services.IDALZeiterfassung;

public class ZeiterfassungModel extends Model implements IDALZeiterfassung {
	
	//Notify Names
	public final String GET_LIST = "getList";
	public final String ITEM_CHANGE = "itemChange";
	public final String DELETE_ITEM = "deleteItem";
	public final String PROJEKT_LIST = "projektList";
	
	//SingletonClass
	private static final ZeiterfassungModel instance = new ZeiterfassungModel();
	public static ZeiterfassungModel getInstance() {
		return instance;
	}

	@Override
	public ArrayList<Zeit> getZeiterfassungList() throws DALException {
		notifyView( new NotiEvent( DALFactory.getDAL().getZeiterfassungList(), this.GET_LIST) );
		return null;
	}

	@Override
	public Zeit saveZeiterfassung(Zeit z) throws DALException {
		notifyView( new NotiEvent( DALFactory.getDAL().saveZeiterfassung(z), this.ITEM_CHANGE) );
		return null;
	}

	@Override
	public boolean deleteZeiterfassung(Zeit z) throws DALException {
		if(DALFactory.getDAL().deleteZeiterfassung(z)) {
			notifyView( new NotiEvent( z, this.DELETE_ITEM) );
			return true;
		} else {
			return false;
		}
	}
	
	public ArrayList<Projekt> getProjektList() throws DALException {
		notifyView( new NotiEvent( DALFactory.getDAL().getProjektList(), this.PROJEKT_LIST) );
		return null;
	}

}
