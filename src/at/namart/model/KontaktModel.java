package at.namart.model;

import java.util.ArrayList;

import at.namart.lib.Model;
import at.namart.lib.NotiEvent;
import at.namart.lib.logger.Logger;
import at.namart.model.bo.Kontakt;
import at.namart.model.services.DALException;
import at.namart.model.services.DALFactory;
import at.namart.model.services.IDALKontakt;

public class KontaktModel extends Model implements IDALKontakt {
	
	//Notify Names
	public final String GET_LIST = "getList";
	public final String ITEM_CHANGE = "itemChange";
	public final String DELETE_ITEM = "deleteItem";
	
	//SingletonClass
	private static final KontaktModel instance = new KontaktModel();
	public static KontaktModel getInstance() {
		return instance;
	}

	@Override
	public ArrayList<Kontakt> getKontaktList() throws DALException {
		notifyView( new NotiEvent( DALFactory.getDAL().getKontaktList(), this.GET_LIST) );
		return null;
	}

	@Override
	public Kontakt saveKontakt(Kontakt k) throws DALException {
		notifyView( new NotiEvent( DALFactory.getDAL().saveKontakt(k), this.ITEM_CHANGE) );
		return null;
	}

	@Override
	public boolean deleteKontakt(Kontakt k) throws DALException {
		if(DALFactory.getDAL().deleteKontakt(k)) {
			notifyView( new NotiEvent( k, this.DELETE_ITEM) );
			return true;
		} else {
			return false;
		}
	}

}
