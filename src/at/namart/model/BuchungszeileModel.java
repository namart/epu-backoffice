package at.namart.model;

import java.util.ArrayList;

import at.namart.lib.Model;
import at.namart.lib.NotiEvent;
import at.namart.model.bo.Buchungszeile;
import at.namart.model.bo.Kategorie;
import at.namart.model.services.DALException;
import at.namart.model.services.DALFactory;
import at.namart.model.services.IDALBuchungszeile;

public class BuchungszeileModel extends Model implements IDALBuchungszeile {

	//Notify Names
	public final String GET_LIST = "getList";
	public final String ITEM_CHANGE = "itemChange";
	public final String DELETE_ITEM = "deleteItem";
	public final String KATEGORIEN_CHANGE = "KategorienChange";
	
	private ArrayList<Kategorie> cacheKategorien;
	
	//SingletonClass
	private static final BuchungszeileModel instance = new BuchungszeileModel();
	public static BuchungszeileModel getInstance() {
		return instance;
	}
	
	@Override
	public ArrayList<Buchungszeile> getBuchungszeileList() throws DALException {
		notifyView( new NotiEvent( DALFactory.getDAL().getBuchungszeileList(), this.GET_LIST) );
		return null;
	}

	@Override
	public Buchungszeile saveBuchungszeile(Buchungszeile b) throws DALException {
		notifyView( new NotiEvent( DALFactory.getDAL().saveBuchungszeile(b), this.ITEM_CHANGE) );
		return null;
	}

	@Override
	public boolean deleteBuchungszeile(Buchungszeile b) throws DALException {
		if(DALFactory.getDAL().deleteBuchungszeile(b)) {
			notifyView( new NotiEvent( b, this.DELETE_ITEM) );
			return true;
		} else {
			return false;
		}
	}
	
	// Gibt die Einheiten zur�ck
	@Override
	public ArrayList<Kategorie> getKategorien() throws DALException {
		
		if(this.cacheKategorien == null) {
            this.cacheKategorien = DALFactory.getDAL().getKategorien();
        }
		notifyView( new NotiEvent( this.cacheKategorien, this.KATEGORIEN_CHANGE) );
		return null;	
	}

}
