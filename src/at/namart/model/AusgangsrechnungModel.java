package at.namart.model;

import java.util.ArrayList;

import at.namart.lib.Model;
import at.namart.lib.NotiEvent;
import at.namart.lib.logger.Logger;
import at.namart.lib.pdfs.AusgangsrechnungPdf;
import at.namart.model.bo.Angebot;
import at.namart.model.bo.Ausgangsrechnung;
import at.namart.model.bo.Kunde;
import at.namart.model.bo.Rechnungszeile;
import at.namart.model.services.DALException;
import at.namart.model.services.DALFactory;
import at.namart.model.services.IDALAusgangsrechnung;

public class AusgangsrechnungModel extends Model implements IDALAusgangsrechnung {
	
	//Notify Names
	public final String GET_LIST = "getList";
	public final String ITEM_CHANGE = "itemChange";
	public final String DELETE_ITEM = "deleteItem";
	public final String KUNDE_LIST = "kundeList";
	
	//SingletonClass
	private static final AusgangsrechnungModel instance = new AusgangsrechnungModel();
	public static AusgangsrechnungModel getInstance() {
		return instance;
	}

	@Override
	public ArrayList<Ausgangsrechnung> getAusgangsrechnungList() throws DALException {
		notifyView( new NotiEvent( DALFactory.getDAL().getAusgangsrechnungList(), this.GET_LIST) );
		return null;
	}

	@Override
	public Ausgangsrechnung saveAusgangsrechnung(Ausgangsrechnung a) throws DALException {
		notifyView( new NotiEvent( DALFactory.getDAL().saveAusgangsrechnung(a), this.ITEM_CHANGE) );
		return null;
	}

	@Override
	public boolean deleteAusgangsrechnung(Ausgangsrechnung a) throws DALException {
		if(DALFactory.getDAL().deleteAusgangsrechnung(a)) {
			notifyView( new NotiEvent( a, this.DELETE_ITEM) );
			return true;
		} else {
			return false;
		}
	}
	
	public ArrayList<Kunde> getKundeList() throws DALException {
		notifyView( new NotiEvent( DALFactory.getDAL().getKundeList(), this.KUNDE_LIST) );
		return null;
	}
	
	/*
	 * Ausgangsrechnung PDF erstellen
	 * */
	public void createPdf(Ausgangsrechnung a) throws DALException, Exception {
		
		if(a.getId() == -1){
			throw new Exception("W�hle eine Ausgangsrechnung aus um ein PDF davon zu erstellen");
		}
		
		ArrayList<Angebot> rz = DALFactory.getDAL().getRechnungszeileAngebotByIdList(a.getId());
		
		AusgangsrechnungPdf pdf = new AusgangsrechnungPdf(a);
		pdf.createRechnungenTable(a, rz);
		
	}

}
