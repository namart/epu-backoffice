package at.namart.model.bo;

import at.namart.lib.Model;

public class Eingangsrechnung {

	private int id = Model.INVALIDID;
	private String name;
	private int KontaktId = Model.UNDEFINED;
	
	public Eingangsrechnung(int id, String name, int kontaktId) {
		super();
		this.id = id;
		this.name = name;
		KontaktId = kontaktId;
	}
	
	public Eingangsrechnung() {
		
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getKontaktId() {
		return KontaktId;
	}

	public void setKontaktId(int kontaktId) {
		KontaktId = kontaktId;
	}
	
	

}
