package at.namart.model.bo;

import at.namart.lib.Model;

public class Projekt {

	private int id = Model.INVALIDID;
	private String name;
	
	public Projekt() {
		
	}
	
	public Projekt(int id, String name) {
		this.id = id;
		this.name = name;
	}
	
	public String toString() {
        return name;
    }
	
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
}
