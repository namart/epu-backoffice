package at.namart.model.bo;

import at.namart.lib.Model;


public class Kunde {

	private int id = Model.INVALIDID;
	private String name;
	private int uid;
	private int kontaktId = Model.UNDEFINED;

	public Kunde(int id, String name, int uid, int kontakt_id) {
		super();
		this.id = id;
		this.name = name;
		this.uid = uid;
		this.kontaktId = kontakt_id;
	}
	
	public Kunde() {

	}
	
	public String toString() {
        return name;
    }
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getUid() {
		return uid;
	}
	public void setUid(int uid) {
		this.uid = uid;
	}
	
	public int getKontaktId() {
		return kontaktId;
	}

	public void setKontaktId(int kontaktId) {
		this.kontaktId = kontaktId;
	}
}
