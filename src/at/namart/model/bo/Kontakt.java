package at.namart.model.bo;

import at.namart.lib.Model;

public class Kontakt {
	
	private int id = Model.INVALIDID;
	private String vorname;
	private String nachname;
	private String adresse;
	private int telefon;
	private String email;
	
	public Kontakt(int id, String vorname, String nachname, String adresse, int telefon, String email) {
		super();
		this.id = id;
		this.vorname = vorname;
		this.nachname = nachname;
		this.adresse = adresse;
		this.telefon = telefon;
		this.email = email;
	}
	
	public Kontakt() {
		
	}
	
	public String toString() {
        return vorname + " " + nachname;
    }
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getVorname() {
		return vorname;
	}
	public void setVorname(String vorname) {
		this.vorname = vorname;
	}
	public String getNachname() {
		return nachname;
	}
	public void setNachname(String nachname) {
		this.nachname = nachname;
	}
	public String getAdresse() {
		return adresse;
	}
	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}
	public int getTelefon() {
		return telefon;
	}
	public void setTelefon(int telefon) {
		this.telefon = telefon;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}

}
