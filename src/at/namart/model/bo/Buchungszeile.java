package at.namart.model.bo;

import java.util.ArrayList;

import at.namart.lib.Model;

public class Buchungszeile {

	private int id = Model.INVALIDID;
	private String name;
	private float betrag;
	private ArrayList<Integer> KategorienIds;
	
	public Buchungszeile(int id, String name, float betrag) {
		super();
		this.id = id;
		this.name = name;
		this.betrag = betrag;
	}
	
	public Buchungszeile() {}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public float getBetrag() {
		return betrag;
	}

	public void setBetrag(float betrag) {
		this.betrag = betrag;
	}

	public ArrayList<Integer> getKategorienIds() {
		return KategorienIds;
	}

	public void setKategorienIds(ArrayList<Integer> KategorienIds) {
		this.KategorienIds = KategorienIds;
	}
	
	
}
