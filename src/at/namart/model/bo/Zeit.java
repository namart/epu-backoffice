package at.namart.model.bo;

import java.sql.Date;

import at.namart.lib.Model;

public class Zeit {

	private int id = Model.INVALIDID;
	private int dauer;
	private Date datum;
	private int ProjektId = Model.UNDEFINED;
	
	public Zeit(int id, int dauer, Date datum, int projektId) {
		super();
		this.id = id;
		this.dauer = dauer;
		this.datum = datum;
		ProjektId = projektId;
	}
	
	public Zeit() {}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getDauer() {
		return dauer;
	}

	public void setDauer(int dauer) {
		this.dauer = dauer;
	}

	public Date getDatum() {
		return datum;
	}

	public void setDatum(Date datum) {
		this.datum = datum;
	}

	public int getProjektId() {
		return ProjektId;
	}

	public void setProjektId(int projektId) {
		ProjektId = projektId;
	}
	
	
}
