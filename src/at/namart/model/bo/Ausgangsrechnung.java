package at.namart.model.bo;

import at.namart.lib.Model;

public class Ausgangsrechnung {

	private int id = Model.INVALIDID;
	private int KundenId = Model.UNDEFINED;
	private String name;

	public Ausgangsrechnung(int id, String name, int kundenId) {
		super();
		this.id = id;
		this.name = name;
		KundenId = kundenId;
	}
	
	public Ausgangsrechnung() {
		
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getKundenId() {
		return KundenId;
	}
	public void setKundenId(int kundenId) {
		KundenId = kundenId;
	}

}
