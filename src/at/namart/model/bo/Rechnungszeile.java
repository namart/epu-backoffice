package at.namart.model.bo;

import at.namart.lib.Model;

public class Rechnungszeile {

	private int id = Model.INVALIDID;
	private String name;
	private int AngebotId = Model.UNDEFINED;
	private int AusgangsrechnungId = Model.UNDEFINED;

	public Rechnungszeile(int id, String name, int angebotId, int ausgangsrechnungId) {

		this.id = id;
		this.name = name;
		this.AngebotId = angebotId;
		this.AusgangsrechnungId = ausgangsrechnungId;
	}
	
	public Rechnungszeile() {
		
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getAngebotId() {
		return AngebotId;
	}

	public void setAngebotId(int angebotId) {
		AngebotId = angebotId;
	}
	
	public int getAusgangsrechnungId() {
		return AusgangsrechnungId;
	}

	public void setAusgangsrechnungId(int ausgangsrechnungId) {
		AusgangsrechnungId = ausgangsrechnungId;
	}
}
