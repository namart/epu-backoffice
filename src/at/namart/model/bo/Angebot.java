package at.namart.model.bo;

import java.sql.Date;

import at.namart.lib.Model;

public class Angebot {
	
	private int id = Model.INVALIDID;
	private String name;
	private int summe;
	private int dauer;
	private Date datum;
	private int chance;
	private int ProjektId = Model.UNDEFINED;
	private int KundenId = Model.UNDEFINED;
	
	public Angebot() {
	}
	
	public Angebot(int id, String name, int summe, int dauer, Date datum, int chance, int projektId, int kundenId) {
		this.id = id;
		this.name = name;
		this.summe = summe;
		this.dauer = dauer;
		this.datum = datum;
		this.chance = chance;
		this.ProjektId = projektId;
		this.KundenId = kundenId;
	}
	
	public String toString() {
        return name;
    }
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getSumme() {
		return summe;
	}
	public void setSumme(int summe) {
		this.summe = summe;
	}
	public int getDauer() {
		return dauer;
	}
	public void setDauer(int dauer) {
		this.dauer = dauer;
	}
	public Date getDatum() {
		return datum;
	}
	public void setDatum(Date datum) {
		this.datum = datum;
	}
	public int getChance() {
		return chance;
	}
	public void setChance(int chance) {
		this.chance = chance;
	}
	public int getProjektId() {
		return ProjektId;
	}
	public void setProjektId(int projektId) {
		ProjektId = projektId;
	}
	public int getKundenId() {
		return KundenId;
	}
	public void setKundenId(int kundenId) {
		KundenId = kundenId;
	}
	
	
}
