package at.namart.model.bo;


public class Kategorie {
	
	private int id;
	private String name;
	
	public Kategorie(int id, String name) {
		super();
		this.id = id;
		this.name = name;
	}
	
	public Kategorie() {}
	
	public String toString() {
        return name;
    }
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

}
