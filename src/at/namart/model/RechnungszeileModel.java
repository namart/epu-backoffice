package at.namart.model;

import java.util.ArrayList;

import at.namart.lib.Model;
import at.namart.lib.NotiEvent;
import at.namart.lib.logger.Logger;
import at.namart.model.bo.Kontakt;
import at.namart.model.bo.Rechnungszeile;
import at.namart.model.services.DALException;
import at.namart.model.services.DALFactory;
import at.namart.model.services.IDALRechnungszeile;

public class RechnungszeileModel extends Model implements IDALRechnungszeile {
	
	//Notify Names
	public final String GET_LIST = "getList";
	public final String ITEM_CHANGE = "itemChange";
	public final String DELETE_ITEM = "deleteItem";
	public final String ANGEBOT_LIST = "angebotList";
	
	//SingletonClass
	private static final RechnungszeileModel instance = new RechnungszeileModel();
	public static RechnungszeileModel getInstance() {
		return instance;
	}

	@Override
	public ArrayList<Rechnungszeile> getRechnungszeileByIdList(int AusgangsrechnungsId) throws DALException {
		Logger.debug(AusgangsrechnungsId);
		notifyView( new NotiEvent( DALFactory.getDAL().getRechnungszeileByIdList(AusgangsrechnungsId), this.GET_LIST) );
		return null;
	}

	@Override
	public Rechnungszeile saveRechnungszeile(Rechnungszeile a) throws DALException {
		Logger.debug(a.getAusgangsrechnungId());
		notifyView( new NotiEvent( DALFactory.getDAL().saveRechnungszeile(a), this.ITEM_CHANGE) );
		return null;
	}

	@Override
	public boolean deleteRechnungszeile(Rechnungszeile a) throws DALException {
		if(DALFactory.getDAL().deleteRechnungszeile(a)) {
			notifyView( new NotiEvent( a, this.DELETE_ITEM) );
			return true;
		} else {
			return false;
		}
	}
	
	public ArrayList<Kontakt> getAngebotList() throws DALException {
		notifyView( new NotiEvent( DALFactory.getDAL().getAngebotList(), this.ANGEBOT_LIST) );
		return null;
	}

}
