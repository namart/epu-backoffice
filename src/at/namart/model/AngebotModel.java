package at.namart.model;

import java.util.ArrayList;

import at.namart.lib.Model;
import at.namart.lib.NotiEvent;
import at.namart.lib.logger.Logger;
import at.namart.model.bo.Angebot;
import at.namart.model.bo.Kunde;
import at.namart.model.bo.Projekt;
import at.namart.model.services.DALException;
import at.namart.model.services.DALFactory;
import at.namart.model.services.IDALAngebot;

public class AngebotModel extends Model implements IDALAngebot {

	//Notify Names
	public final String GET_LIST = "getList";
	public final String ITEM_CHANGE = "itemChange";
	public final String DELETE_ITEM = "deleteItem";
	public final String PROJEKT_LIST = "projektList";
	public final String KUNDE_LIST = "angebotList";
	
	//SingletonClass
	private static final AngebotModel instance = new AngebotModel();
	public static AngebotModel getInstance() {
		return instance;
	}
	
	@Override
	public ArrayList<Angebot> getAngebotList() throws DALException {
		notifyView( new NotiEvent( DALFactory.getDAL().getAngebotList(), this.GET_LIST) );
		return null;
	}
	@Override
	public Angebot saveAngebot(Angebot a) throws DALException {
		notifyView( new NotiEvent( DALFactory.getDAL().saveAngebot(a), this.ITEM_CHANGE) );
		return null;
	}
	@Override
	public boolean deleteAngebot(Angebot a) throws DALException {
		if(DALFactory.getDAL().deleteAngebot(a)) {
			notifyView( new NotiEvent( a, this.DELETE_ITEM) );
			return true;
		} else {
			return false;
		}
	}
	
	public ArrayList<Projekt> getProjektList() throws DALException {
		notifyView( new NotiEvent( DALFactory.getDAL().getProjektList(), this.PROJEKT_LIST) );
		return null;
	}
	
	public ArrayList<Kunde> getKundeList() throws DALException {
		notifyView( new NotiEvent( DALFactory.getDAL().getKundeList(), this.KUNDE_LIST) );
		return null;
	}
}
