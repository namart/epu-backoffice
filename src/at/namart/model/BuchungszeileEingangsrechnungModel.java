package at.namart.model;

import java.util.ArrayList;

import at.namart.lib.Model;
import at.namart.lib.NotiEvent;
import at.namart.model.bo.Eingangsrechnung;
import at.namart.model.services.DALException;
import at.namart.model.services.DALFactory;
import at.namart.model.services.IDALBuchungszeilenEingangsrechnung;

public class BuchungszeileEingangsrechnungModel extends Model implements IDALBuchungszeilenEingangsrechnung{

	//Notify Names
	public final String GET_LIST = "getList";
	public final String ITEM_CHANGE = "itemChange";
	public final String DELETE_ITEM = "deleteItem";
	public final String GET_EINGANGSRECHNUNGLIST = "getEingangsrechnungList";
	
	//SingletonClass
	private static final BuchungszeileEingangsrechnungModel instance = new BuchungszeileEingangsrechnungModel();
	public static BuchungszeileEingangsrechnungModel getInstance() {
		return instance;
	}
	
	@Override
	public ArrayList<Eingangsrechnung> getBuchungszeileEingangsrechnungList(int buchungszeile_id) throws DALException {
		notifyView( new NotiEvent( DALFactory.getDAL().getBuchungszeileEingangsrechnungList(buchungszeile_id), this.GET_LIST) );
		return null;
	}

	@Override
	public Eingangsrechnung saveBuchungszeileEingangsrechnung(int buchungszeile_id, Eingangsrechnung e) throws DALException {
		notifyView( new NotiEvent( DALFactory.getDAL().saveBuchungszeileEingangsrechnung(buchungszeile_id, e), this.ITEM_CHANGE) );
		
		//notify Auswahlmenu the change
		this.getNotUsedEingangsrechnungList(buchungszeile_id);
		return null;
	}

	@Override
	public boolean deleteBuchungszeileEingangsrechnung(int buchungszeile_id, Eingangsrechnung e) throws DALException {
		if(DALFactory.getDAL().deleteBuchungszeileEingangsrechnung(buchungszeile_id, e)) {
			notifyView( new NotiEvent( e, this.DELETE_ITEM) );
			
			//notify Auswahlmenu the change
			this.getNotUsedEingangsrechnungList(buchungszeile_id);
			return true;
		} else {
			return false;
		}
	}

	@Override
	public ArrayList<Eingangsrechnung> getNotUsedEingangsrechnungList(int buchungszeile_id) throws DALException {
		notifyView( new NotiEvent( DALFactory.getDAL().getNotUsedEingangsrechnungList(buchungszeile_id), this.GET_EINGANGSRECHNUNGLIST) );
		return null;
	}

}
