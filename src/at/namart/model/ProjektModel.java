package at.namart.model;

import java.util.ArrayList;

import at.namart.lib.Model;
import at.namart.lib.NotiEvent;
import at.namart.model.bo.Projekt;
import at.namart.model.services.DALException;
import at.namart.model.services.DALFactory;
import at.namart.model.services.IDALProjekt;

public class ProjektModel extends Model implements IDALProjekt {
	
	//Notify Names
	public final String GET_LIST = "getList";
	public final String ITEM_CHANGE = "itemChange";
	public final String DELETE_ITEM = "deleteItem";
	
	//SingletonClass
	private static final ProjektModel instance = new ProjektModel();
	public static ProjektModel getInstance() {
		return instance;
	}

	@Override
	public ArrayList<Projekt> getProjektList() throws DALException {
		notifyView( new NotiEvent( DALFactory.getDAL().getProjektList(), this.GET_LIST) );
		return null;
	}

	@Override
	public Projekt saveProjekt(Projekt p) throws DALException {	
		notifyView( new NotiEvent( DALFactory.getDAL().saveProjekt(p), this.ITEM_CHANGE) );
		return null;
	}

	@Override
	public boolean deleteProjekt(Projekt p) throws DALException {
		if(DALFactory.getDAL().deleteProjekt(p)) {
			notifyView( new NotiEvent( p, this.DELETE_ITEM) );
			return true;
		} else {
			return false;
		}
		
	}

}
