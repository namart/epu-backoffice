package at.namart.model;

import java.util.ArrayList;

import at.namart.lib.Model;
import at.namart.lib.NotiEvent;
import at.namart.lib.logger.Logger;
import at.namart.model.bo.Kontakt;
import at.namart.model.bo.Kunde;
import at.namart.model.bo.Projekt;
import at.namart.model.services.DALException;
import at.namart.model.services.DALFactory;
import at.namart.model.services.IDALKunde;

public class KundeModel extends Model implements IDALKunde {
	
	//Notify Names
	public final String GET_LIST = "getList";
	public final String ITEM_CHANGE = "itemChange";
	public final String DELETE_ITEM = "deleteItem";
	public final String KONTAKT_LIST = "kontaktList";
	
	//SingletonClass
	private static final KundeModel instance = new KundeModel();
	public static KundeModel getInstance() {
		return instance;
	}

	@Override
	public ArrayList<Kunde> getKundeList() throws DALException {
		notifyView( new NotiEvent( DALFactory.getDAL().getKundeList(), this.GET_LIST) );
		return null;
	}

	@Override
	public Kunde saveKunde(Kunde k) throws DALException {
		Logger.debug(k.getKontaktId());
		notifyView( new NotiEvent( DALFactory.getDAL().saveKunde(k), this.ITEM_CHANGE) );
		return null;
	}

	@Override
	public boolean deleteKunde(Kunde k) throws DALException {
		if(DALFactory.getDAL().deleteKunde(k)) {
			notifyView( new NotiEvent( k, this.DELETE_ITEM) );
			return true;
		} else {
			return false;
		}
	}
	
	public ArrayList<Kontakt> getKontaktList() throws DALException {
		notifyView( new NotiEvent( DALFactory.getDAL().getKontaktList(), this.KONTAKT_LIST) );
		return null;
	}

}
