package at.namart.model;

import java.util.ArrayList;

import at.namart.lib.Model;
import at.namart.lib.NotiEvent;
import at.namart.lib.logger.Logger;
import at.namart.model.bo.Ausgangsrechnung;
import at.namart.model.services.DALException;
import at.namart.model.services.DALFactory;
import at.namart.model.services.IDALBuchungszeilenAusgangsrechnung;

public class BuchungszeileAusgangsrechnungModel extends Model implements IDALBuchungszeilenAusgangsrechnung {
	
	//Notify Names
	public final String GET_LIST = "getList";
	public final String ITEM_CHANGE = "itemChange";
	public final String DELETE_ITEM = "deleteItem";
	public final String GET_AUSGANGSRECHNUNGLIST = "getAusgangsrechnungList";
	
	//SingletonClass
	private static final BuchungszeileAusgangsrechnungModel instance = new BuchungszeileAusgangsrechnungModel();
	public static BuchungszeileAusgangsrechnungModel getInstance() {
		return instance;
	}

	@Override
	public ArrayList<Ausgangsrechnung> getBuchungszeileAusgangsrechnungList(int buchungszeile_id) throws DALException {
		notifyView( new NotiEvent( DALFactory.getDAL().getBuchungszeileAusgangsrechnungList(buchungszeile_id), this.GET_LIST) );
		return null;
	}

	@Override
	public Ausgangsrechnung saveBuchungszeileAusgangsrechnung(int buchungszeile_id, Ausgangsrechnung a) throws DALException {
		notifyView( new NotiEvent( DALFactory.getDAL().saveBuchungszeileAusgangsrechnung(buchungszeile_id, a), this.ITEM_CHANGE) );
		
		//notify Auswahlmenu the change
		this.getNotUsedAusgangsrechnungList(buchungszeile_id);
		return null;
	}

	@Override
	public boolean deleteBuchungszeileAusgangsrechnung(int buchungszeile_id, Ausgangsrechnung a) throws DALException {
		
		if(DALFactory.getDAL().deleteBuchungszeileAusgangsrechnung(buchungszeile_id, a)) {
			notifyView( new NotiEvent( a, this.DELETE_ITEM) );
			
			//notify Auswahlmenu the change
			this.getNotUsedAusgangsrechnungList(buchungszeile_id);
			return true;
		} else {
			return false;
		}
	
	}

	@Override
	public ArrayList<Ausgangsrechnung> getNotUsedAusgangsrechnungList(int buchungszeile_id) throws DALException {
		notifyView( new NotiEvent( DALFactory.getDAL().getNotUsedAusgangsrechnungList(buchungszeile_id), this.GET_AUSGANGSRECHNUNGLIST) );
		return null;
	}

}
