package at.namart.model.services;

public class DALException extends Exception {
	
	public DALException() {
		super();
	}
	  
	public DALException ( String s ) {
	    super( s );
	}

}
