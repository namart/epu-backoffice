package at.namart.model.services;

import java.util.ArrayList;

import at.namart.model.bo.Projekt;


public interface IDALProjekt {

	ArrayList<Projekt> getProjektList() throws DALException;
	Projekt saveProjekt(Projekt p) throws DALException;
	boolean deleteProjekt(Projekt p) throws DALException;
}
