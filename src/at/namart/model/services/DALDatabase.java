package at.namart.model.services;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.mysql.jdbc.Statement;

import at.namart.lib.ConfigLoader;
import at.namart.lib.Model;
import at.namart.lib.logger.Logger;
import at.namart.model.bo.Angebot;
import at.namart.model.bo.Ausgangsrechnung;
import at.namart.model.bo.Buchungszeile;
import at.namart.model.bo.Eingangsrechnung;
import at.namart.model.bo.Kategorie;
import at.namart.model.bo.Kontakt;
import at.namart.model.bo.Kunde;
import at.namart.model.bo.Projekt;
import at.namart.model.bo.Rechnungszeile;
import at.namart.model.bo.Zeit;

public class DALDatabase implements IDALProjekt, IDALKunde, IDALAngebot, IDALKontakt, IDALAusgangsrechnung, IDALEingangsrechnung, IDALRechnungszeile, IDALBuchungszeile, IDALBuchungszeilenAusgangsrechnung, IDALBuchungszeilenEingangsrechnung, IDALZeiterfassung {

	private String databaseUser;
	private String databasePassword;
	private String databaseURL;
	
	/*
	 * database Driver
	 * */
	public DALDatabase(String databaseUser, String databasePassword, String databaseURL) throws DALException {
		
		this.databaseUser = databaseUser;
		this.databasePassword = databasePassword;
		this.databaseURL = databaseURL;
		
		try 
        {
            Class.forName("com.mysql.jdbc.Driver");
        }
        catch(ClassNotFoundException ex)
        {
            //throw new DALException(ex.getMessage());
        	Logger.error(ex.getMessage());
            throw new DALException(ex.getMessage());
        }
	}
	
	/*
	 * database Connection Method
	 * */
	private Connection getConnection() throws DALException {
		
		Connection conn = null;
		
		try
        {
			String userName = this.databaseUser;
	        String password = this.databasePassword;
	        String url = this.databaseURL;
			return DriverManager.getConnection(url, userName, password);
        }
        catch (Exception e)
        {
            Logger.error("Cannot connect to database server");
            throw new DALException(e.getMessage());
        }
        finally
        {
            if (conn != null)
            {
                try
                {
                    conn.close ();
                    Logger.trace("Database connection terminated"); 
                }
                catch (Exception e) { /* ignore close errors */ }
            }
        }
	}
	
	
	/*---------------------------------------------------------|
	 *----------------------- PROJEKTE ------------------------|
	 *---------------------------------------------------------|
	 */

	@Override
	public ArrayList<Projekt> getProjektList() throws DALException {
		
		try {
			// Datenbankverbindung �ffnen
			Connection db = this.getConnection();
		
			// SQL STMT vorbereiten
			PreparedStatement cmd = db.prepareStatement("SELECT id, name FROM Projekte");
			// Ausf�hren
			ResultSet rd = cmd.executeQuery();
			
			// produktListe
			ArrayList<Projekt> projektListe = new ArrayList<Projekt>();
			
			// Daten holen
			while(rd.next()) {
				Projekt p = new Projekt(rd.getInt(1), rd.getString(2));
				projektListe.add(p);
			}
			
			rd.close();
			cmd.close();
			db.close();
			
			return projektListe;
		} 
		catch (SQLException e) {
			throw new DALException(e.getMessage());
		}
		catch (Exception e) {
			throw new DALException(e.getMessage());
		}
		
		
	}

	@Override
	public Projekt saveProjekt(Projekt p) throws DALException {
		
		try {
			// Datenbankverbindung �ffnen
			Connection db = this.getConnection();
		
			// Update/Insert cmd
			PreparedStatement cmd;
			
			int result1, newKey = 0;
			
			if(p.getId() != Model.INVALIDID) {
				
				cmd = db.prepareStatement("UPDATE Projekte SET name = ? WHERE id = ?");
				cmd.setString(1, p.getName());
				cmd.setInt(2, p.getId());
				
				// execute insert/update
				result1 = cmd.executeUpdate();
				
			}
			
			else {
				
				cmd = db.prepareStatement("INSERT INTO Projekte (name) VALUES (?)", Statement.RETURN_GENERATED_KEYS);
				cmd.setString(1, p.getName());
				
				// execute insert/update
				result1 = cmd.executeUpdate();
				
				//get generated key of the Project
				ResultSet rs = null;
				
				rs = cmd.getGeneratedKeys();
				
				//Check ob rs leer ist!
				
				while (rs.next()) {
					newKey = rs.getInt(1);
				}
				
				p.setId(newKey);
				
				
			}	
					
			cmd.close();
			db.close();
			
			if(result1 == 0) {
				throw new DALException("Fehler beim speichern");
			}
			
			return p;
			
		} 
		catch (SQLException e) {
			throw new DALException(e.getMessage());
		}
		catch (Exception e) {
			throw new DALException(e.getMessage());
		}
	}

	@Override
	public boolean deleteProjekt(Projekt p) throws DALException {
		
		try {
			// Datenbankverbindung �ffnen
			Connection db = this.getConnection();
		
			int result1, result2;
			
			//Zeiterfassung l�schen
			PreparedStatement cmd1 = db.prepareStatement("DELETE FROM Zeiterfassung WHERE projekt_id = ?");
			cmd1.setInt(1, p.getId());
			result1 = cmd1.executeUpdate();

			//Projekt l�schen
			PreparedStatement cmd2 = db.prepareStatement("DELETE FROM Projekte WHERE id = ?");
			cmd2.setInt(1, p.getId());
			result2 = cmd2.executeUpdate();
			
			cmd1.close();
			cmd2.close();
			db.close();
			
			if(result2 == 0) {
				Logger.trace("nichts zum l�schen");
				return false;
			}
			
			return true;
			
		} 
		catch (SQLException e) {
			if(e.getErrorCode() == 1451) {
				throw new DALException("Dieses Projekt wird verwendet");
			} else {
				throw new DALException(e.getMessage());
			}
		}
		catch (Exception e) {
			throw new DALException(e.getMessage());
		}
	}

	
	/*---------------------------------------------------------|
	 *-------------------- ZEITERFASSUNG ----------------------|
	 *---------------------------------------------------------|
	 */
	
	@Override
	public ArrayList<Zeit> getZeiterfassungList() throws DALException {
		
		try {
			// Datenbankverbindung �ffnen
			Connection db = this.getConnection();
		
			// SQL STMT vorbereiten
			PreparedStatement cmd = db.prepareStatement("SELECT id, dauer, datum, projekt_id FROM Zeiterfassung ");
			// Ausf�hren
			ResultSet rd = cmd.executeQuery();
			
			// produktListe
			ArrayList<Zeit> zeiterfassungListe = new ArrayList<Zeit>();
			
			// Daten holen
			while(rd.next()) {
				Zeit z = new Zeit(rd.getInt(1), rd.getInt(2), rd.getDate(3), rd.getInt(4));
				zeiterfassungListe.add(z);
			}
			
			rd.close();
			cmd.close();
			db.close();
			
			return zeiterfassungListe;
		} 
		catch (SQLException e) {
			throw new DALException(e.getMessage());
		}
		catch (Exception e) {
			throw new DALException(e.getMessage());
		}
		
		
	}

	@Override
	public Zeit saveZeiterfassung(Zeit p) throws DALException {
		
		try {
			// Datenbankverbindung �ffnen
			Connection db = this.getConnection();
		
			// Update/Insert cmd
			PreparedStatement cmd;
			
			int result1, newKey = 0;
			
			if(p.getId() != Model.INVALIDID) {
				
				cmd = db.prepareStatement("UPDATE Zeiterfassung SET dauer = ?, datum = ?, projekt_id = ? WHERE id = ?");
				cmd.setInt(1, p.getDauer());
				cmd.setDate(2, p.getDatum());
				cmd.setInt(3, p.getProjektId());
				cmd.setInt(4, p.getId());
				
				// execute insert/update
				result1 = cmd.executeUpdate();
				
			}
			
			else {
				
				cmd = db.prepareStatement("INSERT INTO Zeiterfassung (dauer, datum, projekt_id) VALUES (?,?,?)", Statement.RETURN_GENERATED_KEYS);
				cmd.setInt(1, p.getDauer());
				cmd.setDate(2, p.getDatum());
				cmd.setInt(3, p.getProjektId());
				
				// execute insert/update
				result1 = cmd.executeUpdate();
				
				//get generated key of the Project
				ResultSet rs = null;
				
				rs = cmd.getGeneratedKeys();
				
				//Check ob rs leer ist!
				
				while (rs.next()) {
					newKey = rs.getInt(1);
				}
				
				p.setId(newKey);
				
				
			}	
					
			cmd.close();
			db.close();
			
			if(result1 == 0) {
				throw new DALException("Fehler beim speichern");
			}
			
			return p;
			
		} 
		catch (SQLException e) {
			throw new DALException(e.getMessage());
		}
		catch (Exception e) {
			throw new DALException(e.getMessage());
		}
	}
	
	@Override
	public boolean deleteZeiterfassung(Zeit p) throws DALException {
		
		try {
			// Datenbankverbindung �ffnen
			Connection db = this.getConnection();
		
			int result;

			//Projekt l�schen
			PreparedStatement cmd = db.prepareStatement("DELETE FROM Zeiterfassung WHERE id = ?");
			cmd.setInt(1, p.getId());
			result = cmd.executeUpdate();
			
			cmd.close();
			db.close();
			
			if(result == 0) {
				Logger.trace("nichts zum l�schen");
				return false;
			}
			
			return true;
			
		} 
		catch (SQLException e) {
			if(e.getErrorCode() == 1451) {
				throw new DALException("Dieses Projekt wird verwendet");
			} else {
				throw new DALException(e.getMessage());
			}
		}
		catch (Exception e) {
			throw new DALException(e.getMessage());
		}
	}

	
	/*---------------------------------------------------------|
	 *------------------------ KUNDEN -------------------------|
	 *---------------------------------------------------------|
	 */

	@Override
	public ArrayList<Kunde> getKundeList() throws DALException {
		
		try {
			// Datenbankverbindung �ffnen
			Connection db = this.getConnection();
		
			// SQL STMT vorbereiten
			PreparedStatement cmd = db.prepareStatement("SELECT id, name, uid, kontakt_id FROM Kunden");
			// Ausf�hren
			ResultSet rd = cmd.executeQuery();
			
			// produktListe
			ArrayList<Kunde> kundenListe = new ArrayList<Kunde>();
			
			// Daten holen
			while(rd.next()) {
				Kunde p = new Kunde(rd.getInt(1), rd.getString(2), rd.getInt(3), rd.getInt(4));
				kundenListe.add(p);
			}
			
			rd.close();
			cmd.close();
			db.close();
			
			return kundenListe;
		} 
		catch (SQLException e) {
			throw new DALException(e.getMessage());
		}
		catch (Exception e) {
			throw new DALException(e.getMessage());
		}
	}
	
	@Override
	public Kunde saveKunde(Kunde k) throws DALException {
		
		try {
			// Datenbankverbindung �ffnen
			Connection db = this.getConnection();
		
			// Update/Insert cmd
			PreparedStatement cmd;
			
			int result, newKey = 0;
			
			if(k.getId() != Model.INVALIDID) {
				
				cmd = db.prepareStatement("UPDATE Kunden SET name = ?, uid = ? , kontakt_id = ? WHERE id = ?");
				cmd.setString(1, k.getName());
				cmd.setInt(2, k.getUid());
				
				//Wenn Foreignkey nicht definiert sind in die Datenbank null schreiben
				if(k.getKontaktId() == Model.UNDEFINED) {
					cmd.setObject(3, null);				
				} else {
					cmd.setInt(3, k.getKontaktId());
				}
				
				cmd.setInt(4, k.getId());
				
				// execute insert/update
				result = cmd.executeUpdate();

			}
			
			else {
				
				cmd = db.prepareStatement("INSERT INTO Kunden (name, uid, kontakt_id) VALUES (?, ?, ?)", Statement.RETURN_GENERATED_KEYS);
				cmd.setString(1, k.getName());
				cmd.setInt(2, k.getUid());
				
				//Wenn Foreignkey nicht definiert sind in die Datenbank null schreiben
				if(k.getKontaktId() == Model.UNDEFINED) {
					cmd.setObject(3, null);				
				} else {
					cmd.setInt(3, k.getKontaktId());
				}
				
				// execute insert/update
				result = cmd.executeUpdate();
				
				//get generated key of the Project
				ResultSet rs = null;	
				rs = cmd.getGeneratedKeys();
				
				//Check ob rs leer ist!
				
				while (rs.next()) {
					newKey = rs.getInt(1);
				}
				
				k.setId(newKey);	
				
			}	
					
			cmd.close();
			db.close();
			
			if(result == 0) {
				throw new DALException("Fehler beim speichern");
			}
			
			return k;
			
		} 
		catch (SQLException e) {
			throw new DALException(e.getMessage());
		}
		catch (Exception e) {
			throw new DALException(e.getMessage());
		}
	}
	
	@Override
	public boolean deleteKunde(Kunde k) throws DALException {
		
		try {
			// Datenbankverbindung �ffnen
			Connection db = this.getConnection();
		
			int result;
	
			//Projekt l�schen
			PreparedStatement cmd = db.prepareStatement("DELETE FROM Kunden WHERE id = ?");
			cmd.setInt(1, k.getId());
			result = cmd.executeUpdate();
			cmd.close();
			db.close();
			
			if(result == 0) {
				Logger.trace("nichts zum l�schen");
				return false;
			}
			
			return true;
			
		} 
		catch (SQLException e) {

			if(e.getErrorCode() == 1451) {
				throw new DALException("Dieser Kunde wird verwendet");
			} else {
				throw new DALException(e.getMessage());
			}
			
		}
		catch (Exception e) {
			throw new DALException(e.getMessage());
		}
	}

	
	/*---------------------------------------------------------|
	 *----------------------- ANGEBOTE ------------------------|
	 *---------------------------------------------------------|
	 */
	
	@Override
	public ArrayList<Angebot> getAngebotList() throws DALException {
		
		try {
			// Datenbankverbindung �ffnen
			Connection db = this.getConnection();
		
			// SQL STMT vorbereiten
			PreparedStatement cmd = db.prepareStatement("SELECT id, name, summe, dauer, datum, chance, projekt_id, kunde_id FROM Angebote");
			// Ausf�hren
			ResultSet rd = cmd.executeQuery();
			
			// produktListe
			ArrayList<Angebot> angebotListe = new ArrayList<Angebot>();
			
			int kundenId, projektId;
			
			// Daten holen
			while(rd.next()) {
				
				//Foreign keys auf undefiened(-1) setzen falls null
				if(rd.getObject(7) == null) {
					projektId = Model.UNDEFINED;
				} else {
					projektId = rd.getInt(7);
				}
				
				if(rd.getObject(8) == null) {
					kundenId = Model.UNDEFINED;
				} else {
					kundenId = rd.getInt(8);
				}
				
				Angebot a = new Angebot(rd.getInt(1), rd.getString(2), rd.getInt(3), rd.getInt(4), rd.getDate(5), rd.getInt(6), projektId, kundenId);
				angebotListe.add(a);
				Logger.trace(a.getProjektId());
				Logger.trace(a.getKundenId());
			}
			
			rd.close();
			cmd.close();
			db.close();
			
			return angebotListe;
		} 
		catch (SQLException e) {
			throw new DALException(e.getMessage());
		}
		catch (Exception e) {
			throw new DALException(e.getMessage());
		}
	}

	@Override
	public Angebot saveAngebot(Angebot a) throws DALException {
		
		try {
			// Datenbankverbindung �ffnen
			Connection db = this.getConnection();
		
			// Update/Insert cmd
			PreparedStatement cmd;
			
			int result, newKey = 0;
			
			if(a.getId() != Model.INVALIDID) {
				
				cmd = db.prepareStatement("UPDATE Angebote SET name = ?, summe = ?, dauer = ?, datum = ?, chance = ?, projekt_id = ?, kunde_id = ? WHERE id = ?");
				cmd.setString(1, a.getName());
				cmd.setInt(2, a.getSumme());
				cmd.setInt(3, a.getDauer());
				cmd.setDate(4, a.getDatum());
				cmd.setInt(5, a.getChance());
				
				//Wenn Foreignkey nicht definiert sind in die Datenbank null schreiben
				if(a.getProjektId() == Model.UNDEFINED) {
					cmd.setObject(6, null);				
				} else {
					cmd.setInt(6, a.getProjektId());
				}
				
				if(a.getKundenId() == Model.UNDEFINED) {
					cmd.setObject(7, null);				
				} else {
					cmd.setInt(7, a.getKundenId());
				}
				
				cmd.setInt(8, a.getId());
				
				// execute insert/update
				result = cmd.executeUpdate();

			}
			
			else {
				
				cmd = db.prepareStatement("INSERT INTO Angebote (name, summe, dauer, datum, chance, projekt_id, kunde_id) VALUES (?, ?, ?, ?, ?, ?, ?)", Statement.RETURN_GENERATED_KEYS);
				cmd.setString(1, a.getName());
				cmd.setInt(2, a.getSumme());
				cmd.setInt(3, a.getDauer());
				cmd.setDate(4, a.getDatum());
				cmd.setInt(5, a.getChance());
				
				//Wenn Foreignkey nicht definiert sind in die Datenbank null schreiben
				if(a.getProjektId() == Model.UNDEFINED) {
					cmd.setObject(6, null);				
				} else {
					cmd.setInt(6, a.getProjektId());
				}
				
				if(a.getKundenId() == Model.UNDEFINED) {
					cmd.setObject(7, null);				
				} else {
					cmd.setInt(7, a.getKundenId());
				}
				
				// execute insert/update
				result = cmd.executeUpdate();
				
				//get generated key of the Project
				ResultSet rs = null;	
				rs = cmd.getGeneratedKeys();
				
				//Check ob rs leer ist!
				
				while (rs.next()) {
					newKey = rs.getInt(1);
				}
				
				a.setId(newKey);	
				
			}	
					
			cmd.close();
			db.close();
			
			if(result == 0) {
				throw new DALException("Fehler beim speichern");
			}
			
			return a;
			
		} 
		catch (SQLException e) {
			throw new DALException(e.getMessage());
		}
		catch (Exception e) {
			throw new DALException(e.getMessage());
		}
	}

	@Override
	public boolean deleteAngebot(Angebot a) throws DALException {
		
		try {
			// Datenbankverbindung �ffnen
			Connection db = this.getConnection();
		
			int result;
	
			//Projekt l�schen
			PreparedStatement cmd = db.prepareStatement("DELETE FROM Angebote WHERE id = ?");
			cmd.setInt(1, a.getId());
			result = cmd.executeUpdate();
			
			cmd.close();
			db.close();
			
			if(result == 0) {
				Logger.trace("nichts zum l�schen");
				return false;
			}
			
			return true;
			
		} 
		catch (SQLException e) {
			if(e.getErrorCode() == 1451) {
				throw new DALException("Dieses Angebot wird verwendet");
			} else {
				throw new DALException(e.getMessage());
			}
		}
		catch (Exception e) {
			throw new DALException(e.getMessage());
		}
	}
	
	
	/*---------------------------------------------------------|
	 *----------------------- KONTAKTE ------------------------|
	 *---------------------------------------------------------|
	 */

	@Override
	public ArrayList<Kontakt> getKontaktList() throws DALException {
		try {
			// Datenbankverbindung �ffnen
			Connection db = this.getConnection();
		
			// SQL STMT vorbereiten
			PreparedStatement cmd = db.prepareStatement("SELECT id, vorname, nachname, adresse, telefon, email FROM Kontakte");
			// Ausf�hren
			ResultSet rd = cmd.executeQuery();
			
			// produktListe
			ArrayList<Kontakt> kontaktListe = new ArrayList<Kontakt>();
			
			// Daten holen
			while(rd.next()) {
				Kontakt p = new Kontakt(rd.getInt(1), rd.getString(2), rd.getString(3), rd.getString(4), rd.getInt(5), rd.getString(6));
				kontaktListe.add(p);
			}
			
			rd.close();
			cmd.close();
			db.close();
			
			return kontaktListe;
		} 
		catch (SQLException e) {
			throw new DALException(e.getMessage());
		}
		catch (Exception e) {
			throw new DALException(e.getMessage());
		}
	}

	@Override
	public Kontakt saveKontakt(Kontakt k) throws DALException {
		try {
			// Datenbankverbindung �ffnen
			Connection db = this.getConnection();
		
			// Update/Insert cmd
			PreparedStatement cmd;
			
			int result, newKey = 0;
			
			if(k.getId() != Model.INVALIDID) {
				
				cmd = db.prepareStatement("UPDATE Kontakte SET vorname = ?, nachname = ?, adresse = ?, telefon = ?, email = ? WHERE id = ?");
				cmd.setString(1, k.getVorname());
				cmd.setString(2, k.getNachname());
				cmd.setString(3, k.getAdresse());
				cmd.setInt(4, k.getTelefon());
				cmd.setString(5, k.getEmail());
				cmd.setInt(6, k.getId());
				
				// execute insert/update
				result = cmd.executeUpdate();

			}
			
			else {
				
				cmd = db.prepareStatement("INSERT INTO Kontakte (nachname, vorname, adresse, telefon, email) VALUES (?, ?, ? ,? ,?)", Statement.RETURN_GENERATED_KEYS);
				cmd.setString(1, k.getVorname());
				cmd.setString(2, k.getNachname());
				cmd.setString(3, k.getAdresse());
				cmd.setInt(4, k.getTelefon());
				cmd.setString(5, k.getEmail());
				
				// execute insert/update
				result = cmd.executeUpdate();
				
				//get generated key of the Project
				ResultSet rs = null;	
				rs = cmd.getGeneratedKeys();
				
				//Check ob rs leer ist!
				
				while (rs.next()) {
					newKey = rs.getInt(1);
				}
				
				k.setId(newKey);	
				
			}	
					
			cmd.close();
			db.close();
			
			if(result == 0) {
				throw new DALException("Fehler beim speichern");
			}
			
			return k;
			
		} 
		catch (SQLException e) {
			throw new DALException(e.getMessage());
		}
		catch (Exception e) {
			throw new DALException(e.getMessage());
		}
	}

	@Override
	public boolean deleteKontakt(Kontakt k) throws DALException {
		try {
			// Datenbankverbindung �ffnen
			Connection db = this.getConnection();
		
			int result;
	
			//Projekt l�schen
			PreparedStatement cmd = db.prepareStatement("DELETE FROM Kontakte WHERE id = ?");
			cmd.setInt(1, k.getId());
			result = cmd.executeUpdate();
			cmd.close();
			db.close();
			
			if(result == 0) {
				Logger.trace("nichts zum l�schen");
				return false;
			}
			
			return true;
			
		} 
		catch (SQLException e) {

			if(e.getErrorCode() == 1451) {
				throw new DALException("Dieser Kunde wird verwendet");
			} else {
				throw new DALException(e.getMessage());
			}
			
		}
		catch (Exception e) {
			throw new DALException(e.getMessage());
		}
	}


	/*---------------------------------------------------------|
	 *------------------ AUSGANGSRECHNUNGEN -------------------|
	 *---------------------------------------------------------|
	 */
	
	@Override
	public ArrayList<Ausgangsrechnung> getAusgangsrechnungList() throws DALException {
		
		try {
			// Datenbankverbindung �ffnen
			Connection db = this.getConnection();
		
			// SQL STMT vorbereiten
			PreparedStatement cmd = db.prepareStatement("SELECT id, name, kunde_id FROM Ausgangsrechnungen");
			// Ausf�hren
			ResultSet rd = cmd.executeQuery();
			
			// produktListe
			ArrayList<Ausgangsrechnung> ausgangsrechnungListe = new ArrayList<Ausgangsrechnung>();
			
			int kundenId;
			
			// Daten holen
			while(rd.next()) {
				
				//Foreign keys auf undefiened(-1) setzen falls null
				if(rd.getObject(3) == null) {
					kundenId = Model.UNDEFINED;
				} else {
					kundenId = rd.getInt(3);
				}
				
				Ausgangsrechnung a = new Ausgangsrechnung(rd.getInt(1), rd.getString(2), kundenId);
				ausgangsrechnungListe.add(a);
			}
			
			rd.close();
			cmd.close();
			db.close();
			
			return ausgangsrechnungListe;
		} 
		catch (SQLException e) {
			throw new DALException(e.getMessage());
		}
		catch (Exception e) {
			throw new DALException(e.getMessage());
		}
	}

	@Override
	public Ausgangsrechnung saveAusgangsrechnung(Ausgangsrechnung a) throws DALException {
		
		try {
			// Datenbankverbindung �ffnen
			Connection db = this.getConnection();
		
			// Update/Insert cmd
			PreparedStatement cmd;
			
			int result, newKey = 0;
			
			if(a.getId() != Model.INVALIDID) {
				
				cmd = db.prepareStatement("UPDATE Ausgangsrechnungen SET name = ?, kunde_id = ? WHERE id = ?");
				cmd.setString(1, a.getName());
				
				//Wenn Foreignkey nicht definiert sind in die Datenbank null schreiben				
				if(a.getKundenId() == Model.UNDEFINED) {
					cmd.setObject(2, null);				
				} else {
					cmd.setInt(2, a.getKundenId());
				}
				
				cmd.setInt(3, a.getId());
				
				// execute insert/update
				result = cmd.executeUpdate();

			}
			
			else {
				
				cmd = db.prepareStatement("INSERT INTO Ausgangsrechnungen (name, kunde_id) VALUES (?, ?)", Statement.RETURN_GENERATED_KEYS);
				cmd.setString(1, a.getName());
				
				//Wenn Foreignkey nicht definiert sind in die Datenbank null schreiben
				if(a.getKundenId() == Model.UNDEFINED) {
					cmd.setObject(2, null);				
				} else {
					cmd.setInt(2, a.getKundenId());
				}
				
				// execute insert/update
				result = cmd.executeUpdate();
				
				//get generated key of the Project
				ResultSet rs = null;	
				rs = cmd.getGeneratedKeys();
				
				//Check ob rs leer ist!
				
				while (rs.next()) {
					newKey = rs.getInt(1);
				}
				
				a.setId(newKey);	
				
			}	
					
			cmd.close();
			db.close();
			
			if(result == 0) {
				throw new DALException("Fehler beim speichern");
			}
			
			return a;
			
		} 
		catch (SQLException e) {
			throw new DALException(e.getMessage());
		}
		catch (Exception e) {
			throw new DALException(e.getMessage());
		}
	}

	@Override
	public boolean deleteAusgangsrechnung(Ausgangsrechnung a) throws DALException {
		
		try {
			// Datenbankverbindung �ffnen
			Connection db = this.getConnection();
		
			int result;
	
			//Projekt l�schen
			PreparedStatement cmd = db.prepareStatement("DELETE FROM Ausgangsrechnungen WHERE id = ?");
			cmd.setInt(1, a.getId());
			result = cmd.executeUpdate();
			
			cmd.close();
			db.close();
			
			if(result == 0) {
				Logger.trace("nichts zum l�schen");
				return false;
			}
			
			return true;
			
		} 
		catch (SQLException e) {

			if(e.getErrorCode() == 1451) {
				throw new DALException("Diese Ausgangsrechnung wird verwendet");
			} else {
				throw new DALException(e.getMessage());
			}
			
		}
		catch (Exception e) {
			throw new DALException(e.getMessage());
		}
	}

	
	
	/*---------------------------------------------------------|
	 *------------------ EINGANGSRECHNUNGEN -------------------|
	 *---------------------------------------------------------|
	 */
	
	@Override
	public ArrayList<Eingangsrechnung> getEingangsrechnungList() throws DALException {
		
		try {
			// Datenbankverbindung �ffnen
			Connection db = this.getConnection();
		
			// SQL STMT vorbereiten
			PreparedStatement cmd = db.prepareStatement("SELECT id, name, kontakt_id FROM Eingangsrechnungen");
			// Ausf�hren
			ResultSet rd = cmd.executeQuery();
			
			// produktListe
			ArrayList<Eingangsrechnung> eingangsrechnungListe = new ArrayList<Eingangsrechnung>();
			
			int kontaktId;
			
			// Daten holen
			while(rd.next()) {
				
				//Foreign keys auf undefiened(-1) setzen falls null
				if(rd.getObject(3) == null) {
					kontaktId = Model.UNDEFINED;
				} else {
					kontaktId = rd.getInt(3);
				}
				
				Eingangsrechnung e = new Eingangsrechnung(rd.getInt(1), rd.getString(2), kontaktId);
				eingangsrechnungListe.add(e);
			}
			
			rd.close();
			cmd.close();
			db.close();
			
			return eingangsrechnungListe;
		} 
		catch (SQLException e) {
			throw new DALException(e.getMessage());
		}
		catch (Exception e) {
			throw new DALException(e.getMessage());
		}
	}

	@Override
	public Eingangsrechnung saveEingangsrechnung(Eingangsrechnung e) throws DALException {
		
		try {
			// Datenbankverbindung �ffnen
			Connection db = this.getConnection();
		
			// Update/Insert cmd
			PreparedStatement cmd;
			
			int result, newKey = 0;
			
			if(e.getId() != Model.INVALIDID) {
				
				cmd = db.prepareStatement("UPDATE Eingangsrechnungen SET name = ?, kontakt_id = ? WHERE id = ?");
				cmd.setString(1, e.getName());
				
				//Wenn Foreignkey nicht definiert sind in die Datenbank null schreiben				
				if(e.getKontaktId() == Model.UNDEFINED) {
					cmd.setObject(2, null);				
				} else {
					cmd.setInt(2, e.getKontaktId());
				}
				
				cmd.setInt(3, e.getId());
				
				// execute insert/update
				result = cmd.executeUpdate();

			}
			
			else {
				
				cmd = db.prepareStatement("INSERT INTO Eingangsrechnungen (name, kontakt_id) VALUES (?, ?)", Statement.RETURN_GENERATED_KEYS);
				cmd.setString(1, e.getName());
				
				//Wenn Foreignkey nicht definiert sind in die Datenbank null schreiben
				if(e.getKontaktId() == Model.UNDEFINED) {
					cmd.setObject(2, null);				
				} else {
					cmd.setInt(2, e.getKontaktId());
				}
				
				// execute insert/update
				result = cmd.executeUpdate();
				
				//get generated key of the Project
				ResultSet rs = null;	
				rs = cmd.getGeneratedKeys();
				
				//Check ob rs leer ist!
				
				while (rs.next()) {
					newKey = rs.getInt(1);
				}
				
				e.setId(newKey);	
				
			}	
					
			cmd.close();
			db.close();
			
			if(result == 0) {
				throw new DALException("Fehler beim speichern");
			}
			
			return e;
			
		} 
		catch (SQLException ex) {
			throw new DALException(ex.getMessage());
		}
		catch (Exception ex) {
			throw new DALException(ex.getMessage());
		}
	}

	@Override
	public boolean deleteEingangsrechnung(Eingangsrechnung e) throws DALException {
		
		try {
			// Datenbankverbindung �ffnen
			Connection db = this.getConnection();
		
			int result;
	
			//Projekt l�schen
			PreparedStatement cmd = db.prepareStatement("DELETE FROM Eingangsrechnungen WHERE id = ?");
			cmd.setInt(1, e.getId());
			result = cmd.executeUpdate();
			
			cmd.close();
			db.close();
			
			if(result == 0) {
				Logger.trace("nichts zum l�schen");
				return false;
			}
			
			return true;
			
		} 
		catch (SQLException ex) {

			if(ex.getErrorCode() == 1451) {
				throw new DALException("Diese Eingangsrechnung wird verwendet");
			} else {
				throw new DALException(ex.getMessage());
			}
			
		}
		catch (Exception ex) {
			throw new DALException(ex.getMessage());
		}
	}

	
	
	
	/*---------------------------------------------------------|
	 *-------------------- RECHNUNGSZEILEN --------------------|
	 *---------------------------------------------------------|
	 */
	
	@Override
	public ArrayList<Rechnungszeile> getRechnungszeileByIdList(int AusgangsrechnungsId) throws DALException {
		try {
			// Datenbankverbindung �ffnen
			Connection db = this.getConnection();
		
			// SQL STMT vorbereiten
			PreparedStatement cmd = db.prepareStatement("SELECT id, name, angebot_id FROM Rechnungszeilen WHERE ausgangsrechnung_id = ?");
			cmd.setInt(1, AusgangsrechnungsId);
			
			// Ausf�hren
			ResultSet rd = cmd.executeQuery();
			
			// produktListe
			ArrayList<Rechnungszeile> rechnungszeilenListe = new ArrayList<Rechnungszeile>();
			
			int angebotId;
			
			// Daten holen
			while(rd.next()) {
				
				//Foreign keys auf undefiened(-1) setzen falls null
				if(rd.getObject(3) == null) {
					angebotId = Model.UNDEFINED;
				} else {
					angebotId = rd.getInt(3);
				}
				
				Rechnungszeile rz = new Rechnungszeile(rd.getInt(1), rd.getString(2), angebotId, AusgangsrechnungsId);
				rechnungszeilenListe.add(rz);
			}
			
			rd.close();
			cmd.close();
			db.close();
			
			return rechnungszeilenListe;
		} 
		catch (SQLException e) {
			throw new DALException(e.getMessage());
		}
		catch (Exception e) {
			throw new DALException(e.getMessage());
		}
	}

	@Override
	public Rechnungszeile saveRechnungszeile(Rechnungszeile r) throws DALException {
		
		try {
			
			// Datenbankverbindung �ffnen
			Connection db = this.getConnection();
		
			// Update/Insert cmd
			PreparedStatement cmd;
			
			int result, newKey = 0;
			
			if(r.getId() != Model.INVALIDID) {
				
				cmd = db.prepareStatement("UPDATE Rechnungszeilen SET name = ?, angebot_id = ?, ausgangsrechnung_id = ? WHERE id = ?");
				cmd.setString(1, r.getName());
				
				//Wenn Foreignkey nicht definiert sind in die Datenbank null schreiben				
				if(r.getAngebotId() == Model.UNDEFINED) {
					cmd.setObject(2, null);				
				} else {
					cmd.setInt(2, r.getAngebotId());
				}
				cmd.setInt(3, r.getAusgangsrechnungId());
				cmd.setInt(4, r.getId());
				
				// execute insert/update
				result = cmd.executeUpdate();

			}
			
			else {
				
				cmd = db.prepareStatement("INSERT INTO Rechnungszeilen (name, angebot_id, ausgangsrechnung_id) VALUES (?, ?, ?)", Statement.RETURN_GENERATED_KEYS);
				cmd.setString(1, r.getName());
				
				//Wenn Foreignkey nicht definiert sind in die Datenbank null schreiben
				if(r.getAngebotId() == Model.UNDEFINED) {
					cmd.setObject(2, null);				
				} else {
					cmd.setInt(2, r.getAngebotId());
				}
				
				cmd.setInt(3, r.getAusgangsrechnungId());
				
				// execute insert/update
				result = cmd.executeUpdate();
				
				//get generated key of the Project
				ResultSet rs = null;	
				rs = cmd.getGeneratedKeys();
				
				//Check ob rs leer ist!
				
				while (rs.next()) {
					newKey = rs.getInt(1);
				}
				
				r.setId(newKey);	
				
			}	
					
			cmd.close();
			db.close();
			
			if(result == 0) {
				throw new DALException("Fehler beim speichern");
			}
			
			return r;
			
		} 
		catch (SQLException e) {
			throw new DALException(e.getMessage());
		}
		catch (Exception e) {
			throw new DALException(e.getMessage());
		}
	}

	@Override
	public boolean deleteRechnungszeile(Rechnungszeile r) throws DALException {
		try {
			// Datenbankverbindung �ffnen
			Connection db = this.getConnection();
		
			int result;
	
			//Projekt l�schen
			PreparedStatement cmd = db.prepareStatement("DELETE FROM Rechnungszeilen WHERE id = ?");
			cmd.setInt(1, r.getId());
			result = cmd.executeUpdate();
			
			cmd.close();
			db.close();
			
			if(result == 0) {
				Logger.trace("nichts zum l�schen");
				return false;
			}
			
			return true;
			
		} 
		catch (SQLException e) {
			throw new DALException(e.getMessage());
		}
		catch (Exception e) {
			throw new DALException(e.getMessage());
		}
	}

	public ArrayList<Angebot> getRechnungszeileAngebotByIdList(int AusgangsrechnungsId) throws DALException {
		try {
			// Datenbankverbindung �ffnen
			Connection db = this.getConnection();
			

			
		
			// SQL STMT vorbereiten
			PreparedStatement cmd = db.prepareStatement("SELECT Angebote.id, Angebote.name, Angebote.summe, Angebote.dauer, Angebote.datum, Angebote.chance FROM Rechnungszeilen JOIN Angebote WHERE Rechnungszeilen.angebot_id = Angebote.id AND ausgangsrechnung_id = ?");
			cmd.setInt(1, AusgangsrechnungsId);
			
			// Ausf�hren
			ResultSet rd = cmd.executeQuery();
			
			// produktListe
			ArrayList<Angebot> rechnungszeilenAngebotListe = new ArrayList<Angebot>();
			
			// Daten holen
			while(rd.next()) {
				
				Angebot raz = new Angebot(rd.getInt(1), rd.getString(2), rd.getInt(3), rd.getInt(4), rd.getDate(5), rd.getInt(6), -1, -1);
				rechnungszeilenAngebotListe.add(raz);
			}
			
			rd.close();
			cmd.close();
			db.close();
			
			return rechnungszeilenAngebotListe;
		} 
		catch (SQLException e) {
			throw new DALException(e.getMessage());
		}
		catch (Exception e) {
			throw new DALException(e.getMessage());
		}
	}
	

	/*---------------------------------------------------------|
	 *-------------------- BUCHUNGSZEILEN ---------------------|
	 *---------------------------------------------------------|
	 */
	
	@Override
	public ArrayList<Buchungszeile> getBuchungszeileList() throws DALException {
		
		try {
			// Datenbankverbindung �ffnen
			Connection db = this.getConnection();
		
			// SQL STMT vorbereiten
			PreparedStatement cmd = db.prepareStatement("SELECT id, name, betrag, kategorie_id FROM Buchungszeilen LEFT JOIN BuchungszeileKategorien ON Buchungszeilen.id = BuchungszeileKategorien.buchungszeile_id");
			// Ausf�hren
			ResultSet rd = cmd.executeQuery();
			
			// produktListe
			ArrayList<Buchungszeile> buchungszeileListe = new ArrayList<Buchungszeile>();
			
			// Daten holen
			while(rd.next()) {
				
				Buchungszeile b = new Buchungszeile(rd.getInt(1), rd.getString(2), rd.getFloat(3));
				
				ArrayList<Integer> kategorien = new ArrayList<Integer>();
				kategorien.add(rd.getInt(4));
				
				int buchungszeileId = rd.getInt(1);
	
				while(rd.next()) {		
					if(rd.getInt(1) == buchungszeileId) {
						kategorien.add(rd.getInt(4));
					}
					else {
						rd.previous();
						break;
					}
				}
				
				b.setKategorienIds(kategorien);
				buchungszeileListe.add(b);
			}
			
			rd.close();
			cmd.close();
			db.close();
			
			return buchungszeileListe;
		} 
		catch (SQLException e) {
			throw new DALException(e.getMessage());
		}
		catch (Exception e) {
			throw new DALException(e.getMessage());
		}
	}

	@Override
	public Buchungszeile saveBuchungszeile(Buchungszeile b) throws DALException {
		
		try {
			// Datenbankverbindung �ffnen
			Connection db = this.getConnection();
		
			// Update/Insert cmd
			PreparedStatement cmd;
			PreparedStatement cmd2 = null;
			
			int result, result2, newKey = 0;
			
			if(b.getId() != Model.INVALIDID) {
				
				cmd = db.prepareStatement("UPDATE Buchungszeilen SET name = ?, betrag = ? WHERE id = ?");
				cmd.setString(1, b.getName());	
				cmd.setFloat(2, b.getBetrag());
				cmd.setInt(3, b.getId());
				
				// execute insert/update
				result = cmd.executeUpdate();
				
				//Update Kategories
				cmd2 = db.prepareStatement("SELECT buchungszeile_id, kategorie_id FROM BuchungszeileKategorien WHERE buchungszeile_id = ?");
				cmd2.setInt(1, b.getId());
				ResultSet rd = cmd2.executeQuery();
				
				ArrayList<Integer> kategorienRemote = new ArrayList<Integer>();
				while(rd.next()) {
					kategorienRemote.add(rd.getInt(2));
				}
				
				for(Integer lkatId : b.getKategorienIds()) {
					
					boolean katExists = false;
					
					for(Integer rkatId : kategorienRemote) {		
						if(lkatId == rkatId) {
							katExists = true;		
						}			
					}
					
					if(!katExists) {
						
						//INSERT
						cmd2 = db.prepareStatement("INSERT INTO BuchungszeileKategorien (buchungszeile_id, kategorie_id) VALUES (?, ?)");
						cmd2.setInt(1, b.getId());
						cmd2.setInt(2, lkatId);
						
						result2 = cmd2.executeUpdate();
						if(result2 == 0) {
							throw new DALException("Fehler beim aktualisieren der Kategorien");
						}
					}
				}
				
				for(Integer rkatId : kategorienRemote) {
					
					boolean katExists = false;
					
					for(Integer lkatId : b.getKategorienIds()) {		
						if(rkatId == lkatId) {
							katExists = true;		
						}			
					}
					
					if(!katExists) {
						
						//DELETE
						cmd2 = db.prepareStatement("DELETE FROM BuchungszeileKategorien WHERE buchungszeile_id = ? AND kategorie_id = ?");
						cmd2.setInt(1, b.getId());
						cmd2.setInt(2, rkatId);
						result2 = cmd2.executeUpdate();
						Logger.debug(result2);
						if(result2 == 0) {
							throw new DALException("Fehler beim aktualisieren der Kategorien");
						}
					}
				}
		
			}
			
			else {
				
				cmd = db.prepareStatement("INSERT INTO Buchungszeilen (name, betrag) VALUES (?, ?)", Statement.RETURN_GENERATED_KEYS);
				cmd.setString(1, b.getName());
				cmd.setFloat(2, b.getBetrag());
				
				// execute insert/update
				result = cmd.executeUpdate();
				
				//get generated key of the Project
				ResultSet rs = null;	
				rs = cmd.getGeneratedKeys();
				
				//Check ob rs leer ist!
				
				while (rs.next()) {
					newKey = rs.getInt(1);
				}
				
				b.setId(newKey);	
				
				//Update Kategories
				if(b.getKategorienIds() != null) {
					for (Integer kategorieId: b.getKategorienIds()) {
						cmd2 = db.prepareStatement("INSERT INTO BuchungszeileKategorien (buchungszeile_id, kategorie_id) VALUES (?, ?)");
						cmd2.setInt(1, b.getId());
						cmd2.setInt(2, kategorieId);
						
						result2 = cmd2.executeUpdate();
						if(result2 == 0) {
							throw new DALException("Fehler beim aktualisieren der Kategorien");
						}
					}
				}
				
			}	
					
			cmd.close();
			db.close();
			
			if(result == 0) {
				throw new DALException("Fehler beim speichern");
			}
			
			return b;
			
		} 
		catch (SQLException e) {
			throw new DALException(e.getMessage());
		}
		catch (Exception e) {
			throw new DALException(e.getMessage());
		}
	}

	@Override
	public boolean deleteBuchungszeile(Buchungszeile b) throws DALException {
		
		try {
			// Datenbankverbindung �ffnen
			Connection db = this.getConnection();
		
			int result, result2;
			
			//Delete Kategories
			PreparedStatement cmd2 = db.prepareStatement("DELETE FROM BuchungszeileKategorien WHERE buchungszeile_id = ?");
			cmd2.setInt(1, b.getId());
			result2 = cmd2.executeUpdate();
			
			//Delete Eingangsrechnungen
			PreparedStatement cmd3 = db.prepareStatement("DELETE FROM BuchungszeileEingangsrechnungen WHERE buchungszeile_id = ?");
			cmd3.setInt(1, b.getId());
			result2 = cmd3.executeUpdate();
			
			//Delete Ausgangsrechnungen
			PreparedStatement cmd4 = db.prepareStatement("DELETE FROM BuchungszeileAusgangsrechnungen WHERE buchungszeile_id = ?");
			cmd4.setInt(1, b.getId());
			result2 = cmd4.executeUpdate();
	
			//Buchungszeile l�schen
			PreparedStatement cmd = db.prepareStatement("DELETE FROM Buchungszeilen WHERE id = ?");
			cmd.setInt(1, b.getId());
			result = cmd.executeUpdate();
			
			
			cmd.close();
			db.close();
			
			if(result == 0) {
				Logger.trace("nichts zum l�schen");
				return false;
			}
			
			return true;
			
		} 
		catch (SQLException e) {
			throw new DALException(e.getMessage());
		}
		catch (Exception e) {
			throw new DALException(e.getMessage());
		}
	}
	
	@Override
	public ArrayList<Kategorie> getKategorien() throws DALException {
		
		try {
			// Datenbankverbindung �ffnen
			Connection db = this.getConnection();
		
			// SQL STMT vorbereiten
			PreparedStatement cmd = db.prepareStatement("SELECT id, name FROM Kategorien");
			// Ausf�hren
			ResultSet rd = cmd.executeQuery();
			
			// produktListe
			ArrayList<Kategorie> kategorieListe = new ArrayList<Kategorie>();
			
			// Daten holen
			while(rd.next()) {
				
				Kategorie k = new Kategorie(rd.getInt(1), rd.getString(2));

				kategorieListe.add(k);
			}
			
			rd.close();
			cmd.close();
			db.close();
			
			return kategorieListe;
		} 
		catch (SQLException e) {
			throw new DALException(e.getMessage());
		}
		catch (Exception e) {
			throw new DALException(e.getMessage());
		}
	}

	public Kategorie saveKategorie(Kategorie k) throws DALException {
		
		try {
			// Datenbankverbindung �ffnen
			Connection db = this.getConnection();
		
			// Update/Insert cmd
			PreparedStatement cmd;
			
			int result1, newKey = 0;
			
			if(k.getId() != Model.INVALIDID) {
				
				cmd = db.prepareStatement("UPDATE Kategorien SET name = ? WHERE id = ?");
				cmd.setString(1, k.getName());
				cmd.setInt(2, k.getId());
				
				// execute insert/update
				result1 = cmd.executeUpdate();
				
			}
			
			else {
				
				cmd = db.prepareStatement("INSERT INTO Kategorien (name) VALUES (?)", Statement.RETURN_GENERATED_KEYS);
				cmd.setString(1, k.getName());
				
				// execute insert/update
				result1 = cmd.executeUpdate();
				
				//get generated key of the Project
				ResultSet rs = null;
				
				rs = cmd.getGeneratedKeys();
				
				//Check ob rs leer ist!
				
				while (rs.next()) {
					newKey = rs.getInt(1);
				}
				
				k.setId(newKey);
				
				
			}	
					
			cmd.close();
			db.close();
			
			if(result1 == 0) {
				throw new DALException("Fehler beim speichern");
			}
			
			return k;
			
		} 
		catch (SQLException e) {
			throw new DALException(e.getMessage());
		}
		catch (Exception e) {
			throw new DALException(e.getMessage());
		}
	}
	
	
	/*---------------------------------------------------------|
	 *------------ BUCHUNGSZEILEN-AUSGANGSRECHNUNG ------------|
	 *---------------------------------------------------------|
	 */
	
	@Override
	public ArrayList<Ausgangsrechnung> getBuchungszeileAusgangsrechnungList(int buchungszeile_id) throws DALException {
		try {
			
			// Datenbankverbindung �ffnen
			Connection db = this.getConnection();
		
			// SQL STMT vorbereiten
			PreparedStatement cmd = db.prepareStatement("SELECT id, name, kunde_id FROM Ausgangsrechnungen JOIN BuchungszeileAusgangsrechnungen ON BuchungszeileAusgangsrechnungen.buchungszeile_id = ? AND BuchungszeileAusgangsrechnungen.ausgangsrechnung_id = Ausgangsrechnungen.id");
			cmd.setInt(1, buchungszeile_id);
			// Ausf�hren
			ResultSet rd = cmd.executeQuery();
			
			// produktListe
			ArrayList<Ausgangsrechnung> ausgangsrechnungListe = new ArrayList<Ausgangsrechnung>();
			
			// Daten holen
			while(rd.next()) {
				
				Ausgangsrechnung a = new Ausgangsrechnung(rd.getInt(1), rd.getString(2), rd.getInt(3));

				ausgangsrechnungListe.add(a);
			}
			
			rd.close();
			cmd.close();
			db.close();
			
			return ausgangsrechnungListe;
		} 
		catch (SQLException e) {
			throw new DALException(e.getMessage());
		}
		catch (Exception e) {
			throw new DALException(e.getMessage());
		}
	}
	

	@Override
	public Ausgangsrechnung saveBuchungszeileAusgangsrechnung(int buchungszeile_id, Ausgangsrechnung a) throws DALException {
		
		try {
			// Datenbankverbindung �ffnen
			Connection db = this.getConnection();
		
			// Update/Insert cmd
			PreparedStatement cmd;
			
			int result, newKey = 0;
			
			cmd = db.prepareStatement("INSERT INTO BuchungszeileAusgangsrechnungen (buchungszeile_id, ausgangsrechnung_id) VALUES (?, ?)");
			cmd.setInt(1, buchungszeile_id);
			cmd.setInt(2, a.getId());
			
			// execute insert/update
			result = cmd.executeUpdate();
					
			cmd.close();
			db.close();
			
			if(result == 0) {
				throw new DALException("Fehler beim speichern");
			}
			
			return a;
			
		} 
		catch (SQLException e) {
			throw new DALException(e.getMessage());
		}
		catch (Exception e) {
			throw new DALException(e.getMessage());
		}
	}
	

	@Override
	public boolean deleteBuchungszeileAusgangsrechnung(int buchungszeile_id, Ausgangsrechnung a) throws DALException {
		
		try {
			// Datenbankverbindung �ffnen
			Connection db = this.getConnection();
		
			int result;
	
			//Projekt l�schen
			PreparedStatement cmd = db.prepareStatement("DELETE FROM BuchungszeileAusgangsrechnungen WHERE buchungszeile_id = ? AND ausgangsrechnung_id = ?");
			cmd.setInt(1, buchungszeile_id);
			cmd.setInt(2, a.getId());
			result = cmd.executeUpdate();
			cmd.close();
			db.close();
			
			if(result == 0) {
				Logger.trace("nichts zum l�schen");
				return false;
			}
			
			return true;
			
		} 
		catch (SQLException e) {

			if(e.getErrorCode() == 1451) {
				throw new DALException("Dieser Kunde wird verwendet");
			} else {
				throw new DALException(e.getMessage());
			}
			
		}
		catch (Exception e) {
			throw new DALException(e.getMessage());
		}
	}
	

	@Override
	public ArrayList<Ausgangsrechnung> getNotUsedAusgangsrechnungList(int buchungszeile_id) throws DALException {

		try {
			
			// Datenbankverbindung �ffnen
			Connection db = this.getConnection();
		
			// SQL STMT vorbereiten
			PreparedStatement cmd = db.prepareStatement("SELECT DISTINCT id, name, kunde_id FROM Ausgangsrechnungen WHERE (id) NOT IN (SELECT ausgangsrechnung_id FROM BuchungszeileAusgangsrechnungen)");
			//cmd.setInt(1, buchungszeile_id);
			// Ausf�hren
			ResultSet rd = cmd.executeQuery();
			
			// produktListe
			ArrayList<Ausgangsrechnung> ausgangsrechnungListe = new ArrayList<Ausgangsrechnung>();
			
			// Daten holen
			while(rd.next()) {
				
				Ausgangsrechnung a = new Ausgangsrechnung(rd.getInt(1), rd.getString(2), rd.getInt(3));

				ausgangsrechnungListe.add(a);
			}
			
			rd.close();
			cmd.close();
			db.close();
			
			return ausgangsrechnungListe;
		} 
		catch (SQLException e) {
			throw new DALException(e.getMessage());
		}
		catch (Exception e) {
			throw new DALException(e.getMessage());
		}
	}
	

	/*---------------------------------------------------------|
	 *------------ BUCHUNGSZEILEN-EinGANGSRECHNUNG ------------|
	 *---------------------------------------------------------|
	 */
	
	@Override
	public ArrayList<Eingangsrechnung> getBuchungszeileEingangsrechnungList(int buchungszeile_id) throws DALException {
		try {
			
			// Datenbankverbindung �ffnen
			Connection db = this.getConnection();
		
			// SQL STMT vorbereiten
			PreparedStatement cmd = db.prepareStatement("SELECT id, name, kontakt_id FROM Eingangsrechnungen JOIN BuchungszeileEingangsrechnungen ON BuchungszeileEingangsrechnungen.buchungszeile_id = ? AND BuchungszeileEingangsrechnungen.eingangsrechnung_id = Eingangsrechnungen.id");
			cmd.setInt(1, buchungszeile_id);
			// Ausf�hren
			ResultSet rd = cmd.executeQuery();
			
			// produktListe
			ArrayList<Eingangsrechnung> eingangsrechnungListe = new ArrayList<Eingangsrechnung>();
			
			// Daten holen
			while(rd.next()) {
				
				Eingangsrechnung e = new Eingangsrechnung(rd.getInt(1), rd.getString(2), rd.getInt(3));

				eingangsrechnungListe.add(e);
			}
			
			rd.close();
			cmd.close();
			db.close();
			
			return eingangsrechnungListe;
		} 
		catch (SQLException e) {
			throw new DALException(e.getMessage());
		}
		catch (Exception e) {
			throw new DALException(e.getMessage());
		}
	}
	

	@Override
	public Eingangsrechnung saveBuchungszeileEingangsrechnung(int buchungszeile_id, Eingangsrechnung e) throws DALException {
		
		try {
			// Datenbankverbindung �ffnen
			Connection db = this.getConnection();
		
			// Update/Insert cmd
			PreparedStatement cmd;
			
			int result, newKey = 0;
			
			cmd = db.prepareStatement("INSERT INTO BuchungszeileEingangsrechnungen (buchungszeile_id, eingangsrechnung_id) VALUES (?, ?)");
			cmd.setInt(1, buchungszeile_id);
			cmd.setInt(2, e.getId());
			
			// execute insert/update
			result = cmd.executeUpdate();
					
			cmd.close();
			db.close();
			
			if(result == 0) {
				throw new DALException("Fehler beim speichern");
			}
			
			return e;
			
		} 
		catch (SQLException ex) {
			throw new DALException(ex.getMessage());
		}
		catch (Exception ex) {
			throw new DALException(ex.getMessage());
		}
	}
	

	@Override
	public boolean deleteBuchungszeileEingangsrechnung(int buchungszeile_id, Eingangsrechnung e) throws DALException {
		
		try {
			// Datenbankverbindung �ffnen
			Connection db = this.getConnection();
		
			int result;
	
			//Projekt l�schen
			PreparedStatement cmd = db.prepareStatement("DELETE FROM BuchungszeileEingangsrechnungen WHERE buchungszeile_id = ? AND eingangsrechnung_id = ?");
			cmd.setInt(1, buchungszeile_id);
			cmd.setInt(2, e.getId());
			result = cmd.executeUpdate();
			cmd.close();
			db.close();
			
			if(result == 0) {
				Logger.trace("nichts zum l�schen");
				return false;
			}
			
			return true;
			
		} 
		catch (SQLException ex) {
			throw new DALException(ex.getMessage());	
		}
		catch (Exception ex) {
			throw new DALException(ex.getMessage());
		}
	}
	

	@Override
	public ArrayList<Eingangsrechnung> getNotUsedEingangsrechnungList(int buchungszeile_id) throws DALException {

		try {
			
			// Datenbankverbindung �ffnen
			Connection db = this.getConnection();
		
			// SQL STMT vorbereiten
			PreparedStatement cmd = db.prepareStatement("SELECT DISTINCT id, name, kontakt_id FROM Eingangsrechnungen WHERE (id) NOT IN (SELECT eingangsrechnung_id FROM BuchungszeileEingangsrechnungen)");
			//cmd.setInt(1, buchungszeile_id);
			// Ausf�hren
			ResultSet rd = cmd.executeQuery();
			
			// produktListe
			ArrayList<Eingangsrechnung> eingangsrechnungListe = new ArrayList<Eingangsrechnung>();
			
			// Daten holen
			while(rd.next()) {
				
				Eingangsrechnung e = new Eingangsrechnung(rd.getInt(1), rd.getString(2), rd.getInt(3));

				eingangsrechnungListe.add(e);
			}
			
			rd.close();
			cmd.close();
			db.close();
			
			return eingangsrechnungListe;
		} 
		catch (SQLException e) {
			throw new DALException(e.getMessage());
		}
		catch (Exception e) {
			throw new DALException(e.getMessage());
		}
	}
	
}
