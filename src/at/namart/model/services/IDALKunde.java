package at.namart.model.services;

import java.util.ArrayList;

import at.namart.model.bo.Kunde;


public interface IDALKunde {

	ArrayList<Kunde> getKundeList() throws DALException;
	Kunde saveKunde(Kunde p) throws DALException;
	boolean deleteKunde(Kunde p) throws DALException;
}
