package at.namart.model.services;

import java.util.ArrayList;

import at.namart.model.bo.Rechnungszeile;

public interface IDALRechnungszeile {
	
	ArrayList<Rechnungszeile> getRechnungszeileByIdList(int AusgangsrechnungsId) throws DALException;
	Rechnungszeile saveRechnungszeile(Rechnungszeile a) throws DALException;
	boolean deleteRechnungszeile(Rechnungszeile a) throws DALException;

}
