package at.namart.model.services;

import java.util.ArrayList;

import at.namart.model.bo.Eingangsrechnung;

public interface IDALBuchungszeilenEingangsrechnung {

	ArrayList<Eingangsrechnung> getBuchungszeileEingangsrechnungList(int buchungszeile_id) throws DALException;
	Eingangsrechnung saveBuchungszeileEingangsrechnung(int buchungszeile_id, Eingangsrechnung a) throws DALException;
	boolean deleteBuchungszeileEingangsrechnung(int buchungszeile_id, Eingangsrechnung a) throws DALException;
	ArrayList<Eingangsrechnung> getNotUsedEingangsrechnungList(int buchungszeile_id) throws DALException;
}
