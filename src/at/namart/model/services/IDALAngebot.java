package at.namart.model.services;

import java.util.ArrayList;

import at.namart.model.bo.Angebot;


public interface IDALAngebot {

	ArrayList<Angebot> getAngebotList() throws DALException;
	Angebot saveAngebot(Angebot a) throws DALException;
	boolean deleteAngebot(Angebot a) throws DALException;
}
