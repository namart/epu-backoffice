package at.namart.model.services;

import java.util.ArrayList;

import at.namart.model.bo.Ausgangsrechnung;

public interface IDALBuchungszeilenAusgangsrechnung {

	ArrayList<Ausgangsrechnung> getBuchungszeileAusgangsrechnungList(int buchungszeile_id) throws DALException;
	Ausgangsrechnung saveBuchungszeileAusgangsrechnung(int buchungszeile_id, Ausgangsrechnung a) throws DALException;
	boolean deleteBuchungszeileAusgangsrechnung(int buchungszeile_id, Ausgangsrechnung a) throws DALException;
	ArrayList<Ausgangsrechnung> getNotUsedAusgangsrechnungList(int buchungszeile_id) throws DALException;
}
