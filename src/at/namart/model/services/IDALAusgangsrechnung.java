package at.namart.model.services;

import java.util.ArrayList;

import at.namart.model.bo.Ausgangsrechnung;

public interface IDALAusgangsrechnung {

	ArrayList<Ausgangsrechnung> getAusgangsrechnungList() throws DALException;
	Ausgangsrechnung saveAusgangsrechnung(Ausgangsrechnung a) throws DALException;
	boolean deleteAusgangsrechnung(Ausgangsrechnung a) throws DALException;
}
