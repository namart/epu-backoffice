package at.namart.model.services;

import java.util.ArrayList;

import at.namart.model.bo.Kontakt;

public interface IDALKontakt {

	ArrayList<Kontakt> getKontaktList() throws DALException;
	Kontakt saveKontakt(Kontakt k) throws DALException;
	boolean deleteKontakt(Kontakt k) throws DALException;
}
