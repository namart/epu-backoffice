package at.namart.model.services;

import java.util.ArrayList;

import at.namart.model.bo.Eingangsrechnung;

public interface IDALEingangsrechnung {

	ArrayList<Eingangsrechnung> getEingangsrechnungList() throws DALException;
	Eingangsrechnung saveEingangsrechnung(Eingangsrechnung a) throws DALException;
	boolean deleteEingangsrechnung(Eingangsrechnung a) throws DALException;
}
