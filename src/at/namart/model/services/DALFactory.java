package at.namart.model.services;

import at.namart.lib.ConfigLoader;


public class DALFactory {
	
	private static ConfigLoader config = ConfigLoader.getInstance();
	
	public static DALDatabase getDAL() throws DALException {
		
		return new DALDatabase(config.getProperty("user"), config.getProperty("password"), config.getProperty("url"));
	}
}
