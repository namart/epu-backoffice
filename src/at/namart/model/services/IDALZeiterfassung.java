package at.namart.model.services;

import java.util.ArrayList;

import at.namart.model.bo.Zeit;

public interface IDALZeiterfassung {

	ArrayList<Zeit> getZeiterfassungList() throws DALException;
	Zeit saveZeiterfassung(Zeit z) throws DALException;
	boolean deleteZeiterfassung(Zeit z) throws DALException;
}
