package at.namart.model.services;

import java.util.ArrayList;

import at.namart.model.bo.Buchungszeile;
import at.namart.model.bo.Kategorie;

public interface IDALBuchungszeile {

	ArrayList<Buchungszeile> getBuchungszeileList() throws DALException;
	Buchungszeile saveBuchungszeile(Buchungszeile b) throws DALException;
	boolean deleteBuchungszeile(Buchungszeile b) throws DALException;
	public ArrayList<Kategorie> getKategorien() throws DALException;
}
