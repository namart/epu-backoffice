package at.namart.model;

import java.util.ArrayList;

import at.namart.lib.Model;
import at.namart.lib.NotiEvent;
import at.namart.model.bo.Eingangsrechnung;
import at.namart.model.bo.Kunde;
import at.namart.model.services.DALException;
import at.namart.model.services.DALFactory;
import at.namart.model.services.IDALEingangsrechnung;

public class EingangsrechnungModel extends Model implements IDALEingangsrechnung {

	//Notify Names
	public final String GET_LIST = "getList";
	public final String ITEM_CHANGE = "itemChange";
	public final String DELETE_ITEM = "deleteItem";
	public final String KONTAKT_LIST = "kontaktList";
	
	//SingletonClass
	private static final EingangsrechnungModel instance = new EingangsrechnungModel();
	public static EingangsrechnungModel getInstance() {
		return instance;
	}
	
	@Override
	public ArrayList<Eingangsrechnung> getEingangsrechnungList() throws DALException {
		notifyView( new NotiEvent( DALFactory.getDAL().getEingangsrechnungList(), this.GET_LIST) );
		return null;
	}

	@Override
	public Eingangsrechnung saveEingangsrechnung(Eingangsrechnung e) throws DALException {
		notifyView( new NotiEvent( DALFactory.getDAL().saveEingangsrechnung(e), this.ITEM_CHANGE) );
		return null;
	}

	@Override
	public boolean deleteEingangsrechnung(Eingangsrechnung e) throws DALException {
		if(DALFactory.getDAL().deleteEingangsrechnung(e)) {
			notifyView( new NotiEvent( e, this.DELETE_ITEM) );
			return true;
		} else {
			return false;
		}
	}
	
	public ArrayList<Kunde> getKontaktList() throws DALException {
		notifyView( new NotiEvent( DALFactory.getDAL().getKontaktList(), this.KONTAKT_LIST) );
		return null;
	}

}
