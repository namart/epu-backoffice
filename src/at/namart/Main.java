package at.namart;

import java.awt.Dimension;
import java.awt.Toolkit;

import javax.swing.JFrame;

import at.namart.controller.AngebotController;
import at.namart.controller.AusgangsrechnungController;
import at.namart.controller.BuchungszeileController;
import at.namart.controller.EingangsrechnungController;
import at.namart.controller.KontaktController;
import at.namart.controller.KundeController;
import at.namart.controller.ProjektController;
import at.namart.controller.ZeiterfassungController;
import at.namart.view.AngebotView;
import at.namart.view.ApplicationView;
import at.namart.view.AusgangsrechnungView;
import at.namart.view.BuchungszeileView;
import at.namart.view.EingangsrechnungView;
import at.namart.view.KontaktView;
import at.namart.view.KundeView;
import at.namart.view.ProjektView;
import at.namart.view.ZeiterfassungView;


public class Main {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
	

		ApplicationView view = new ApplicationView("EPU-Backoffice");
		view.pack();
		view.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		view.setSize(900, 500);
		Dimension d = Toolkit.getDefaultToolkit().getScreenSize();
		view.setLocation((d.width - view.getSize().width) / 2,
				(d.height - view.getSize().height) / 2);
		view.setVisible(true);
		
		//Projektverwaltung
		ProjektController pc = new ProjektController(new ProjektView());
		view.addTab("Projekte", pc);
		
		//Projektverwaltung
		ZeiterfassungController zc = new ZeiterfassungController(new ZeiterfassungView());
		view.addTab("Zeiterfassung", zc);
		
		//Kundenverwaltung
		KundeController kuc = new KundeController(new KundeView());
		view.addTab("Kunden", kuc);
		
		//Angebotverwaltung
		AngebotController ac = new AngebotController(new AngebotView());
		view.addTab("Angebote", ac);

		//Kontaktverwaltung
		KontaktController koc = new KontaktController(new KontaktView());
		view.addTab("Kontakte", koc);
		
		//Ausgangsrechnungverwaltung
		AusgangsrechnungController arc = new AusgangsrechnungController(new AusgangsrechnungView());
		view.addTab("Ausgangsrechnungen", arc);
		
		//Eingangsrechnungverwaltung
		EingangsrechnungController erc = new EingangsrechnungController(new EingangsrechnungView());
		view.addTab("Eingangsrechnungen", erc);
		
		//Buchungszeilenverwaltung
		BuchungszeileController bc = new BuchungszeileController(new BuchungszeileView());
		view.addTab("Buchungszeilen", bc);
	}

}
