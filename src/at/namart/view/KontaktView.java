package at.namart.view;

import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.util.ArrayList;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import at.namart.lib.Model;
import at.namart.lib.NotiEvent;
import at.namart.lib.binder.RequiredRule;
import at.namart.model.KontaktModel;
import at.namart.model.bo.Kontakt;
import at.namart.view.components.VerwaltungView;
import at.namart.view.components.tables.KontaktTableModel;

public class KontaktView extends VerwaltungView<Kontakt, KontaktTableModel, KontaktModel> {
	
	//Felder
	JTextField fieldNr;
	JTextField fieldVorname;
	JTextField fieldNachname;
	JTextField fieldAdresse;
	JTextField fieldTelefon;
	JTextField fieldEmail;
	
	public KontaktView() {
		
		super(new Kontakt(), new KontaktTableModel(new String[]{"Nr.", "Vorname", "Nachname", "Adresse", "Telefon", "Email"}, new ArrayList<Kontakt>()), KontaktModel.getInstance());
		
		//Sich beim model registrieren
		this.model.addProduktListener(this);
		
		//Form
        JPanel InputForm = new JPanel();
		InputForm.setLayout( new BoxLayout(InputForm, BoxLayout.Y_AXIS) );
		InputForm.setBorder(BorderFactory.createEmptyBorder(10, 20, 10, 20));
		add(InputForm);
		
		//InputFields
		JPanel InputFields = new JPanel();
		InputFields.setLayout( new GridLayout(3,2) );
		InputForm.add(InputFields);
		
		InputFields.add( new JLabel("Nr.") );
		fieldNr = new JTextField(20);
		fieldNr.setEditable(false);
		InputFields.add(fieldNr);
		
		InputFields.add( new JLabel("Vorname") );
		fieldVorname = new JTextField(20);
		InputFields.add(fieldVorname);
		
		InputFields.add( new JLabel("Nachname") );
		fieldNachname = new JTextField(20);
		InputFields.add(fieldNachname);
		
		InputFields.add( new JLabel("Adresse") );
		fieldAdresse = new JTextField(20);
		InputFields.add(fieldAdresse);

		InputFields.add( new JLabel("Telefon") );
		fieldTelefon = new JTextField(20);
		InputFields.add(fieldTelefon);
		
		InputFields.add( new JLabel("Email") );
		fieldEmail = new JTextField(20);
		InputFields.add(fieldEmail);
		
		//Buttons
		JPanel ButtonForm = new JPanel();
		add(ButtonForm);
		ButtonForm.setLayout(new FlowLayout());
		//Create Buttons
		ButtonForm.add(this.newButton);
		ButtonForm.add(this.saveButton);
		ButtonForm.add(this.deleteButton);
		ButtonForm.add(this.refreshButton);
	}
	
	/**
	* F�ngt Meldungen vom Model auf
	*/
	@Override
	public void change(NotiEvent e) {
		
		// GET_LIST - The list has changed
		if(e.getName() == model.GET_LIST ) {
			this.tableModel.setList((ArrayList<Kontakt>)e.getSource());
		}
		
		else if(e.getName() == model.ITEM_CHANGE) {
			
			int row = this.tableModel.findObjectRow((Kontakt) e.getSource());
			if(row == -1) {
				this.tableModel.addObject((Kontakt) e.getSource());
				row = this.tableModel.getObjectIndex((Kontakt) e.getSource());
				this.table.setRowSelectionInterval(row, row);
			} else {
				this.tableModel.setObjectAt((Kontakt) e.getSource(), row);
				this.table.setRowSelectionInterval(row, row);
			}	
		}
		
		else if(e.getName() == model.DELETE_ITEM) {
			this.tableModel.removeObject((Kontakt) e.getSource());
		}
	}

	@Override
	public void setComponent(Kontakt k) {
		
		if(k == null) {
			component = new Kontakt();
			
			//Damit markierung verschwindet
			this.tableModel.fireTableDataChanged();
			myBinder.resetErrorControl();
        }
        else {
        	component = k;
        }
        BindTo();
	}
	
	/**
	* Bindet das "Kunde" an die GUI
	*/
	public void BindTo() {
		if(component.getId() != Model.INVALIDID){
			fieldNr.setText(Integer.toString(component.getId()));
		} else {
			fieldNr.setText("");
		}
		
		fieldVorname.setText(component.getVorname());
		fieldNachname.setText(component.getNachname());
		fieldAdresse.setText(component.getAdresse());
		fieldTelefon.setText(Integer.toString(component.getTelefon()));
		fieldEmail.setText(component.getEmail());
	}
	
	/**
	* Bindet die GUI an das "Kunde"
	*/
	public boolean BindFrom() {
		
		if(component == null) {
			component = new Kontakt();
		}
		
		myBinder.resetErrorControl();
		if(fieldNr.getText().isEmpty()) {
			component.setId(-1);
		} else {
			component.setId(myBinder.BindFrom_Int(fieldNr, new RequiredRule()));
		}
		
		component.setVorname(myBinder.BindFrom_String (fieldVorname, new RequiredRule()));
		component.setNachname(myBinder.BindFrom_String (fieldNachname, new RequiredRule()));
		component.setAdresse(myBinder.BindFrom_String (fieldAdresse, new RequiredRule()));
		component.setTelefon(myBinder.BindFrom_Telefon(fieldTelefon, new RequiredRule()));
		component.setEmail(myBinder.BindFrom_Email (fieldEmail, new RequiredRule()));
		
		if(myBinder.hasError()) {
			return false;
		} else {
			return true;
		}
	}
	
	/* GETTER AND SETTER FOR UNIT-TESTS*/

	public JTextField getFieldVorname() {
		return fieldVorname;
	}

	public JTextField getFieldNachname() {
		return fieldNachname;
	}

	public JTextField getFieldAdresse() {
		return fieldAdresse;
	}

	public JTextField getFieldTelefon() {
		return fieldTelefon;
	}

	public JTextField getFieldEmail() {
		return fieldEmail;
	}
	
	

}
