package at.namart.view;

import java.awt.FlowLayout;
import java.util.ArrayList;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import at.namart.lib.NotiEvent;
import at.namart.lib.logger.Logger;
import at.namart.model.BuchungszeileAusgangsrechnungModel;
import at.namart.model.bo.Ausgangsrechnung;
import at.namart.view.components.VerwaltungView;
import at.namart.view.components.tables.AusgangsrechnungTableModel;
import at.namart.view.components.tables.MyDefaultTableModel;

public class BuchungszeileAusgangsrechnungView extends VerwaltungView<Ausgangsrechnung, AusgangsrechnungTableModel, BuchungszeileAusgangsrechnungModel> {

	private JTable tableAuswahl;
	protected AusgangsrechnungTableModel tableModelAuswahl;
	
	public BuchungszeileAusgangsrechnungView() {
		
		super(new Ausgangsrechnung(), new AusgangsrechnungTableModel(new String[]{"Nr.", "Name"}, new ArrayList<Ausgangsrechnung>()), BuchungszeileAusgangsrechnungModel.getInstance());
		
		//Sich beim model registrieren
		this.model.addProduktListener(this);
		
		//Buttons
		JPanel ButtonForm1 = new JPanel();
		add(ButtonForm1);
		ButtonForm1.setLayout(new FlowLayout());
		//Create Buttons
		ButtonForm1.add(this.deleteButton);
		
		add( new JLabel("Ausgangsrechnungen") );
	
		// Das JTable initialisieren
		tableModelAuswahl = new AusgangsrechnungTableModel(new String[]{"Nr.", "Name"}, new ArrayList<Ausgangsrechnung>());
		tableAuswahl = new JTable(tableModelAuswahl);
		tableAuswahl.setRowHeight(20);
		tableAuswahl.setColumnSelectionAllowed(false);
		tableAuswahl.setRowSelectionAllowed(true);
        JScrollPane cp = new JScrollPane( tableAuswahl );
        add(cp);
        
        tableAuswahl.getSelectionModel().setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		
		//Buttons
		JPanel ButtonForm2 = new JPanel();
		add(ButtonForm2);
		ButtonForm2.setLayout(new FlowLayout());
		//Create Buttons
		this.saveButton.setText("Hinzuf�gen");
		ButtonForm2.add(this.saveButton);
		ButtonForm2.add(this.refreshButton);
	}
	
	/**
	* F�ngt Meldungen vom Model auf
	*/
	@Override
	public void change(NotiEvent e) {
		
		// GET_LIST - The list has changed
		if(e.getName() == model.GET_LIST ) {
			this.tableModel.setList((ArrayList<Ausgangsrechnung>)e.getSource());
			
		}
		
		else if(e.getName() == model.ITEM_CHANGE) {
			
			int row = this.tableModel.findObjectRow((Ausgangsrechnung) e.getSource());
			if(row == -1) {
				this.tableModel.addObject((Ausgangsrechnung) e.getSource());
				row = this.tableModel.getObjectIndex((Ausgangsrechnung) e.getSource());
				this.table.setRowSelectionInterval(row, row);
			} else {
				this.tableModel.setObjectAt((Ausgangsrechnung) e.getSource(), row);
				this.table.setRowSelectionInterval(row, row);
			}	
		}
		
		else if(e.getName() == model.DELETE_ITEM) {
			this.tableModel.removeObject((Ausgangsrechnung) e.getSource());
		}
		
		else if(e.getName() == model.GET_AUSGANGSRECHNUNGLIST) {
			this.tableModelAuswahl.setList((ArrayList<Ausgangsrechnung>)e.getSource());
		}

	}

	@Override
	public void setComponent(Ausgangsrechnung c) {
		
		if(c == null) {
			component = new Ausgangsrechnung();
			
			//Damit markierung verschwindet
			this.tableModel.fireTableDataChanged();
			myBinder.resetErrorControl();

        }
        else {
        	
        	component = c;
        }
		
	}
	
	public Ausgangsrechnung getAuswahlComponent() {
		return this.tableModelAuswahl.getObjectAt(this.tableAuswahl.getSelectedRow());
	}
}
