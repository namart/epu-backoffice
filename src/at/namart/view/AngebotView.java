package at.namart.view;

import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import javax.swing.*;

import at.namart.lib.Model;
import at.namart.lib.NotiEvent;
import at.namart.lib.binder.RequiredRule;
import at.namart.lib.logger.Logger;
import at.namart.model.AngebotModel;
import at.namart.model.bo.Angebot;
import at.namart.model.bo.Kunde;
import at.namart.model.bo.Projekt;
import at.namart.view.components.VerwaltungView;
import at.namart.view.components.combobox.KundeComboBoxModel;
import at.namart.view.components.combobox.ProjektComboBoxModel;
import at.namart.view.components.tables.AngebotTableModel;


public class AngebotView extends VerwaltungView<Angebot,AngebotTableModel,AngebotModel> {

	//Felder
	JTextField fieldNr;
	JTextField fieldName;
	JTextField fieldSumme;
	JTextField fieldDauer;
	JTextField fieldDatum;
	JTextField fieldChance;
	
	JComboBox fieldProjektCombo; 
	ProjektComboBoxModel fieldProjektComboModel; 
	
	JComboBox fieldKundeCombo; 
	KundeComboBoxModel fieldKundeComboModel; 
	
	public AngebotView() {
		
		super(new Angebot(), new AngebotTableModel(new String[]{"Nr.", "Name", "Summe", "Dauer", "Datum", "chance"}, new ArrayList<Angebot>()), AngebotModel.getInstance());
		
		//Sich beim model registrieren
		this.model.addProduktListener(this);
        
        //Form
        JPanel InputForm = new JPanel();
		InputForm.setLayout( new BoxLayout(InputForm, BoxLayout.Y_AXIS) );
		InputForm.setBorder(BorderFactory.createEmptyBorder(10, 20, 10, 20));
		add(InputForm);
		
		//InputFields
		JPanel InputFields = new JPanel();
		InputFields.setLayout( new GridLayout(8,2) );
		InputForm.add(InputFields);
		
		InputFields.add( new JLabel("Nr.") );
		fieldNr = new JTextField(20);
		fieldNr.setEditable(false);
		InputFields.add(fieldNr);
		
		InputFields.add( new JLabel("Name") );
		fieldName = new JTextField(20);
		InputFields.add(fieldName);
		
		InputFields.add( new JLabel("Summe") );
		fieldSumme = new JTextField(20);
		InputFields.add(fieldSumme);
		
		InputFields.add( new JLabel("Dauer") );
		fieldDauer = new JTextField(20);
		InputFields.add(fieldDauer);
		
		InputFields.add( new JLabel("Datum") );
		fieldDatum = new JTextField(20);
		InputFields.add(fieldDatum);
		
		InputFields.add( new JLabel("Chance") );
		fieldChance = new JTextField(20);
		InputFields.add(fieldChance);
		
		InputFields.add( new JLabel("Projekt") );
		fieldProjektCombo = new JComboBox();
		fieldProjektComboModel = new ProjektComboBoxModel(new ArrayList<Projekt>());
		InputFields.add(fieldProjektCombo);
		fieldProjektCombo.setModel(fieldProjektComboModel);
		
		InputFields.add( new JLabel("Kunde") );
		fieldKundeCombo = new JComboBox();
		fieldKundeComboModel = new KundeComboBoxModel(new ArrayList<Kunde>());
		InputFields.add(fieldKundeCombo);
		fieldKundeCombo.setModel(fieldKundeComboModel);

        //Buttons
		JPanel ButtonForm = new JPanel();
		add(ButtonForm);
		ButtonForm.setLayout(new FlowLayout());
		//Create Buttons
		ButtonForm.add(this.newButton);
		ButtonForm.add(this.saveButton);
		ButtonForm.add(this.deleteButton);
		ButtonForm.add(this.refreshButton);
 
	}
	
	/**
	* F�ngt Meldungen vom Model auf
	*/
	@Override
	public void change(NotiEvent e) {
		
		// GET_LIST - The list has changed
		if(e.getName() == model.GET_LIST ) {
			//Logger.trace(((ArrayList<Angebot>)e.getSource()).get(1).getChance());
			this.tableModel.setList((ArrayList<Angebot>)e.getSource());
		}
		
		else if(e.getName() == model.ITEM_CHANGE) {
			
			int row = this.tableModel.findObjectRow((Angebot) e.getSource());
			if(row == -1) {
				this.tableModel.addObject((Angebot) e.getSource());
				row = this.tableModel.getObjectIndex((Angebot) e.getSource());
				this.table.setRowSelectionInterval(row, row);
			} else {
				this.tableModel.setObjectAt((Angebot) e.getSource(), row);
				this.table.setRowSelectionInterval(row, row);
			}	
		}
		
		else if(e.getName() == model.DELETE_ITEM) {
			this.tableModel.removeObject((Angebot) e.getSource());
		}
		
		else if (e.getName() == model.PROJEKT_LIST) {
			fieldProjektComboModel.setList((ArrayList<Projekt>)e.getSource());
		}
		
		else if (e.getName() == model.KUNDE_LIST) {
			fieldKundeComboModel.setList((ArrayList<Kunde>)e.getSource());
		}
	}
	
	/**
	* Setz das "Angebot" und bindet es anschlie�end an die GUI
	*/
	public void setComponent(Angebot a) {
		
		if(a == null) {
			component = new Angebot();
			
			//Damit markierung verschwindet
			this.tableModel.fireTableDataChanged();
			myBinder.resetErrorControl();
        }
        else {
        	component = a;
        }
        BindTo();

	}
	
	/**
	* Bindet das "Angebot" an die GUI
	*/
	public void BindTo() {
		
		if(component.getId() != Model.INVALIDID){
			fieldNr.setText(Integer.toString(component.getId()));
			fieldDatum.setText(new SimpleDateFormat("dd/MM/yyyy").format(component.getDatum()));
		} else {
			fieldNr.setText("");
			fieldDatum.setText("dd/MM/yyyy");	
		}
		
		fieldName.setText(component.getName());
		fieldSumme.setText(Integer.toString(component.getSumme()));
		fieldDauer.setText(Integer.toString(component.getDauer()));
		fieldChance.setText(Integer.toString(component.getChance()));
		
		//Gew�hltes Projekt an die GUi binden
		//Logger.trace(component.getProjektId());
		if(component.getProjektId() != Model.UNDEFINED) {
			fieldProjektComboModel.setSelectedItemById(component.getProjektId());
		} else {
			fieldProjektComboModel.setSelectedItemById(Model.UNDEFINED);
		}
		
		//Gew�hlter Kunde an die GUi binden
		if(component.getKundenId() != Model.UNDEFINED) {
			fieldKundeComboModel.setSelectedItemById(component.getKundenId());
		} else {
			fieldKundeComboModel.setSelectedItemById(Model.UNDEFINED);
		}
		
		//Logger.trace(fieldKundeComboModel.getSelectedItem());
	}
	
	/**
	* Bindet die GUI an das "Angebot"
	*/
	public boolean BindFrom() {
		
		if(component == null) {
			component = new Angebot();
		}
		
		myBinder.resetErrorControl();
		if(fieldNr.getText().isEmpty()) {
			component.setId(-1);
		} else {
			component.setId(myBinder.BindFrom_Int(fieldNr, new RequiredRule()));
		}
		
		component.setName(myBinder.BindFrom_String (fieldName, new RequiredRule()));
		component.setSumme(myBinder.BindFrom_Int(fieldSumme, new RequiredRule()));
		component.setDauer(myBinder.BindFrom_Minus(fieldDauer, new RequiredRule()));
		component.setDatum(myBinder.BindFrom_Date(fieldDatum, new RequiredRule()));
		component.setChance(myBinder.BindFrom_Percent(fieldChance, new RequiredRule()));
		
		//Ausgew�hltes Projekt in die Komponente
		Projekt p = fieldProjektComboModel.getSelectedItem();
		if(p != null) {
			component.setProjektId(p.getId());
		} else {
			component.setProjektId(Model.UNDEFINED);
		}
		
		//Ausgew�hlter Kunde in die Komponente
		Kunde k = fieldKundeComboModel.getSelectedItem();
		if(k != null) {
			component.setKundenId(k.getId());
		} else {
			component.setKundenId(Model.UNDEFINED);
		}
		
		if(myBinder.hasError()) {
			return false;
		} else {
			return true;
		}
	}
	
	/* GETTER AND SETTER FOR UNIT-TESTS*/

	public JTextField getFieldName() {
		return fieldName;
	}

	public JTextField getFieldSummer() {
		return fieldSumme;
	}

	public JTextField getFieldDauer() {
		return fieldDauer;
	}

	public JTextField getFieldDatum() {
		return fieldDatum;
	}

	public JTextField getFieldChance() {
		return fieldChance;
	}

	public ProjektComboBoxModel getFieldProjektComboModel() {
		return fieldProjektComboModel;
	}

	public KundeComboBoxModel getFieldKundeComboModel() {
		return fieldKundeComboModel;
	}
	
	
}
