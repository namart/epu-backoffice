package at.namart.view;

import java.awt.FlowLayout;
import java.util.ArrayList;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;

import at.namart.lib.NotiEvent;
import at.namart.model.BuchungszeileEingangsrechnungModel;
import at.namart.model.bo.Eingangsrechnung;
import at.namart.view.components.VerwaltungView;
import at.namart.view.components.tables.EingangsrechnungTableModel;

public class BuchungszeileEingangsrechnungView extends VerwaltungView<Eingangsrechnung, EingangsrechnungTableModel, BuchungszeileEingangsrechnungModel> {

	private JTable tableAuswahl;
	protected EingangsrechnungTableModel tableModelAuswahl;
	
	public BuchungszeileEingangsrechnungView() {
		
		super(new Eingangsrechnung(), new EingangsrechnungTableModel(new String[]{"Nr.", "Name"}, new ArrayList<Eingangsrechnung>()), BuchungszeileEingangsrechnungModel.getInstance());
		
		//Sich beim model registrieren
		this.model.addProduktListener(this);
		
		//Buttons
		JPanel ButtonForm1 = new JPanel();
		add(ButtonForm1);
		ButtonForm1.setLayout(new FlowLayout());
		//Create Buttons
		ButtonForm1.add(this.deleteButton);
		
		add( new JLabel("Eingangsrechnungen") );
	
		// Das JTable initialisieren
		tableModelAuswahl = new EingangsrechnungTableModel(new String[]{"Nr.", "Name"}, new ArrayList<Eingangsrechnung>());
		tableAuswahl = new JTable(tableModelAuswahl);
		tableAuswahl.setRowHeight(20);
		tableAuswahl.setColumnSelectionAllowed(false);
		tableAuswahl.setRowSelectionAllowed(true);
        JScrollPane cp = new JScrollPane( tableAuswahl );
        add(cp);
        
        tableAuswahl.getSelectionModel().setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		
		//Buttons
		JPanel ButtonForm2 = new JPanel();
		add(ButtonForm2);
		ButtonForm2.setLayout(new FlowLayout());
		//Create Buttons
		this.saveButton.setText("Hinzuf�gen");
		ButtonForm2.add(this.saveButton);
		ButtonForm2.add(this.refreshButton);
	}
	
	/**
	* F�ngt Meldungen vom Model auf
	*/
	@Override
	public void change(NotiEvent e) {
		
		// GET_LIST - The list has changed
		if(e.getName() == model.GET_LIST ) {
			this.tableModel.setList((ArrayList<Eingangsrechnung>)e.getSource());
			
		}
		
		else if(e.getName() == model.ITEM_CHANGE) {
			
			int row = this.tableModel.findObjectRow((Eingangsrechnung) e.getSource());
			if(row == -1) {
				this.tableModel.addObject((Eingangsrechnung) e.getSource());
				row = this.tableModel.getObjectIndex((Eingangsrechnung) e.getSource());
				this.table.setRowSelectionInterval(row, row);
			} else {
				this.tableModel.setObjectAt((Eingangsrechnung) e.getSource(), row);
				this.table.setRowSelectionInterval(row, row);
			}	
		}
		
		else if(e.getName() == model.DELETE_ITEM) {
			this.tableModel.removeObject((Eingangsrechnung) e.getSource());
		}
		
		else if(e.getName() == model.GET_EINGANGSRECHNUNGLIST) {
			this.tableModelAuswahl.setList((ArrayList<Eingangsrechnung>)e.getSource());
		}

	}

	@Override
	public void setComponent(Eingangsrechnung c) {
		
		if(c == null) {
			component = new Eingangsrechnung();
			
			//Damit markierung verschwindet
			this.tableModel.fireTableDataChanged();
			myBinder.resetErrorControl();

        }
        else {
        	
        	component = c;
        }
		
	}
	
	public Eingangsrechnung getAuswahlComponent() {
		return this.tableModelAuswahl.getObjectAt(this.tableAuswahl.getSelectedRow());
	}
}
