package at.namart.view;

import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.util.ArrayList;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import at.namart.lib.Model;
import at.namart.lib.NotiEvent;
import at.namart.lib.binder.RequiredRule;
import at.namart.model.RechnungszeileModel;
import at.namart.model.bo.Angebot;
import at.namart.model.bo.Rechnungszeile;
import at.namart.view.components.VerwaltungView;
import at.namart.view.components.combobox.AngebotComboBoxModel;
import at.namart.view.components.tables.RechnungszeileTableModel;

public class RechnungszeilenView extends VerwaltungView<Rechnungszeile, RechnungszeileTableModel, RechnungszeileModel> {
	
	//Felder
	JTextField fieldNr;
	JTextField fieldName;
	
	JComboBox fieldAngebotCombo; 
	AngebotComboBoxModel fieldAngebotComboModel;

	public RechnungszeilenView(){
		
		super(new Rechnungszeile(), new RechnungszeileTableModel(new String[]{"Nr.", "Name"}, new ArrayList<Rechnungszeile>()), RechnungszeileModel.getInstance());
		
		//Sich beim model registrieren
		this.model.addProduktListener(this);
		
		//Form
        JPanel InputForm = new JPanel();
		InputForm.setLayout( new BoxLayout(InputForm, BoxLayout.Y_AXIS) );
		InputForm.setBorder(BorderFactory.createEmptyBorder(10, 20, 10, 20));
		add(InputForm);
		
		//InputFields
		JPanel InputFields = new JPanel();
		InputFields.setLayout( new GridLayout(3,2) );
		InputForm.add(InputFields);
		
		InputFields.add( new JLabel("Nr.") );
		fieldNr = new JTextField(20);
		fieldNr.setEditable(false);
		InputFields.add(fieldNr);
		
		InputFields.add( new JLabel("Name") );
		fieldName = new JTextField(20);
		InputFields.add(fieldName);
		
		InputFields.add( new JLabel("Angebot") );
		fieldAngebotCombo = new JComboBox();
		fieldAngebotComboModel = new AngebotComboBoxModel(new ArrayList<Angebot>());
		InputFields.add(fieldAngebotCombo);
		fieldAngebotCombo.setModel(fieldAngebotComboModel);
		
		//Buttons
		JPanel ButtonForm = new JPanel();
		add(ButtonForm);
		ButtonForm.setLayout(new FlowLayout());
		//Create Buttons
		ButtonForm.add(this.newButton);
		ButtonForm.add(this.saveButton);
		ButtonForm.add(this.deleteButton);
		ButtonForm.add(this.refreshButton);
		
	}
	
	/**
	* F�ngt Meldungen vom Model auf
	*/
	@Override
	public void change(NotiEvent e) {
		
		// GET_LIST - The list has changed
		if(e.getName() == model.GET_LIST ) {
			this.tableModel.setList((ArrayList<Rechnungszeile>)e.getSource());
		}
		
		else if(e.getName() == model.ITEM_CHANGE) {
			
			int row = this.tableModel.findObjectRow((Rechnungszeile) e.getSource());
			if(row == -1) {
				this.tableModel.addObject((Rechnungszeile) e.getSource());
				row = this.tableModel.getObjectIndex((Rechnungszeile) e.getSource());
				this.table.setRowSelectionInterval(row, row);
			} else {
				this.tableModel.setObjectAt((Rechnungszeile) e.getSource(), row);
				this.table.setRowSelectionInterval(row, row);
			}	
		}
		
		else if(e.getName() == model.DELETE_ITEM) {
			this.tableModel.removeObject((Rechnungszeile) e.getSource());
		}
		
		else if (e.getName() == model.ANGEBOT_LIST) {
			fieldAngebotComboModel.setList((ArrayList<Angebot>)e.getSource());
		}
	}

	@Override
	public void setComponent(Rechnungszeile r) {
		
		if(r == null) {
			component = new Rechnungszeile();
			
			//Damit markierung verschwindet
			this.tableModel.fireTableDataChanged();
			myBinder.resetErrorControl();
        }
        else {
        	component = r;
        }
        BindTo();
		
	}
	
	/**
	* Bindet das "Angebot" an die GUI
	*/
	public void BindTo() {
		
		if(component.getId() != Model.INVALIDID){
			fieldNr.setText(Integer.toString(component.getId()));
		} else {
			fieldNr.setText("");
		}
		
		fieldName.setText(component.getName());
		
		//Gew�hlter Kunde an die GUi binden
		if(component.getAngebotId() != Model.UNDEFINED) {
			fieldAngebotComboModel.setSelectedItemById(component.getAngebotId());
		} else {
			fieldAngebotComboModel.setSelectedItemById(Model.UNDEFINED);
		}
	}
	
	/**
	* Bindet die GUI an das "Angebot"
	*/
	public boolean BindFrom() {
		
		if(component == null) {
			component = new Rechnungszeile();
		}
		
		myBinder.resetErrorControl();
		if(fieldNr.getText().isEmpty()) {
			component.setId(-1);
		} else {
			component.setId(myBinder.BindFrom_Int(fieldNr, new RequiredRule()));
		}
		
		component.setName(myBinder.BindFrom_String (fieldName, new RequiredRule()));
		
		//Ausgew�hlter Kunde in die Komponente
		Angebot a = fieldAngebotComboModel.getSelectedItem();
		if(a != null) {
			component.setAngebotId(a.getId());
		} else {
			component.setAngebotId(Model.UNDEFINED);
		}
		
		if(myBinder.hasError()) {
			return false;
		} else {
			return true;
		}
	}

}
