package at.namart.view;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.Toolkit;

import javax.swing.JFrame;



public class FormWrapperView<V> extends JFrame {
	
	V view;
	
	public FormWrapperView(V view) {
		
		this.view = view;
		
		this.pack();
		this.setSize(600, 500);
		Dimension d = Toolkit.getDefaultToolkit().getScreenSize();
		this.setLocation((d.width - this.getSize().width) / 2,
				(d.height - this.getSize().height) / 2);
		this.setVisible(true);
		
		this.add((Component) view);

	}
	
	public V getView() {
		return this.view;
	}

}
