package at.namart.view.components;

import java.awt.BorderLayout;
import java.awt.event.ActionListener;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.TableModel;

import at.namart.lib.View;
import at.namart.lib.binder.MyBinder;
import at.namart.lib.logger.Logger;
import at.namart.view.components.tables.MyDefaultTableModel;

public abstract class VerwaltungView<C,T,M> extends View {

	//Buttons
	protected JButton newButton = new JButton("Neu");
	protected JButton saveButton = new JButton("Speichern");
	protected JButton deleteButton = new JButton("L�schen");
	protected JButton refreshButton = new JButton("Refresh");
	
	//Binder
	protected MyBinder myBinder = new MyBinder();
	
	//Das Model 
	protected M model;
	
	//TabelleModel
	protected T tableModel;
	
	//Tabelle
	protected JTable table;
	
	//Der Typ mit dem gearbeitet wird
	protected C component;
	
	public VerwaltungView(C component, final T tableModel, M model) {
		
		//Sets the Components an instance
		this.component = component;
		this.model = model;
		this.tableModel = tableModel;
		
		setLayout( new BoxLayout(this, BoxLayout.Y_AXIS) );
		
		//ErroControl setzen
		add(myBinder.getErrorControl(),BorderLayout.NORTH);
		
        
        // Das JTable initialisieren
		table = new JTable( (TableModel) tableModel );
		table.setRowHeight(20);
		table.setColumnSelectionAllowed(false);
		table.setRowSelectionAllowed(true);
        JScrollPane cp = new JScrollPane( table );
        add(cp);
        
        table.getSelectionModel().setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        
        //Listener for selections
		ListSelectionListener listSelectionListener = new ListSelectionListener() {
			public void valueChanged(ListSelectionEvent event) {
				boolean adjust = event.getValueIsAdjusting();
				if (!adjust) {

					// Get the data model for this table
					MyDefaultTableModel<C> ltableModel = (MyDefaultTableModel<C>) table.getModel();
					
					int row = table.getSelectedRow();
					if(row != -1) {
						C component = ltableModel.getObjectAt(row);

						//in Form setzen
						setComponent(component);
					}
				}
			}
		};
		table.getSelectionModel().addListSelectionListener(listSelectionListener);
	}
	
	public void setTableSelectionListener(ListSelectionListener l) {
		table.getSelectionModel().addListSelectionListener(l);
	}
	
	/**
	* Setz die "Komponente" und bindet es anschlie�end an die GUI
	*/
	public abstract void setComponent(C c);
	
	/**
	* Gibt die "Komponente" zur�ck
	*/
	public C getComponent() {
		return component;
	}
	
	/**
	* Funktionen bereitstellen, mit denen man sp�ter aus
	* dem Controller die n�tigen Listener hinzuf�gen kann
	*/
	public void setNewButtonListener(ActionListener l){
		this.newButton.addActionListener(l);
	}
	
	public void setSaveButtonListener(ActionListener l){
		this.saveButton.addActionListener(l);
	}
	
	public void setDeleteButtonListener(ActionListener l){
		this.deleteButton.addActionListener(l);
	}	
	
	public void setRefreshButtonListener(ActionListener l){
		this.refreshButton.addActionListener(l);
	}
	
	public MyBinder getMyBinder() {
		return myBinder;
	}

}


