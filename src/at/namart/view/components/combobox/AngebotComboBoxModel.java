package at.namart.view.components.combobox;

import java.util.List;

import javax.swing.AbstractListModel;
import javax.swing.ComboBoxModel;

import at.namart.lib.logger.Logger;
import at.namart.model.bo.Angebot;

public class AngebotComboBoxModel extends AbstractListModel implements ComboBoxModel {
	
	protected List<Angebot> objectList;
	protected Angebot selection = null;
	
	public AngebotComboBoxModel(List<Angebot> objectList) {
		this.objectList = objectList;
	}
	
	public void setList(List<Angebot> objectList) {
		this.objectList = objectList;
		this.fireContentsChanged(this, 0, getSize()-1);
	}

	@Override
	public int getSize() {
		return objectList.size();
	}

	@Override
	public Object getElementAt(int index) {
		return objectList.get(index);
	}

	@Override
	public void setSelectedItem(Object anItem) {
		selection = (Angebot) anItem;
	}

	@Override
	public Angebot getSelectedItem() {
		return selection;
	}
	
	public void setSelectedItemById(int id) {
		for ( int i = 0; i < objectList.size(); i++ ) {
			if (objectList.get(i).getId() == id) {		
				setSelectedItem(objectList.get(i));
				this.fireContentsChanged(this, 0, getSize()-1);
				return;
			}
		}
		setSelectedItem(null);
		this.fireContentsChanged(this, 0, getSize()-1);
	}

}
