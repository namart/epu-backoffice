package at.namart.view.components.tables;


import java.util.List;

import javax.swing.table.AbstractTableModel;

import at.namart.model.bo.Projekt;

public abstract class MyDefaultTableModel<T> extends AbstractTableModel {
	
	private static final long serialVersionUID = -8813801681887559147L;
	
	protected String headerList[];
	protected List<T> objectList;
	
	public MyDefaultTableModel(String[] headerList, List<T> objectList) {
		super();
		this.headerList = headerList;
		this.objectList = objectList;
	}
	
	public MyDefaultTableModel(String[] headerList) {
		super();
		this.headerList = headerList;
	}

	@Override
	public int getRowCount() {
		if(objectList != null)
			return objectList.size();
		else
			return 0;
	}

	@Override
	public int getColumnCount() {
		return headerList.length;
	}
	
	public void addObject(T element) {
		this.objectList.add(element);
		this.fireTableDataChanged();
	}
	
	public void removeObject(T element) {
		this.objectList.remove(element);
		this.fireTableDataChanged();
	}
	
	public void setList(List<T> objectList) {
		this.objectList = objectList;
		this.fireTableDataChanged();
	}
	
	//This method will be used to display the name of columns
	public String getColumnName(int col) {
		return headerList[col];
	}
	
	public int getObjectIndex(T element) {
		return objectList.indexOf(element);
	}
	
	public T getObjectAt(int row) {
		return objectList.get(row);
	}
	
	public void setObjectAt(T element, int index) {
		objectList.set(index, element);
		this.fireTableDataChanged();
	}


}
