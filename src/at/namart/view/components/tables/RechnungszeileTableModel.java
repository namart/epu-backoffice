package at.namart.view.components.tables;

import java.util.List;

import at.namart.model.bo.Rechnungszeile;

public class RechnungszeileTableModel extends MyDefaultTableModel<Rechnungszeile> {
	
	public RechnungszeileTableModel(String[] headerList, List<Rechnungszeile> objectList) {
		super(headerList, objectList);
	}
	
	public RechnungszeileTableModel(String[] headerList) {
		super(headerList);
	}

	@Override
	public Object getValueAt(int row, int column) {
		
		Rechnungszeile entity = null;
		entity = (Rechnungszeile) objectList.get(row);

		switch (column) {

			case 0:
				return entity.getId();
			case 1:
				return entity.getName();
	
			default :
	
			return "";
			
		}
	}
	
	public int findObjectRow(Rechnungszeile element) {

		for ( int i = 0; i < objectList.size(); i++ ) {
			if (objectList.get(i).getId() == element.getId()) {
				return i;
			}
		}
		return -1;

	}

}
