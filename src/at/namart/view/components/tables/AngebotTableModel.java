package at.namart.view.components.tables;


import java.util.List;

import at.namart.model.bo.Angebot;


public class AngebotTableModel extends MyDefaultTableModel<Angebot> {

	public AngebotTableModel(String[] headerList, List<Angebot> objectList) {
		super(headerList, objectList);
	}
	
	public AngebotTableModel(String[] headerList) {
		super(headerList);
	}

	@Override
	public Object getValueAt(int row, int column) {
		
		Angebot entity = null;
		entity = (Angebot) objectList.get(row);

		switch (column) {

			case 0:
				return entity.getId();
			case 1:
				return entity.getName();
			case 2:
				return entity.getSumme();
			case 3:
				return entity.getDauer();
			case 4:
				return entity.getDatum();
			case 5:
				return entity.getChance();
			case 6:
				return entity.getProjektId();
			case 7:
				return entity.getKundenId();
	
			default :
	
			return "";
			
		}
	}
	
	public int findObjectRow(Angebot element) {

		for ( int i = 0; i < objectList.size(); i++ ) {
			if (objectList.get(i).getId() == element.getId()) {
				return i;
			}
		}
		return -1;

	}

}
