package at.namart.view.components.tables;

import java.util.List;

import at.namart.model.bo.Ausgangsrechnung;
import at.namart.model.bo.Eingangsrechnung;

public class EingangsrechnungTableModel extends MyDefaultTableModel<Eingangsrechnung> {
	
	public EingangsrechnungTableModel(String[] headerList, List<Eingangsrechnung> objectList) {
		super(headerList, objectList);
	}
	
	public EingangsrechnungTableModel(String[] headerList) {
		super(headerList);
	}

	@Override
	public Object getValueAt(int row, int column) {
		
		Eingangsrechnung entity = null;
		entity = (Eingangsrechnung) objectList.get(row);

		switch (column) {

			case 0:
				return entity.getId();
			case 1:
				return entity.getName();
	
			default :
	
			return "";
			
		}
	}
	
	public int findObjectRow(Eingangsrechnung element) {

		for ( int i = 0; i < objectList.size(); i++ ) {
			if (objectList.get(i).getId() == element.getId()) {
				return i;
			}
		}
		return -1;

	}

}
