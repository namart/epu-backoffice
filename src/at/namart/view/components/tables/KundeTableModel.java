package at.namart.view.components.tables;


import java.util.List;

import at.namart.model.bo.Kunde;


public class KundeTableModel extends MyDefaultTableModel<Kunde> {

	public KundeTableModel(String[] headerList, List<Kunde> objectList) {
		super(headerList, objectList);
	}
	
	public KundeTableModel(String[] headerList) {
		super(headerList);
	}

	@Override
	public Object getValueAt(int row, int column) {
		
		Kunde entity = null;
		entity = (Kunde) objectList.get(row);

		switch (column) {

			case 0:
			return entity.getId();
			case 1:
			return entity.getName();
			case 2:
			return entity.getUid();
	
			default :
	
			return "";
			
		}
	}
	
	public int findObjectRow(Kunde element) {

		for ( int i = 0; i < objectList.size(); i++ ) {
			if (objectList.get(i).getId() == element.getId()) {
				return i;
			}
		}
		return -1;

	}

}
