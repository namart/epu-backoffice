package at.namart.view.components.tables;


import java.util.List;

import at.namart.model.bo.Projekt;

public class ProjektTableModel extends MyDefaultTableModel<Projekt> {

	private static final long serialVersionUID = 1277017031136103765L;

	public ProjektTableModel(String[] headerList, List<Projekt> objectList) {
		super(headerList, objectList);
	}
	
	public ProjektTableModel(String[] headerList) {
		super(headerList);
	}

	@Override
	public Object getValueAt(int row, int column) {
		
		Projekt entity = null;
		entity = (Projekt) objectList.get(row);

		switch (column) {

			case 0:
			return entity.getId();
			case 1:
			return entity.getName();
	
			default :
	
			return "";
			
		}
	}
	
	public int findObjectRow(Projekt element) {

		for ( int i = 0; i < objectList.size(); i++ ) {
			if (objectList.get(i).getId() == element.getId()) {
				return i;
			}
		}
		return -1;

	}

}
