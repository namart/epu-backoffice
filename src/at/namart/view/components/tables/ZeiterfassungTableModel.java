package at.namart.view.components.tables;

import java.util.List;

import at.namart.model.bo.Zeit;

public class ZeiterfassungTableModel extends MyDefaultTableModel<Zeit> {

	public ZeiterfassungTableModel(String[] headerList, List<Zeit> objectList) {
		super(headerList, objectList);
	}
	
	public ZeiterfassungTableModel(String[] headerList) {
		super(headerList);
	}

	@Override
	public Object getValueAt(int row, int column) {
		
		Zeit entity = null;
		entity = (Zeit) objectList.get(row);

		switch (column) {

			case 0:
				return entity.getId();
			case 1:
				return entity.getDauer();
			case 2:
				return entity.getDatum();
			case 3:
				return entity.getProjektId();
	
			default :
	
			return "";
			
		}
	}
	
	public int findObjectRow(Zeit element) {

		for ( int i = 0; i < objectList.size(); i++ ) {
			if (objectList.get(i).getId() == element.getId()) {
				return i;
			}
		}
		return -1;

	}
}
