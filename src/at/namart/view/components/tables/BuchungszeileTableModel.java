package at.namart.view.components.tables;

import java.util.List;

import at.namart.model.bo.Buchungszeile;

public class BuchungszeileTableModel extends MyDefaultTableModel<Buchungszeile> {

	public BuchungszeileTableModel(String[] headerList, List<Buchungszeile> objectList) {
		super(headerList, objectList);
	}
	
	public BuchungszeileTableModel(String[] headerList) {
		super(headerList);
	}
	
	@Override
	public Object getValueAt(int row, int column) {
		
		Buchungszeile entity = null;
		entity = (Buchungszeile) objectList.get(row);

		switch (column) {

			case 0:
				return entity.getId();
			case 1:
				return entity.getName();
			case 2:
				return entity.getBetrag();
	
			default :
	
			return "";
			
		}
	}
	
	public int findObjectRow(Buchungszeile element) {

		for ( int i = 0; i < objectList.size(); i++ ) {
			if (objectList.get(i).getId() == element.getId()) {
				return i;
			}
		}
		return -1;

	}
}
