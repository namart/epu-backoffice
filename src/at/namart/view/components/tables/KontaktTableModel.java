package at.namart.view.components.tables;

import java.util.List;

import at.namart.model.bo.Kontakt;

public class KontaktTableModel extends MyDefaultTableModel<Kontakt> {

	public KontaktTableModel(String[] headerList, List<Kontakt> objectList) {
		super(headerList, objectList);
		// TODO Auto-generated constructor stub
	}
	
	public KontaktTableModel(String[] headerList) {
		super(headerList);
		// TODO Auto-generated constructor stub
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		
		Kontakt entity = null;
		entity = (Kontakt) objectList.get(rowIndex);

		switch (columnIndex) {

			case 0:
				return entity.getId();
			case 1:
				return entity.getVorname();
			case 2:
				return entity.getNachname();				
			case 3:
				return entity.getAdresse();
			case 4:
				return entity.getTelefon();
			case 5:
				return entity.getEmail();
	
			default :
	
			return "";
			
		}
	}
	
	public int findObjectRow(Kontakt element) {

		for ( int i = 0; i < objectList.size(); i++ ) {
			if (objectList.get(i).getId() == element.getId()) {
				return i;
			}
		}
		return -1;

	}

}
