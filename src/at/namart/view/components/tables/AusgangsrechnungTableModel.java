package at.namart.view.components.tables;

import java.util.List;

import at.namart.model.bo.Ausgangsrechnung;

public class AusgangsrechnungTableModel extends MyDefaultTableModel<Ausgangsrechnung> {
	
	public AusgangsrechnungTableModel(String[] headerList, List<Ausgangsrechnung> objectList) {
		super(headerList, objectList);
	}
	
	public AusgangsrechnungTableModel(String[] headerList) {
		super(headerList);
	}

	@Override
	public Object getValueAt(int row, int column) {
		
		Ausgangsrechnung entity = null;
		entity = (Ausgangsrechnung) objectList.get(row);

		switch (column) {

			case 0:
				return entity.getId();
			case 1:
				return entity.getName();
	
			default :
	
			return "";
			
		}
	}
	
	public int findObjectRow(Ausgangsrechnung element) {

		for ( int i = 0; i < objectList.size(); i++ ) {
			if (objectList.get(i).getId() == element.getId()) {
				return i;
			}
		}
		return -1;

	}

}
