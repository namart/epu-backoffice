package at.namart.view;

import java.awt.BorderLayout;
import java.awt.Component;
import java.util.ArrayList;

import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JTabbedPane;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import at.namart.lib.Controller;
import at.namart.lib.logger.Logger;


public class ApplicationView extends JFrame {
	
	private JTabbedPane tabbedPane;
	private ArrayList<Controller<?>> constrollerList = new ArrayList<Controller<?>>();

	public ApplicationView(String title) {
		
		super(title);
		
		this.setLayout(new BorderLayout());
		
		
		tabbedPane = new JTabbedPane();
		add(tabbedPane, BorderLayout.CENTER);
		
		tabbedPane.addChangeListener(new ChangeListener() {
		    // This method is called whenever the selected tab changes
		    public void stateChanged(ChangeEvent evt) {
		        JTabbedPane tabbedPane = (JTabbedPane)evt.getSource();

		        // Get current tab
		        int index = tabbedPane.getSelectedIndex();
		        constrollerList.get(index).tabChange();
		        
		    }
		});

	}
	
	public void addTab(String title, Controller<?> component) {
		constrollerList.add(component);
		tabbedPane.addTab(title, (Component) component.getView());
	}

}
