package at.namart.view;

import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import at.namart.lib.Model;
import at.namart.lib.NotiEvent;
import at.namart.lib.binder.RequiredRule;
import at.namart.model.AngebotModel;
import at.namart.model.ZeiterfassungModel;
import at.namart.model.bo.Angebot;
import at.namart.model.bo.Kunde;
import at.namart.model.bo.Projekt;
import at.namart.model.bo.Zeit;
import at.namart.view.components.VerwaltungView;
import at.namart.view.components.combobox.KundeComboBoxModel;
import at.namart.view.components.combobox.ProjektComboBoxModel;
import at.namart.view.components.tables.AngebotTableModel;
import at.namart.view.components.tables.ZeiterfassungTableModel;

public class ZeiterfassungView extends VerwaltungView<Zeit,ZeiterfassungTableModel,ZeiterfassungModel> {

	//Felder
	JTextField fieldNr;
	JTextField fieldDauer;
	JTextField fieldDatum;
	
	JComboBox fieldProjektCombo; 
	ProjektComboBoxModel fieldProjektComboModel; 
	
	public ZeiterfassungView() {
		
		super(new Zeit(), new ZeiterfassungTableModel(new String[]{"Nr.", "Dauer", "Datum"}, new ArrayList<Zeit>()), ZeiterfassungModel.getInstance());
		
		//Sich beim model registrieren
		this.model.addProduktListener(this);
		
		//Form
        JPanel InputForm = new JPanel();
		InputForm.setLayout( new BoxLayout(InputForm, BoxLayout.Y_AXIS) );
		InputForm.setBorder(BorderFactory.createEmptyBorder(10, 20, 10, 20));
		add(InputForm);
		
		//InputFields
		JPanel InputFields = new JPanel();
		InputFields.setLayout( new GridLayout(7,2) );
		InputForm.add(InputFields);
		
		InputFields.add( new JLabel("Nr.") );
		fieldNr = new JTextField(20);
		fieldNr.setEditable(false);
		InputFields.add(fieldNr);
		
		InputFields.add( new JLabel("Dauer") );
		fieldDauer = new JTextField(20);
		InputFields.add(fieldDauer);
		
		InputFields.add( new JLabel("Datum") );
		fieldDatum = new JTextField(20);
		InputFields.add(fieldDatum);
		
		InputFields.add( new JLabel("Projekt") );
		fieldProjektCombo = new JComboBox();
		fieldProjektComboModel = new ProjektComboBoxModel(new ArrayList<Projekt>());
		InputFields.add(fieldProjektCombo);
		fieldProjektCombo.setModel(fieldProjektComboModel);

        //Buttons
		JPanel ButtonForm = new JPanel();
		add(ButtonForm);
		ButtonForm.setLayout(new FlowLayout());
		//Create Buttons
		ButtonForm.add(this.newButton);
		ButtonForm.add(this.saveButton);
		ButtonForm.add(this.deleteButton);
		ButtonForm.add(this.refreshButton);
	}
	
	/**
	* F�ngt Meldungen vom Model auf
	*/
	@Override
	public void change(NotiEvent e) {
		
		// GET_LIST - The list has changed
		if(e.getName() == model.GET_LIST ) {
			this.tableModel.setList((ArrayList<Zeit>)e.getSource());
			this.setComponent(null);
		}
		
		else if(e.getName() == model.ITEM_CHANGE) {
			
			int row = this.tableModel.findObjectRow((Zeit) e.getSource());
			if(row == -1) {
				this.tableModel.addObject((Zeit) e.getSource());
				row = this.tableModel.getObjectIndex((Zeit) e.getSource());
				this.table.setRowSelectionInterval(row, row);
			} else {
				this.tableModel.setObjectAt((Zeit) e.getSource(), row);
				this.table.setRowSelectionInterval(row, row);
			}	
		}
		
		else if(e.getName() == model.DELETE_ITEM) {
			this.tableModel.removeObject((Zeit) e.getSource());
		}
		
		else if (e.getName() == model.PROJEKT_LIST) {
			fieldProjektComboModel.setList((ArrayList<Projekt>)e.getSource());
		}
	}
	
	/**
	* Setz das "Angebot" und bindet es anschlie�end an die GUI
	*/
	public void setComponent(Zeit z) {
		if(z == null) {
			component = new Zeit();
			
			//Damit markierung verschwindet
			this.tableModel.fireTableDataChanged();
			myBinder.resetErrorControl();
        }
        else {
        	component = z;
        }
        BindTo();
	}
	
	/**
	* Bindet das "Angebot" an die GUI
	*/
	public void BindTo() {
		
		if(component.getId() != Model.INVALIDID){
			fieldNr.setText(Integer.toString(component.getId()));
			fieldDatum.setText(new SimpleDateFormat("dd/MM/yyyy").format(component.getDatum()));
		} else {
			fieldNr.setText("");
			fieldDatum.setText("dd/MM/yyyy");	
		}

		fieldDauer.setText(Integer.toString(component.getDauer()));
		
		//Gew�hltes Projekt an die GUi binden
		if(component.getProjektId() != Model.UNDEFINED) {
			fieldProjektComboModel.setSelectedItemById(component.getProjektId());
		} else {
			fieldProjektComboModel.setSelectedItemById(Model.UNDEFINED);
		}

	}
	
	/**
	* Bindet die GUI an das "Angebot"
	*/
	public boolean BindFrom() {
		
		if(component == null) {
			component = new Zeit();
		}
		
		myBinder.resetErrorControl();
		if(fieldNr.getText().isEmpty()) {
			component.setId(-1);
		} else {
			component.setId(myBinder.BindFrom_Int(fieldNr, new RequiredRule()));
		}

		component.setDauer(myBinder.BindFrom_Minus(fieldDauer, new RequiredRule()));
		component.setDatum(myBinder.BindFrom_Date(fieldDatum, new RequiredRule()));
		
		//Ausgew�hltes Projekt in die Komponente
		Projekt p = fieldProjektComboModel.getSelectedItem();
		if(p != null) {
			component.setProjektId(p.getId());
		} else {
			myBinder.setError("Sie m�ssen ein Projekt angeben");
			component.setProjektId(Model.UNDEFINED);
		}
		
		if(myBinder.hasError()) {
			return false;
		} else {
			return true;
		}
	}
	
	/* GETTER AND SETTER FOR UNIT-TESTS*/

	public void setFieldDauer(String dauer) {
		this.fieldDauer.setText(dauer);
	}

	public void setFieldDatum(String datum) {
		this.fieldDatum.setText(datum);
	}
	
	public ProjektComboBoxModel getFieldProjektCombo() {
		return this.fieldProjektComboModel;
	}
	
}
