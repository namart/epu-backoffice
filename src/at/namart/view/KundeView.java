package at.namart.view;

import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.util.ArrayList;

import javax.swing.*;

import at.namart.lib.Model;
import at.namart.lib.NotiEvent;
import at.namart.lib.binder.RequiredRule;
import at.namart.model.KundeModel;
import at.namart.model.bo.Kontakt;
import at.namart.model.bo.Kunde;
import at.namart.model.bo.Projekt;
import at.namart.view.components.VerwaltungView;
import at.namart.view.components.combobox.KontaktComboBoxModel;
import at.namart.view.components.combobox.KundeComboBoxModel;
import at.namart.view.components.combobox.ProjektComboBoxModel;
import at.namart.view.components.tables.KundeTableModel;


public class KundeView extends VerwaltungView<Kunde,KundeTableModel,KundeModel> {

	//Felder
	JTextField fieldNr;
	JTextField fieldName;
	JTextField fieldUid;
	
	JComboBox fieldKontaktCombo; 
	KontaktComboBoxModel fieldKontaktComboModel; 

	public KundeView() {
		
		super(new Kunde(), new KundeTableModel(new String[]{"Nr.", "Name", "UID"}, new ArrayList<Kunde>()), KundeModel.getInstance());
		
		//Sich beim model registrieren
		this.model.addProduktListener(this);
        
        //Form
        JPanel InputForm = new JPanel();
		InputForm.setLayout( new BoxLayout(InputForm, BoxLayout.Y_AXIS) );
		InputForm.setBorder(BorderFactory.createEmptyBorder(10, 20, 10, 20));
		add(InputForm);
		
		//InputFields
		JPanel InputFields = new JPanel();
		InputFields.setLayout( new GridLayout(4,2) );
		InputForm.add(InputFields);
		
		InputFields.add( new JLabel("Nr.") );
		fieldNr = new JTextField(20);
		fieldNr.setEditable(false);
		InputFields.add(fieldNr);
		
		InputFields.add( new JLabel("Name") );
		fieldName = new JTextField(20);
		InputFields.add(fieldName);
		
		InputFields.add( new JLabel("UID") );
		fieldUid = new JTextField(20);
		InputFields.add(fieldUid);
		
		InputFields.add( new JLabel("Kontakt") );
		fieldKontaktCombo = new JComboBox();
		fieldKontaktComboModel = new KontaktComboBoxModel(new ArrayList<Kontakt>());
		InputFields.add(fieldKontaktCombo);
		fieldKontaktCombo.setModel(fieldKontaktComboModel);

        //Buttons
		JPanel ButtonForm = new JPanel();
		add(ButtonForm);
		ButtonForm.setLayout(new FlowLayout());
		//Create Buttons
		ButtonForm.add(this.newButton);
		ButtonForm.add(this.saveButton);
		ButtonForm.add(this.deleteButton);
		ButtonForm.add(this.refreshButton);
 
	}
	
	/**
	* F�ngt Meldungen vom Model auf
	*/
	@Override
	public void change(NotiEvent e) {
		
		// GET_LIST - The list has changed
		if(e.getName() == model.GET_LIST ) {
			this.tableModel.setList((ArrayList<Kunde>)e.getSource());
		}
		
		else if(e.getName() == model.ITEM_CHANGE) {
			
			int row = this.tableModel.findObjectRow((Kunde) e.getSource());
			if(row == -1) {
				this.tableModel.addObject((Kunde) e.getSource());
				row = this.tableModel.getObjectIndex((Kunde) e.getSource());
				this.table.setRowSelectionInterval(row, row);
			} else {
				this.tableModel.setObjectAt((Kunde) e.getSource(), row);
				this.table.setRowSelectionInterval(row, row);
			}	
		}
		
		else if(e.getName() == model.DELETE_ITEM) {
			this.tableModel.removeObject((Kunde) e.getSource());
		}
		
		else if (e.getName() == model.KONTAKT_LIST) {
			fieldKontaktComboModel.setList((ArrayList<Kontakt>)e.getSource());
		}
		
	}
	
	/**
	* Setz das "Kunde" und bindet es anschlie�end an die GUI
	*/
	public void setComponent(Kunde k) {
		if(k == null) {
			component = new Kunde();
			
			//Damit markierung verschwindet
			this.tableModel.fireTableDataChanged();
			myBinder.resetErrorControl();
        }
        else {
        	component = k;
        }
        BindTo();
	}
	
	/**
	* Bindet desn "Kunden" an die GUI
	*/
	public void BindTo() {
		if(component.getId() != Model.INVALIDID){
			fieldNr.setText(Integer.toString(component.getId()));
		} else {
			fieldNr.setText("");
		}
		
		fieldName.setText(component.getName());
		fieldUid.setText(Integer.toString(component.getUid()));
		
		//Gew�hlter Kontakt an die GUi binden
		if(component.getKontaktId() != Model.UNDEFINED) {
			fieldKontaktComboModel.setSelectedItemById(component.getKontaktId());
		} else {
			fieldKontaktComboModel.setSelectedItemById(Model.UNDEFINED);
		}
	}
	
	/**
	* Bindet die GUI an das "Kunde"
	*/
	public boolean BindFrom() {
		
		if(component == null) {
			component = new Kunde();
		}
		
		myBinder.resetErrorControl();
		if(fieldNr.getText().isEmpty()) {
			component.setId(-1);
		} else {
			component.setId(myBinder.BindFrom_Int(fieldNr, new RequiredRule()));
		}
		
		component.setName(myBinder.BindFrom_String (fieldName, new RequiredRule()));
		component.setUid(myBinder.BindFrom_Minus(fieldUid, new RequiredRule()));
		
		//Ausgew�hlter Kontakt in die Komponente
		Kontakt k = fieldKontaktComboModel.getSelectedItem();
		if(k != null) {
			component.setKontaktId(k.getId());
		} else {
			component.setKontaktId(Model.UNDEFINED);
		}
		
		if(myBinder.hasError()) {
			return false;
		} else {
			return true;
		}
	}
	
	/* GETTER AND SETTER FOR UNIT-TESTS*/

	public void setFieldName(String name) {
		this.fieldName.setText(name);
	}

	public void setFieldUID(String uid) {
		this.fieldUid.setText(uid);
	}
	
	public KontaktComboBoxModel getFieldKontaktCombo() {
		return this.fieldKontaktComboModel;
	}

}
