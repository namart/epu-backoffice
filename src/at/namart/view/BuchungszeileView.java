package at.namart.view;

import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;

import at.namart.controller.BuchungszeileAusgangsrechnungController;
import at.namart.controller.BuchungszeileEingangsrechnungController;
import at.namart.controller.RechnungszeilenController;
import at.namart.lib.Model;
import at.namart.lib.NotiEvent;
import at.namart.lib.binder.RequiredRule;
import at.namart.lib.logger.Logger;
import at.namart.model.BuchungszeileModel;
import at.namart.model.bo.Buchungszeile;
import at.namart.model.bo.Kategorie;
import at.namart.view.components.VerwaltungView;
import at.namart.view.components.tables.BuchungszeileTableModel;

public class BuchungszeileView extends VerwaltungView<Buchungszeile, BuchungszeileTableModel, BuchungszeileModel> {

	//Felder
	JTextField fieldNr;
	JTextField fieldName;
	JTextField fieldBetrag;
	
	protected JButton ausgangsrechnungenButton = new JButton("Ausgangsrechnungen bearbeiten");
	protected JButton eingangsrechnungenButton = new JButton("Eingangsrechnungen bearbeiten");
	
	JList kategorieListe;
	
	public BuchungszeileView() {
		
		super(new Buchungszeile(), new BuchungszeileTableModel(new String[]{"Nr.", "Name", "Betrag"}, new ArrayList<Buchungszeile>()), BuchungszeileModel.getInstance());
		
		//Sich beim model registrieren
		this.model.addProduktListener(this);
		
		//Form
        JPanel InputForm = new JPanel();
		InputForm.setLayout( new BoxLayout(InputForm, BoxLayout.Y_AXIS) );
		InputForm.setBorder(BorderFactory.createEmptyBorder(10, 20, 10, 20));
		add(InputForm);
		
		//InputFields
		JPanel InputFields = new JPanel();
		InputFields.setLayout( new GridLayout(6,2) );
		InputForm.add(InputFields);
		
		InputFields.add( new JLabel("Nr.") );
		fieldNr = new JTextField(20);
		fieldNr.setEditable(false);
		InputFields.add(fieldNr);
		
		InputFields.add( new JLabel("Name") );
		fieldName = new JTextField(20);
		InputFields.add(fieldName);
		
		InputFields.add( new JLabel("Betrag") );
		fieldBetrag = new JTextField(20);
		InputFields.add(fieldBetrag);
		
		//Ausgangsrechnungen Form
		InputFields.add( new JLabel("Ausgangsrechnungen") );
		//disable button of nothing selected
		ausgangsrechnungenButton.setEnabled(false);
		InputFields.add(ausgangsrechnungenButton);
		ausgangsrechnungenButton.addActionListener( new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//AusgangsrechnungenController
				new BuchungszeileAusgangsrechnungController(new FormWrapperView<BuchungszeileAusgangsrechnungView>(new BuchungszeileAusgangsrechnungView()), component.getId());
			}
		});
		
		//Eingangsrechnungen Form
		InputFields.add( new JLabel("Eingangsrechnungen") );
		//disable button of nothing selected
		eingangsrechnungenButton.setEnabled(false);
		InputFields.add(eingangsrechnungenButton);
		eingangsrechnungenButton.addActionListener( new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//EingangsrechnungenController
				new BuchungszeileEingangsrechnungController(new FormWrapperView<BuchungszeileEingangsrechnungView>(new BuchungszeileEingangsrechnungView()), component.getId());
			}
		});
		
		//Kategorien
		JPanel KategorieForm = new JPanel();
		InputForm.add(KategorieForm);
		KategorieForm.setLayout( new GridLayout(1,2) );
		KategorieForm.add( new JLabel("Kategorien") );
		kategorieListe = new JList();
		kategorieListe.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		KategorieForm.add(kategorieListe);
	
		
		
		//Buttons
		JPanel ButtonForm = new JPanel();
		add(ButtonForm);
		ButtonForm.setLayout(new FlowLayout());
		//Create Buttons
		ButtonForm.add(this.newButton);
		ButtonForm.add(this.saveButton);
		ButtonForm.add(this.deleteButton);
		ButtonForm.add(this.refreshButton);
	}
	
	/**
	* F�ngt Meldungen vom Model auf
	*/
	@Override
	public void change(NotiEvent e) {
		
		// GET_LIST - The list has changed
		if(e.getName() == model.GET_LIST ) {
			this.tableModel.setList((ArrayList<Buchungszeile>)e.getSource());
			
			//disable button of nothing selected
			ausgangsrechnungenButton.setEnabled(false);
			eingangsrechnungenButton.setEnabled(false);
		}
		
		else if(e.getName() == model.ITEM_CHANGE) {
			
			int row = this.tableModel.findObjectRow((Buchungszeile) e.getSource());
			if(row == -1) {
				this.tableModel.addObject((Buchungszeile) e.getSource());
				row = this.tableModel.getObjectIndex((Buchungszeile) e.getSource());
				this.table.setRowSelectionInterval(row, row);
			} else {
				this.tableModel.setObjectAt((Buchungszeile) e.getSource(), row);
				this.table.setRowSelectionInterval(row, row);
			}	
		}
		
		else if(e.getName() == model.DELETE_ITEM) {
			this.tableModel.removeObject((Buchungszeile) e.getSource());
		}
		
		else if(e.getName() == model.KATEGORIEN_CHANGE) {
			
			DefaultListModel kategorieListModel = new DefaultListModel();

			for ( Kategorie k : (ArrayList<Kategorie>)e.getSource() ) {
				kategorieListModel.addElement( k );
			}

			kategorieListe.setModel(kategorieListModel);
		}

	}

	@Override
	public void setComponent(Buchungszeile c) {
		
		if(c == null) {
			component = new Buchungszeile();
			
			//Damit markierung verschwindet
			this.tableModel.fireTableDataChanged();
			myBinder.resetErrorControl();
			
			//disable button of nothing selected
			ausgangsrechnungenButton.setEnabled(false);
			eingangsrechnungenButton.setEnabled(false);
        }
        else {
        	
        	//enable button of item is selected
        	ausgangsrechnungenButton.setEnabled(true);
			eingangsrechnungenButton.setEnabled(true);
        	component = c;
        }
        BindTo();
		
	}
	
	/**
	* Bindet die "Buchungszeile" an die GUI
	*/
	public void BindTo() {
		
		if(component.getId() != Model.INVALIDID){
			fieldNr.setText(Integer.toString(component.getId()));
		} else {
			fieldNr.setText("");
		}
		
		fieldName.setText(component.getName());
		fieldBetrag.setText(Float.toString(component.getBetrag()));
		
		//Set KategorieList
		kategorieListe.clearSelection();
		if(component.getKategorienIds() != null) {
			int[] selectIndicesArray = null;
			ArrayList<Integer> selectIndicesList = new ArrayList<Integer>();
			for(Integer kategorieId : component.getKategorienIds()) {
		
				for (int i = 0; i < kategorieListe.getModel().getSize(); i++) {
				    Kategorie element = (Kategorie) kategorieListe.getModel().getElementAt(i);
				 
				    if(kategorieId == element.getId()) {
				    	selectIndicesList.add(i);
				    }
				}	

			}
			selectIndicesArray = new int[selectIndicesList.size()];
			
			for(int i=0; i<selectIndicesList.size(); i++) {
				
				selectIndicesArray[i] = selectIndicesList.get(i);
			}
			
			kategorieListe.setSelectedIndices(selectIndicesArray);
		}
		

	}
	
	/**
	* Bindet die GUI an die "Buchungszeile"
	*/
	public boolean BindFrom() {
		
		if(component == null) {
			component = new Buchungszeile();
		}
		
		myBinder.resetErrorControl();
		if(fieldNr.getText().isEmpty()) {
			component.setId(-1);
		} else {
			component.setId(myBinder.BindFrom_Int(fieldNr, new RequiredRule()));
		}
		
		component.setName(myBinder.BindFrom_String (fieldName, new RequiredRule()));
		component.setBetrag(myBinder.BindFrom_Float(fieldBetrag, new RequiredRule()));
		
		//Get selected KategorieList
		int selected[] = kategorieListe.getSelectedIndices();
	      
	    ArrayList<Integer> KategorienIds = new ArrayList<Integer>();

	    for (int i = 0; i < selected.length; i++) {
	      Kategorie element = (Kategorie) kategorieListe.getModel().getElementAt(selected[i]);
	      KategorienIds.add(element.getId());
	    }
	    component.setKategorienIds(KategorienIds);
		
		if(myBinder.hasError()) {
			return false;
		} else {
			return true;
		}
	}

	public JTextField getFieldName() {
		return fieldName;
	}

	public JTextField getFieldBetrag() {
		return fieldBetrag;
	}

	public JList getKategorieListe() {
		return kategorieListe;
	}
	
	
}
