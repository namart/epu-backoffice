package at.namart.view;

import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.util.ArrayList;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import at.namart.lib.Model;
import at.namart.lib.NotiEvent;
import at.namart.lib.binder.RequiredRule;
import at.namart.model.EingangsrechnungModel;
import at.namart.model.bo.Eingangsrechnung;
import at.namart.model.bo.Kontakt;
import at.namart.view.components.VerwaltungView;
import at.namart.view.components.combobox.KontaktComboBoxModel;

import at.namart.view.components.tables.EingangsrechnungTableModel;

public class EingangsrechnungView extends VerwaltungView<Eingangsrechnung, EingangsrechnungTableModel, EingangsrechnungModel> {

	//Felder
	JTextField fieldNr;
	JTextField fieldName;
	
	JComboBox fieldKontaktCombo; 
	KontaktComboBoxModel fieldKontaktComboModel; 
	
	protected JButton scanButton = new JButton("Scannen");
	
	public EingangsrechnungView() {
		
		super(new Eingangsrechnung(), new EingangsrechnungTableModel(new String[]{"Nr.", "Name"}, new ArrayList<Eingangsrechnung>()), EingangsrechnungModel.getInstance());
		
		//Sich beim model registrieren
		this.model.addProduktListener(this);
		
		//Form
        JPanel InputForm = new JPanel();
		InputForm.setLayout( new BoxLayout(InputForm, BoxLayout.Y_AXIS) );
		InputForm.setBorder(BorderFactory.createEmptyBorder(10, 20, 10, 20));
		add(InputForm);
		
		//InputFields
		JPanel InputFields = new JPanel();
		InputFields.setLayout( new GridLayout(4,2) );
		InputForm.add(InputFields);
		
		InputFields.add( new JLabel("Nr.") );
		fieldNr = new JTextField(20);
		fieldNr.setEditable(false);
		InputFields.add(fieldNr);
		
		InputFields.add( new JLabel("Name") );
		fieldName = new JTextField(20);
		InputFields.add(fieldName);
		
		InputFields.add( new JLabel("Kontakt") );
		fieldKontaktCombo = new JComboBox();
		fieldKontaktComboModel = new KontaktComboBoxModel(new ArrayList<Kontakt>());
		InputFields.add(fieldKontaktCombo);
		fieldKontaktCombo.setModel(fieldKontaktComboModel);
		
		//Rechnungszeilen Form
		InputFields.add( new JLabel("Eingangsrechnung scannen") );
		InputFields.add(scanButton);
		
		//Buttons
		JPanel ButtonForm = new JPanel();
		add(ButtonForm);
		ButtonForm.setLayout(new FlowLayout());
		//Create Buttons
		ButtonForm.add(this.newButton);
		ButtonForm.add(this.saveButton);
		ButtonForm.add(this.deleteButton);
		ButtonForm.add(this.refreshButton);
	}
	
	/**
	* F�ngt Meldungen vom Model auf
	*/
	@Override
	public void change(NotiEvent e) {
		
		// GET_LIST - The list has changed
		if(e.getName() == model.GET_LIST ) {
			this.tableModel.setList((ArrayList<Eingangsrechnung>)e.getSource());

		}
		
		else if(e.getName() == model.ITEM_CHANGE) {
			
			int row = this.tableModel.findObjectRow((Eingangsrechnung) e.getSource());
			if(row == -1) {
				this.tableModel.addObject((Eingangsrechnung) e.getSource());
				row = this.tableModel.getObjectIndex((Eingangsrechnung) e.getSource());
				this.table.setRowSelectionInterval(row, row);
			} else {
				this.tableModel.setObjectAt((Eingangsrechnung) e.getSource(), row);
				this.table.setRowSelectionInterval(row, row);
			}	
		}
		
		else if(e.getName() == model.DELETE_ITEM) {
			this.tableModel.removeObject((Eingangsrechnung) e.getSource());
		}
		
		else if (e.getName() == model.KONTAKT_LIST) {
			fieldKontaktComboModel.setList((ArrayList<Kontakt>)e.getSource());
		}
	}

	@Override
	public void setComponent(Eingangsrechnung c) {
		
		if(c == null) {
			component = new Eingangsrechnung();
			
			//Damit markierung verschwindet
			this.tableModel.fireTableDataChanged();
			myBinder.resetErrorControl();

        }
        else {

        	component = c;
        }
        BindTo();
		
	}
	
	/**
	* Bindet das "Angebot" an die GUI
	*/
	public void BindTo() {
		
		if(component.getId() != Model.INVALIDID){
			fieldNr.setText(Integer.toString(component.getId()));
		} else {
			fieldNr.setText("");
		}
		
		fieldName.setText(component.getName());
		
		//Gew�hlter Kontakt an die GUi binden
		if(component.getKontaktId() != Model.UNDEFINED) {
			fieldKontaktComboModel.setSelectedItemById(component.getKontaktId());
		} else {
			fieldKontaktComboModel.setSelectedItemById(Model.UNDEFINED);
		}
	}
	
	/**
	* Bindet die GUI an das "Angebot"
	*/
	public boolean BindFrom() {
		
		if(component == null) {
			component = new Eingangsrechnung();
		}
		
		myBinder.resetErrorControl();
		if(fieldNr.getText().isEmpty()) {
			component.setId(-1);
		} else {
			component.setId(myBinder.BindFrom_Int(fieldNr, new RequiredRule()));
		}
		
		component.setName(myBinder.BindFrom_String (fieldName, new RequiredRule()));
		
		//Ausgew�hlter Kontakt in die Komponente
		Kontakt k = fieldKontaktComboModel.getSelectedItem();
		if(k != null) {
			component.setKontaktId(k.getId());
		} else {
			component.setKontaktId(Model.UNDEFINED);
		}
		
		if(myBinder.hasError()) {
			return false;
		} else {
			return true;
		}
	}

	public JTextField getFieldName() {
		return fieldName;
	}

	public KontaktComboBoxModel getFieldKontaktComboModel() {
		return fieldKontaktComboModel;
	}
	
	
	
}
