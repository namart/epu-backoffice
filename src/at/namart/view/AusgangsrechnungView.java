package at.namart.view;

import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import at.namart.controller.ProjektController;
import at.namart.controller.RechnungszeilenController;
import at.namart.lib.Model;
import at.namart.lib.NotiEvent;
import at.namart.lib.binder.RequiredRule;
import at.namart.lib.logger.Logger;
import at.namart.model.AusgangsrechnungModel;
import at.namart.model.bo.Ausgangsrechnung;
import at.namart.model.bo.Kunde;
import at.namart.view.components.VerwaltungView;
import at.namart.view.components.combobox.KundeComboBoxModel;
import at.namart.view.components.tables.AusgangsrechnungTableModel;

public class AusgangsrechnungView extends VerwaltungView<Ausgangsrechnung, AusgangsrechnungTableModel, AusgangsrechnungModel> {

	//Felder
	JTextField fieldNr;
	JTextField fieldName;
	
	JComboBox fieldKundeCombo; 
	KundeComboBoxModel fieldKundeComboModel; 
	
	private JButton rechnungszeilenButton = new JButton("Rechnungszeilen bearbeiten");
	
	private JButton createPdf = new JButton("erstelle Angebot als PDF");
	
	public AusgangsrechnungView() {
		
		super(new Ausgangsrechnung(), new AusgangsrechnungTableModel(new String[]{"Nr.", "Name"}, new ArrayList<Ausgangsrechnung>()), AusgangsrechnungModel.getInstance());
		
		//Sich beim model registrieren
		this.model.addProduktListener(this);
        
        //Form
        JPanel InputForm = new JPanel();
		InputForm.setLayout( new BoxLayout(InputForm, BoxLayout.Y_AXIS) );
		InputForm.setBorder(BorderFactory.createEmptyBorder(10, 20, 10, 20));
		add(InputForm);
		
		//InputFields
		JPanel InputFields = new JPanel();
		InputFields.setLayout( new GridLayout(5,2) );
		InputForm.add(InputFields);
		
		InputFields.add( new JLabel("Nr.") );
		fieldNr = new JTextField(20);
		fieldNr.setEditable(false);
		InputFields.add(fieldNr);
		
		InputFields.add( new JLabel("Name") );
		fieldName = new JTextField(20);
		InputFields.add(fieldName);
		
		InputFields.add( new JLabel("Kunde") );
		fieldKundeCombo = new JComboBox();
		fieldKundeComboModel = new KundeComboBoxModel(new ArrayList<Kunde>());
		InputFields.add(fieldKundeCombo);
		fieldKundeCombo.setModel(fieldKundeComboModel);
		
		//Rechnungszeilen Form
		InputFields.add( new JLabel("Rechnungszeilen") );
		//disable button of nothing selected
		rechnungszeilenButton.setEnabled(false);
		InputFields.add(rechnungszeilenButton);
		rechnungszeilenButton.addActionListener( new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//RechnungszeilenController
				new RechnungszeilenController(new FormWrapperView<RechnungszeilenView>(new RechnungszeilenView()), component.getId());
			}
		});
		
		InputFields.add( new JLabel("Ein Angebot als PDF erstellen") );
		InputFields.add(createPdf);


        //Buttons
		JPanel ButtonForm = new JPanel();
		add(ButtonForm);
		ButtonForm.setLayout(new FlowLayout());
		//Create Buttons
		ButtonForm.add(this.newButton);
		ButtonForm.add(this.saveButton);
		ButtonForm.add(this.deleteButton);
		ButtonForm.add(this.refreshButton);
 
	}
	
	/**
	* F�ngt Meldungen vom Model auf
	*/
	@Override
	public void change(NotiEvent e) {
		
		// GET_LIST - The list has changed
		if(e.getName() == model.GET_LIST ) {
			this.tableModel.setList((ArrayList<Ausgangsrechnung>)e.getSource());
			
			//disable button of nothing selected
			rechnungszeilenButton.setEnabled(false);
		}
		
		else if(e.getName() == model.ITEM_CHANGE) {
			
			int row = this.tableModel.findObjectRow((Ausgangsrechnung) e.getSource());
			if(row == -1) {
				this.tableModel.addObject((Ausgangsrechnung) e.getSource());
				row = this.tableModel.getObjectIndex((Ausgangsrechnung) e.getSource());
				this.table.setRowSelectionInterval(row, row);
			} else {
				this.tableModel.setObjectAt((Ausgangsrechnung) e.getSource(), row);
				this.table.setRowSelectionInterval(row, row);
			}	
		}
		
		else if(e.getName() == model.DELETE_ITEM) {
			this.tableModel.removeObject((Ausgangsrechnung) e.getSource());
		}
		
		else if (e.getName() == model.KUNDE_LIST) {
			fieldKundeComboModel.setList((ArrayList<Kunde>)e.getSource());
		}
	}
	
	@Override
	public void setComponent(Ausgangsrechnung a) {
		
		
		if(a == null) {
			component = new Ausgangsrechnung();
			
			//Damit markierung verschwindet
			this.tableModel.fireTableDataChanged();
			myBinder.resetErrorControl();
			
			//disable button of nothing selected
			rechnungszeilenButton.setEnabled(false);
        }
        else {
        	
        	//enable button of item is selected
        	rechnungszeilenButton.setEnabled(true);
        	component = a;
        }
        BindTo();
		
	}
	
	/**
	* Bindet das "Angebot" an die GUI
	*/
	public void BindTo() {
		
		if(component.getId() != Model.INVALIDID){
			fieldNr.setText(Integer.toString(component.getId()));
		} else {
			fieldNr.setText("");
		}
		
		fieldName.setText(component.getName());
		
		//Gew�hlter Kunde an die GUi binden
		if(component.getKundenId() != Model.UNDEFINED) {
			fieldKundeComboModel.setSelectedItemById(component.getKundenId());
		} else {
			fieldKundeComboModel.setSelectedItemById(Model.UNDEFINED);
		}
	}
	
	/**
	* Bindet die GUI an das "Angebot"
	*/
	public boolean BindFrom() {
		
		if(component == null) {
			component = new Ausgangsrechnung();
		}
		
		myBinder.resetErrorControl();
		if(fieldNr.getText().isEmpty()) {
			component.setId(-1);
		} else {
			component.setId(myBinder.BindFrom_Int(fieldNr, new RequiredRule()));
		}
		
		component.setName(myBinder.BindFrom_String (fieldName, new RequiredRule()));
		
		//Ausgew�hlter Kunde in die Komponente
		Kunde k = fieldKundeComboModel.getSelectedItem();
		if(k != null) {
			component.setKundenId(k.getId());
		} else {
			component.setKundenId(Model.UNDEFINED);
		}
		
		if(myBinder.hasError()) {
			return false;
		} else {
			return true;
		}
	}
	
	/* GETTER AND SETTER FOR UNIT-TESTS*/
	public void setPdfButtonListener(ActionListener l){
		this.createPdf.addActionListener(l);
	}

	public JTextField getFieldName() {
		return fieldName;
	}

	public KundeComboBoxModel getFieldKundeComboModel() {
		return fieldKundeComboModel;
	}

	
}
