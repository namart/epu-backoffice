package at.namart.view;

import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.util.ArrayList;

import javax.swing.*;

import at.namart.lib.Model;
import at.namart.lib.NotiEvent;
import at.namart.lib.binder.RequiredRule;
import at.namart.model.ProjektModel;
import at.namart.model.bo.Projekt;
import at.namart.view.components.VerwaltungView;
import at.namart.view.components.tables.ProjektTableModel;


public class ProjektView extends VerwaltungView<Projekt,ProjektTableModel,ProjektModel> {
	
	//Felder
	JTextField fieldNr;
	JTextField fieldName;

	public ProjektView() {
		
		super(new Projekt(), new ProjektTableModel(new String[]{"Nr.", "Name"}, new ArrayList<Projekt>()), ProjektModel.getInstance());
		
		
		//Sich beim model registrieren
		model.addProduktListener(this);
        
        //Form
        JPanel InputForm = new JPanel();
		InputForm.setLayout( new BoxLayout(InputForm, BoxLayout.Y_AXIS) );
		InputForm.setBorder(BorderFactory.createEmptyBorder(10, 20, 10, 20));
		add(InputForm);
		
		//InputFields
		JPanel InputFields = new JPanel();
		InputFields.setLayout( new GridLayout(3,2) );
		InputForm.add(InputFields);
		
		InputFields.add( new JLabel("Nr.") );
		fieldNr = new JTextField(20);
		fieldNr.setEditable(false);
		InputFields.add(fieldNr);
		
		InputFields.add( new JLabel("Name") );
		fieldName = new JTextField(20);
		InputFields.add(fieldName);

        //Buttons
		JPanel ButtonForm = new JPanel();
		add(ButtonForm);
		ButtonForm.setLayout(new FlowLayout());
		//Create Buttons
		ButtonForm.add(this.newButton);
		ButtonForm.add(this.saveButton);
		ButtonForm.add(this.deleteButton);
		ButtonForm.add(this.refreshButton);
 
	}
	
	/**
	* F�ngt Meldungen vom Model auf
	*/
	@Override
	public void change(NotiEvent e) {
		
		// GET_LIST - The list has changed
		if(e.getName() == model.GET_LIST ) {
			this.tableModel.setList((ArrayList<Projekt>)e.getSource());
		}
		
		else if(e.getName() == model.ITEM_CHANGE) {
			
			int row = this.tableModel.findObjectRow((Projekt) e.getSource());
			if(row == -1) {
				this.tableModel.addObject((Projekt) e.getSource());
				row = this.tableModel.getObjectIndex((Projekt) e.getSource());
				this.table.setRowSelectionInterval(row, row);
			} else {
				this.tableModel.setObjectAt((Projekt) e.getSource(), row);
				this.table.setRowSelectionInterval(row, row);
			}	
		}
		
		else if(e.getName() == model.DELETE_ITEM) {
			this.tableModel.removeObject((Projekt) e.getSource());
		}
	}
	
	/**
	* Setz das "Projekt" und bindet es anschlie�end an die GUI
	*/
	public void setComponent(Projekt p) {
		if(p == null) {
			component = new Projekt();
			
			//Damit markierung verschwindet
			this.tableModel.fireTableDataChanged();
			myBinder.resetErrorControl();
        }
        else {
        	component = p;
        }
        BindTo();
	}
	
	/**
	* Bindet das "Projekt" an die GUI
	*/
	public void BindTo() {
		if(component.getId() != Model.INVALIDID){
			fieldNr.setText(Integer.toString(component.getId()));
		} else {
			fieldNr.setText("");
		}
		
		fieldName.setText(component.getName());
	}
	
	/**
	* Bindet die GUI an das "Projekt"
	*/
	public boolean BindFrom() {
		
		if(component == null) {
			component = new Projekt();
		}
		
		myBinder.resetErrorControl();
		if(fieldNr.getText().isEmpty()) {
			component.setId(-1);
		} else {
			component.setId(myBinder.BindFrom_Int(fieldNr, new RequiredRule()));
		}
		
		component.setName(myBinder.BindFrom_String (fieldName, new RequiredRule()));
		
		if(myBinder.hasError()) {
			return false;
		} else {
			return true;
		}
	}

}
