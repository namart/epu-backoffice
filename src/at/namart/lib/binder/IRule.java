package at.namart.lib.binder;

public interface IRule {

	public boolean Eval(Object val, ErrorControl ctrl);

	public boolean checkEmail(String val, ErrorControl ctrl);
	public boolean checkPercent(int val, ErrorControl ctrl);
	public boolean checktelefon(int val, ErrorControl ctrl);
	public boolean checkminus(int val, ErrorControl ctrl);
}
