package at.namart.lib.binder;

import java.util.regex.*;

public class RequiredRule implements IRule {

	@Override
	public boolean Eval(Object val, ErrorControl ctrl) {
		if(val instanceof String) {
			if(((String) val).isEmpty()) {
				ctrl.setError("Sie m�ssen einen Wert eingeben");
	            return false;
	        }
	    } 
		return true;
	}

	@Override
	public boolean checkEmail(String val, ErrorControl ctrl) {

	      //Set the email pattern string
	      Pattern p = Pattern.compile(".+@.+\\.[a-z]+");

	      //Match the given string with the pattern
	      Matcher m = p.matcher(val);

	      //check whether match is found 
	      boolean matchFound = m.matches();

	      if (matchFound) {
	        return true;
	      }
	      else {
	    	ctrl.setError("Geben sie eine korrekte email an");
	        return false;
	      }
	}

	@Override
	public boolean checkPercent(int val, ErrorControl ctrl) {
		
		if(val < 0 || val > 100){
			ctrl.setError("Geben sie einen korrekten Prozentwert an");
			return false;
		}
		else {
			return true;
		}
	}

	@Override
	public boolean checktelefon(int val, ErrorControl ctrl) {
		
		Integer valI = val;
		if(val < 0 || valI.toString().length() < 5) {
			ctrl.setError("Geben sie eine korrekte Telefonnummber an");
			return false;
		} else {
			return true;
		}
	}

	@Override
	public boolean checkminus(int val, ErrorControl ctrl) {
		if(val < 0) {
			ctrl.setError("Negative Zahl ist nicht erlaubt");
			return false;
		}
		else {
			return true;
		}
	}

}
