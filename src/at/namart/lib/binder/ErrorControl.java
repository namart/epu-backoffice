package at.namart.lib.binder;

import java.awt.Color;


import javax.swing.JPanel;
import javax.swing.JTextArea;

import at.namart.lib.logger.Logger;

public class ErrorControl extends JPanel {
	
	private JTextArea messageBox;
	public boolean hasError;
	

	public ErrorControl() {
		
		messageBox = new JTextArea();
		messageBox.setForeground( Color.red );
		messageBox.setBackground(Color.yellow);
		messageBox.setEditable(false);
	    add(messageBox);
	    
	    this.setVisible(false);
	    this.setBackground(Color.yellow);

	}

	public void setError(String message) {
		hasError = true;
		messageBox.append(message + "\n");
		this.setVisible(true);
	}
	
	public void resetErrorControl() {
		this.setVisible(false);
		messageBox.setText("");
		hasError = false;
	}
}
