package at.namart.lib.binder;

import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import javax.swing.JTextField;

import at.namart.lib.logger.Level;
import at.namart.lib.logger.Logger;


public class MyBinder {
	
	private ErrorControl errorControl = new ErrorControl();
	
	
	public ErrorControl getErrorControl() {
		return errorControl;
	}

	public String BindFrom_String (JTextField ctrl, IRule rule ) {
		
		// �Converter�
		String result = ctrl.getText(); 
		
	    if(rule  != null)
	    {  
	        // Regel ausf�hren
	    	rule .Eval(result, errorControl); 
	    }
	    
		return result;
	}
	
	public String BindFrom_Email (JTextField ctrl, IRule rule ) {
		
		// �Converter�
		String result = ctrl.getText(); 
		
	    if(rule  != null)
	    {  
	        // Regel ausf�hren
	    	rule .checkEmail(result, errorControl); 
	    }
	    
		return result;
	}
	
	public int BindFrom_Percent(JTextField ctrl, IRule rule ) {
		
		int result = 0;
		// Converter
	    try{
	    	result = Integer.parseInt(ctrl.getText());
	    } catch(NumberFormatException e) {
	    	errorControl.setError("Sie m�ssen eine Zahl eingeben");
	    	Logger.log(Level.TRACE , "NumberFormatException", e);
	        return 0;
	    }
		
	    if(rule  != null)
	    {  
	        // Regel ausf�hren
	    	rule .checkPercent(result, errorControl); 
	    }
	    
		return result;
	}
	
	public int BindFrom_Telefon(JTextField ctrl, IRule rule ) {
		
		int result = 0;
		// Converter
	    try{
	    	result = Integer.parseInt(ctrl.getText());
	    } catch(NumberFormatException e) {
	    	errorControl.setError("Sie m�ssen eine Zahl eingeben");
	    	Logger.log(Level.TRACE , "NumberFormatException", e);
	        return 0;
	    }
		
	    if(rule  != null)
	    {  
	        // Regel ausf�hren
	    	rule .checktelefon(result, errorControl); 
	    }
	    
		return result;
	}
	
public int BindFrom_Minus(JTextField ctrl, IRule rule ) {
		
		int result = 0;
		// Converter
	    try{
	    	result = Integer.parseInt(ctrl.getText());
	    } catch(NumberFormatException e) {
	    	errorControl.setError("Sie m�ssen eine Zahl eingeben");
	    	Logger.log(Level.TRACE , "NumberFormatException", e);
	        return 0;
	    }
		
	    if(rule  != null)
	    {  
	        // Regel ausf�hren
	    	rule .checkminus(result, errorControl); 
	    }
	    
		return result;
	}
	
	public Date BindFrom_Date(JTextField ctrl, IRule rule ) {
		
		// �Converter�
		Date result = null;
		try {
			result = new java.sql.Date(new SimpleDateFormat("dd/MM/yyyy").parse(ctrl.getText()).getTime());
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			errorControl.setError("Bitte geben sie ein korrektes Datum ein. Format: dd/MM/yyyy");
		}
		
	    if(rule  != null)
	    {  
	        // Regel ausf�hren
	    	rule .Eval(result, errorControl); 
	    }
	    
		return result;
	}

	public int BindFrom_Int(JTextField ctrl, IRule rule) {
		
	    int result = 0;
	    
	    // Converter
	    try{
	    	result = Integer.parseInt(ctrl.getText());
	    } catch(NumberFormatException e) {
	    	errorControl.setError("Sie m�ssen eine Zahl eingeben");
	    	Logger.log(Level.TRACE , "NumberFormatException", e);
	        return 0;
	    }

	    if(rule != null) { 
	    	// Regel ausf�hren
	        rule.Eval(result, errorControl); 
	    }
	    return result;
	}
	
	public float BindFrom_Float(JTextField ctrl, IRule rule) {
		
	    float result = 0;
	    
	    // Converter
	    try{
	    	result = Float.parseFloat(ctrl.getText());
	    } catch(NumberFormatException e) {
	    	errorControl.setError("Sie m�ssen eine Zahl eingeben");
	    	Logger.log(Level.TRACE , "NumberFormatException", e);
	        return 0;
	    }

	    if(rule != null) { 
	    	// Regel ausf�hren
	        rule.Eval(result, errorControl); 
	    }
	    return result;
	}
	
	public boolean hasError() {
		return errorControl.hasError;
	}
	
	public void setError(String message) {
		errorControl.setError(message);
	}
	
	public void resetErrorControl() {
		errorControl.resetErrorControl();
	}
}
