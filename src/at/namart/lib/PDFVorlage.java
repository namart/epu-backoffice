package at.namart.lib;

import at.namart.lib.logger.Logger;

import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Rectangle;

import java.io.FileOutputStream;

import com.itextpdf.text.*;
import com.itextpdf.text.pdf.*;

public abstract class PDFVorlage {
	
	protected Document document;
	protected PdfWriter writer;
	protected BaseFont bf_helv;
	protected BaseFont bf_times;
	protected BaseFont bf_courier;
	protected BaseFont bf_symbol;
	
	public PDFVorlage( String filename, String path, String headerText) {
		
		document = new Document(PageSize.A4, 50, 50, 50, 50);
        try {
            // creation of the different writers
            writer = PdfWriter.getInstance(document, new FileOutputStream(path + filename + ".pdf"));
			writer.setBoxSize("art" , new Rectangle(36, 54, 559, 788));

            HeaderFooter event = new HeaderFooter(headerText);
            writer.setPageEvent(event);

            // various fonts
            bf_helv = BaseFont.createFont(BaseFont.HELVETICA, "Cp1252", false);
            bf_times = BaseFont.createFont(BaseFont.TIMES_ROMAN, "Cp1252", false);
            bf_courier = BaseFont.createFont(BaseFont.COURIER, "Cp1252", false);
            bf_symbol = BaseFont.createFont(BaseFont.SYMBOL, "Cp1252", false);
            
        }  catch (DocumentException ex) {
			Logger.trace(ex.getMessage());
			ex.printStackTrace();
		} catch (Exception ex) {
        	Logger.trace(ex.getMessage());
        	ex.printStackTrace();
        }

	}
	
	protected void content() {
		
		try {

            document.open();

            document.add(new Paragraph("Hello World"));

            document.close();			
			
		} catch (DocumentException ex) {
			Logger.trace(ex.getMessage());
			ex.printStackTrace();
		} catch (Exception ex) {
        	Logger.trace(ex.getMessage());
        	ex.printStackTrace();
        }
		
		Logger.debug("pdf created");
	}

	/** Inner class to add a header and a footer. */
    static class HeaderFooter extends PdfPageEventHelper {
    	
    	private String headerText;
    	
    	public HeaderFooter(String headerText) {
    		this.headerText = headerText;
    	}

        public void onEndPage (PdfWriter writer, Document document) {
            Rectangle rect = writer.getBoxSize("art");
            Phrase pr = new Phrase(this.headerText);
            pr.getFont().setStyle("line-height:40px; height:40px;");
            pr.getFont().setSize(15);
            
            ColumnText.showTextAligned(writer.getDirectContent(),
                    Element.ALIGN_LEFT, pr,
                    rect.getLeft(), rect.getTop(), 0);

            ColumnText.showTextAligned(writer.getDirectContent(),
                    Element.ALIGN_CENTER, new Phrase(String.format("page %d", writer.getPageNumber())),
                    (rect.getLeft() + rect.getRight()) / 2, rect.getBottom() - 18, 0);
        }
    }
}
