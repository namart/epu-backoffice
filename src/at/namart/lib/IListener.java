package at.namart.lib;

import java.util.EventListener;



public interface IListener extends EventListener  {

	/** Invoked when an change occurs. */ 
	void change(NotiEvent e); 
}
