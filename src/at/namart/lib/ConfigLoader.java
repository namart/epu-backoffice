package at.namart.lib;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class ConfigLoader {
	
	private static final ConfigLoader instance = new ConfigLoader();

	private Properties configProp = new Properties();


	private ConfigLoader() {

		InputStream in = this.getClass().getClassLoader()
				.getResourceAsStream("config.properties");
		try {
			configProp.load(in);
		} catch (IOException e) {
			e.printStackTrace();
		}

	}
	
	public String getProperty(String key) {
		return configProp.getProperty(key);
	}

	public static ConfigLoader getInstance() {
		return instance;
	}

}
