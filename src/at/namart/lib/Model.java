package at.namart.lib;

import javax.swing.event.EventListenerList;


public abstract class Model {
	
	public static final int INVALIDID = -1;
	public static final int UNDEFINED = -1;
	
	/** List of listeners. */ 
	private EventListenerList listeners = new EventListenerList();
	
	/**
	 * Adds an {@code ProduktListener}  to the model.
	 */
	public void addProduktListener(IListener listener) {
		listeners.add(IListener.class, listener);
	}

	/**
	 * Removes an {@code ProduktListener} from the model.
	 */

	public void removeProduktListener(IListener listener) {
		listeners.remove(IListener.class, listener);
	}

	/**
	 * Notifies all {@code AdListener}s that have registered interest for
	 * notification on an {@code AdEvent}.
	 * 
	 * @param event
	 *            the {@code AdEvent} object
	 * @see EventListenerList
	 */
	protected void notifyView(NotiEvent event) {
		for (IListener l : listeners.getListeners(IListener.class)) {
			l.change(event);
			
		}
	}

}
