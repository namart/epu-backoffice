package at.namart.lib.pdfs;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Font;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;

import at.namart.lib.PDFVorlage;
import at.namart.lib.logger.Logger;
import at.namart.model.bo.Angebot;
import at.namart.model.bo.Ausgangsrechnung;

public class AusgangsrechnungPdf extends PDFVorlage {

	public AusgangsrechnungPdf(Ausgangsrechnung a) {
		super("Ausgangsrechnung_"+a.getId(), "", "Ausgangsrechnung");
	}

	public void createRechnungenTable(Ausgangsrechnung a, ArrayList<Angebot> rz) {
		
		try {

            document.open();

            Paragraph par;
            
            // Rechnungsnummer
            par = new Paragraph("AusgangsrechungsNr.: " + a.getId());
            par.getFont().setStyle(Font.BOLD);
            document.add(par);

            //Tabelle mit Rechnenzeilen
            
            PdfPTable table = new PdfPTable(5); // Tabelle erstellen
        	table.setSpacingBefore(10.0f); // Abstand zum vorigen Element setzen
        	PdfPCell cell;
        	
        	cell = new PdfPCell(new Paragraph("Nr.")); // Header
        	table.addCell(cell);
        	cell = new PdfPCell(new Paragraph("Name")); // Header
        	table.addCell(cell);
        	cell = new PdfPCell(new Paragraph("Summe")); // Header
        	table.addCell(cell);
        	cell = new PdfPCell(new Paragraph("Dauer")); // Header
        	table.addCell(cell);
        	cell = new PdfPCell(new Paragraph("Datum")); // Header
        	table.addCell(cell);
        	
        	int sumSumme = 0;
        	int sumDauer = 0;
        	
            for (Angebot r : rz) {

            	table.addCell(Integer.toString(r.getId()));
            	table.addCell(r.getName());
            	table.addCell(Integer.toString(r.getSumme()));
            	table.addCell(Integer.toString(r.getDauer()));
            	table.addCell(new SimpleDateFormat("dd/MM/yyyy").format(r.getDatum()));  
            	
            	sumSumme += r.getSumme();
            	sumDauer += r.getDauer();
   
            }
            document.add(table);
            
            // Summe
            par = new Paragraph("Summer: " + sumSumme);
            par.getFont().setStyle(Font.BOLD);
            document.add(par);
            
            // Gesamt Dauer
            par = new Paragraph("Gesamtdauer: " + sumDauer);
            par.getFont().setStyle(Font.BOLD);
            document.add(par);
            
            document.close();			
			
		} catch (DocumentException ex) {
			Logger.trace(ex.getMessage());
			ex.printStackTrace();
		} catch (Exception ex) {
        	Logger.trace(ex.getMessage());
        	ex.printStackTrace();
        }
		
		Logger.debug("pdf created");
		
	}
	
}
