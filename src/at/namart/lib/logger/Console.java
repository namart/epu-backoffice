package at.namart.lib.logger;

public class Console implements IAppender {

	@Override
	public void configure() {
		/* nichts zu tun */
	}

	@Override
	public void write(Level level, Object message, Exception ex, String StackTrace) {
		System.out.println(message);
		if(ex != null) {
			System.out.println(ex);
		}
	}

}
