package at.namart.lib.logger;

import at.namart.lib.ConfigLoader;


public class Logger {
	
	private static AppenderManager manager = new AppenderManager();
	private static Level levelObject;
	
	public static void configure() {

		String value = ConfigLoader.getInstance().getProperty("mylogger.rootLogger");
		
		if (value.equals("ALL")) {
			levelObject = Level.ALL;
		} else if (value.equals("TRACE")) {
			levelObject = Level.TRACE ;
		} else if (value.equals("DEBUG")){
			levelObject = Level.DEBUG ;
		} else if (value.equals("INFO")) {
			levelObject = Level.INFO ;
		} else if (value.equals("WARN")) {
			levelObject = Level.WARN ;
		} else if (value.equals("ERROR")) {
			levelObject = Level.ERROR ;
		} else if (value.equals("FATAL")) {
			levelObject = Level.FATAL ;
		} else if (value.equals("OFF")) {
			levelObject = Level.OFF;
		}

		
	}
	
	public static void log(Level level, Object msg) {
		if(levelObject == null) {
			configure();
		}
		if (level.intValue() < levelObject.intValue()) {
		    return;
		}
		
		manager.write(level, msg, null, null);
	}
	
	public static void log(Level level, Object msg, Exception e) {
		if(levelObject == null) {
			configure();
		}
		if (level.intValue() < levelObject.intValue()) {
		    return;
		}
		
		manager.write(level, msg, e, null);
		
	}
	
	public static void trace(Object msg){
		if(levelObject == null) {
			configure();
		}
		if (Level.TRACE .intValue() < levelObject.intValue()) {
		    return;
		}
		log(Level.TRACE , msg);
	} 
	
	public static void debug(Object msg){
		if(levelObject == null) {
			configure();
		}
		if (Level.DEBUG .intValue() < levelObject.intValue()) {
		    return;
		}
		log(Level.DEBUG , msg);
	} 
	
	public static void info(Object msg){
		if(levelObject == null) {
			configure();
		}
		if (Level.INFO .intValue() < levelObject.intValue()) {
		    return;
		}
		log(Level.INFO , msg);
	}
	public static void warn(Object msg){
		if(levelObject == null) {
			configure();
		}
		if (Level.WARN .intValue() < levelObject.intValue()) {
		    return;
		}
		log(Level.WARN , msg);
	} 
	
	public static void error(Object msg){
		if(levelObject == null) {
			configure();
		}
		if (Level.ERROR .intValue() < levelObject.intValue()) {
		    return;
		}
		log(Level.ERROR , msg);
	} 
	
	public static void fatal(Object msg){
		if(levelObject == null) {
			configure();
		}
		if (Level.FATAL .intValue() < levelObject.intValue()) {
		    return;
		}
		log(Level.FATAL , msg);
	}
	
	public void setLevel(Level newLevel) {
		levelObject = newLevel;
	}

}
