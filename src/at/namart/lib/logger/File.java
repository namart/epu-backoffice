package at.namart.lib.logger;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

public class File implements IAppender {

	@Override
	public void configure() {
		// TODO Auto-generated method stub

	}

	@Override
	public void write(Level level, Object message, Exception ex, String StackTrace) {
		BufferedWriter out;
		
		try {
			out = new BufferedWriter (new FileWriter("log.txt", true));
			out.newLine();
			out.write(level.getName());
			out.newLine();
			out.write(message.toString());
			out.newLine();
			out.write("=======end======");
			out.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
