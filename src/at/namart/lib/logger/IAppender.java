package at.namart.lib.logger;

public interface IAppender {

	public void configure(); 
	public void write(Level level, Object message, Exception ex, String StackTrace); 
}
