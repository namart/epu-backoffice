package at.namart.lib.logger;

import java.util.ArrayList;

import at.namart.lib.ConfigLoader;

public class AppenderManager {
	
	private ConfigLoader config = ConfigLoader.getInstance();
	private ArrayList<IAppender> appenders = new ArrayList<IAppender>();
	
	public AppenderManager () {
		configure();
	}

	public void configure() {
		try {
	
			Class<?> c = Class.forName( config.getProperty("mylogger.appender.stout") );
			Class<?> f = Class.forName( config.getProperty("mylogger.appender.file") );
			try {
				appenders.add((IAppender) c.newInstance());
				appenders.add((IAppender) f.newInstance());
			} catch (InstantiationException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
	}

	public void write(Level level, Object message, Exception ex, String StackTrace) {
		
		if(appenders.isEmpty()){
			return;
		}
		
		for(IAppender a : appenders) {
			a.write(level, message, ex, StackTrace);
		}
	}

}
