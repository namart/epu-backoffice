package at.namart.lib.logger;

public class Level {

	public static final Level ALL = new Level("ALL", 0);
	public static final Level TRACE  = new Level("TRACE ", 1);
	public static final Level DEBUG  = new Level("DEBUG ", 2);
	public static final Level INFO  = new Level("INFO ", 3);
	public static final Level WARN  = new Level("WARN ", 4);
	public static final Level ERROR  = new Level("ERROR ", 5);
	public static final Level FATAL  = new Level("FATAL ", 6);
	public static final Level OFF = new Level("OFF", 7);
	private final String name;
	private final int value;
	
	protected Level(String name, int value) {
		if (name == null) {
		    throw new NullPointerException(); 
	        }
	        this.name = name;
	        this.value = value;
	}
	
	public String getName() {
			return name;
	}
	
	public final int intValue() {
			return value;
	}
}
