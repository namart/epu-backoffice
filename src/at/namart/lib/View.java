package at.namart.lib;

import javax.swing.JPanel;


public abstract class View extends JPanel implements IListener {
	

	@Override
	public void change(NotiEvent e) {
		/* should be overridden */
	}

}
