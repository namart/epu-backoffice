package at.namart.lib;

import java.util.EventObject;

public class NotiEvent extends EventObject {
	
	private String name;

	public NotiEvent(Object source, String name) {
		super( source ); 
	    this.name = name; 
	}
	
	public String getName() {
		return this.name;
	}

}
