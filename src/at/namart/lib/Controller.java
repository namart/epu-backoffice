package at.namart.lib;


public abstract class Controller<T> {

	protected T view;
	
	public Controller(T view) {
		this.view = view;
	}
	
	public T getView() {
		return view;
	}
	
	public void tabChange() {
		//for overriding
	}

}
