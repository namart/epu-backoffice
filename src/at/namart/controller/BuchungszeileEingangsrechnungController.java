package at.namart.controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import at.namart.lib.Controller;
import at.namart.lib.binder.ErrorControl;
import at.namart.model.BuchungszeileEingangsrechnungModel;
import at.namart.model.bo.Eingangsrechnung;
import at.namart.model.services.DALException;
import at.namart.view.BuchungszeileEingangsrechnungView;
import at.namart.view.FormWrapperView;

public class BuchungszeileEingangsrechnungController extends Controller<BuchungszeileEingangsrechnungView> {

	private BuchungszeileEingangsrechnungModel model;
	private ErrorControl viewErrorControl;
	private int BuchungszeileId;
	
	public BuchungszeileEingangsrechnungController(FormWrapperView<?> view, int buchungszeile_Id) {
		
		//Set View
		super((BuchungszeileEingangsrechnungView) view.getView());
		
		//Get ErrorControl of the View
		viewErrorControl = getView().getMyBinder().getErrorControl();
		
		//Set relation to Buchungszeile
		this.BuchungszeileId = buchungszeile_Id;
		
		//Set Handlers
		getView().setSaveButtonListener(new SaveButtonListener());
		getView().setDeleteButtonListener(new DeleteButtonListener());
		getView().setRefreshButtonListener(new RefreshButtonListener());
		
		//Set Model
		model = BuchungszeileEingangsrechnungModel.getInstance();
		
		//Get first Objects
		try {
			model.getBuchungszeileEingangsrechnungList(BuchungszeileId);
			model.getNotUsedEingangsrechnungList(BuchungszeileId);
		} catch (DALException ex) {
			viewErrorControl.setError(ex.getMessage());
		}
	}
	
	class SaveButtonListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {	
 	
            // Dem BL zum speichern čbergeben
        	try {
        		
        		Eingangsrechnung r = getView().getAuswahlComponent();
				model.saveBuchungszeileEingangsrechnung(BuchungszeileId, r);
			} catch (DALException ex) {
				viewErrorControl.setError(ex.getMessage());
			}

		}
	}
	
	class DeleteButtonListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {	
			
			try {
				Eingangsrechnung r = getView().getComponent();
				model.deleteBuchungszeileEingangsrechnung(BuchungszeileId, r);
			} catch (DALException ex) {
				viewErrorControl.setError(ex.getMessage());
			}

		}
	}
	
	class RefreshButtonListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {	
			
			try {
				model.getBuchungszeileEingangsrechnungList(BuchungszeileId);
				model.getNotUsedEingangsrechnungList(BuchungszeileId);
			} catch (DALException ex) {
				viewErrorControl.setError(ex.getMessage());
			}

		}
	}
}
