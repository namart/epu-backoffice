package at.namart.controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import at.namart.lib.Controller;
import at.namart.lib.binder.ErrorControl;
import at.namart.model.BuchungszeileAusgangsrechnungModel;
import at.namart.model.bo.Ausgangsrechnung;
import at.namart.model.services.DALException;
import at.namart.view.BuchungszeileAusgangsrechnungView;
import at.namart.view.FormWrapperView;

public class BuchungszeileAusgangsrechnungController extends Controller<BuchungszeileAusgangsrechnungView> {

	private BuchungszeileAusgangsrechnungModel model;
	private ErrorControl viewErrorControl;
	private int BuchungszeileId;
	
	public BuchungszeileAusgangsrechnungController(FormWrapperView<?> view, int buchungszeile_Id) {
		
		//Set View
		super((BuchungszeileAusgangsrechnungView) view.getView());
		
		//Get ErrorControl of the View
		viewErrorControl = getView().getMyBinder().getErrorControl();
		
		//Set relation to Buchungszeile
		this.BuchungszeileId = buchungszeile_Id;
		
		//Set Handlers
		getView().setSaveButtonListener(new SaveButtonListener());
		getView().setDeleteButtonListener(new DeleteButtonListener());
		getView().setRefreshButtonListener(new RefreshButtonListener());
		
		//Set Model
		model = BuchungszeileAusgangsrechnungModel.getInstance();
		
		//Get first Objects
		try {
			model.getBuchungszeileAusgangsrechnungList(BuchungszeileId);
			model.getNotUsedAusgangsrechnungList(BuchungszeileId);
		} catch (DALException ex) {
			viewErrorControl.setError(ex.getMessage());
		}
	}
	
	class SaveButtonListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {	
 	
            // Dem BL zum speichern čbergeben
        	try {
        		
        		Ausgangsrechnung r = getView().getAuswahlComponent();
				model.saveBuchungszeileAusgangsrechnung(BuchungszeileId, r);
			} catch (DALException ex) {
				viewErrorControl.setError(ex.getMessage());
			}

		}
	}
	
	class DeleteButtonListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {	
			
			try {
				Ausgangsrechnung r = getView().getComponent();
				model.deleteBuchungszeileAusgangsrechnung(BuchungszeileId, r);
			} catch (DALException ex) {
				viewErrorControl.setError(ex.getMessage());
			}

		}
	}
	
	class RefreshButtonListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {	
			
			try {
				model.getBuchungszeileAusgangsrechnungList(BuchungszeileId);
				model.getNotUsedAusgangsrechnungList(BuchungszeileId);
			} catch (DALException ex) {
				viewErrorControl.setError(ex.getMessage());
			}

		}
	}
}
