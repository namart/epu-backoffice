package at.namart.controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import at.namart.lib.Controller;
import at.namart.lib.binder.ErrorControl;
import at.namart.model.RechnungszeileModel;
import at.namart.model.bo.Rechnungszeile;
import at.namart.model.services.DALException;
import at.namart.view.FormWrapperView;
import at.namart.view.RechnungszeilenView;

public class RechnungszeilenController extends Controller<RechnungszeilenView> {
	
	private RechnungszeileModel model;
	private ErrorControl viewErrorControl;
	private int AusgangsrechnungId;

	public RechnungszeilenController(FormWrapperView<?> view, int AusgangsrechnungId) {
		
		//Set View
		super((RechnungszeilenView) view.getView());
		
		//Get ErrorControl of the View
		viewErrorControl = getView().getMyBinder().getErrorControl();
		
		//Set Handlers
		getView().setNewButtonListener(new NewButtonListener());
		getView().setSaveButtonListener(new SaveButtonListener());
		getView().setDeleteButtonListener(new DeleteButtonListener());
		getView().setRefreshButtonListener(new RefreshButtonListener());
		
		//Set Model
		model = RechnungszeileModel.getInstance();
		
		//Set relation to Ausgansrechnung
		this.AusgangsrechnungId = AusgangsrechnungId;
		
		//Get first Objects
		try {
			model.getRechnungszeileByIdList(AusgangsrechnungId);
			model.getAngebotList();
		} catch (DALException ex) {
			viewErrorControl.setError(ex.getMessage());
		}
	}
	
	class NewButtonListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {	
			getView().setComponent(null);
		}
	}
	
	class SaveButtonListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {	
			 // Binden
            if(getView().BindFrom()) {      	
            	
                // Dem BL zum speichern čbergeben
            	try {
            		
            		Rechnungszeile r = getView().getComponent();
            		r.setAusgangsrechnungId(AusgangsrechnungId);
					model.saveRechnungszeile(r);
				} catch (DALException ex) {
					viewErrorControl.setError(ex.getMessage());
				}
                
                // Binden
            	getView().BindTo();

            }
		}
	}
	
	class DeleteButtonListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {	
			
			try {
				Rechnungszeile r = getView().getComponent();
        		r.setAusgangsrechnungId(AusgangsrechnungId);
				model.deleteRechnungszeile(r);
			} catch (DALException ex) {
				viewErrorControl.setError(ex.getMessage());
			}
			if(!viewErrorControl.hasError)
				getView().setComponent(null);
		}
	}
	
	class RefreshButtonListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {	
			
			try {
				model.getRechnungszeileByIdList(AusgangsrechnungId);
			} catch (DALException ex) {
				viewErrorControl.setError(ex.getMessage());
			}
			getView().setComponent(null);
		}
	}

}
