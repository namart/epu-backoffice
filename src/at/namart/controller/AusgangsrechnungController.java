package at.namart.controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import at.namart.lib.Controller;
import at.namart.lib.binder.ErrorControl;
import at.namart.model.AusgangsrechnungModel;
import at.namart.model.services.DALException;
import at.namart.view.AusgangsrechnungView;

public class AusgangsrechnungController extends Controller<AusgangsrechnungView> {
	
	private AusgangsrechnungModel model;
	private ErrorControl viewErrorControl;

	public AusgangsrechnungController(AusgangsrechnungView view) {
		
		//Set View
		super(view);
		
		//Get ErrorControl of the View
		viewErrorControl = getView().getMyBinder().getErrorControl();
		
		//Set Handlers
		getView().setNewButtonListener(new NewButtonListener());
		getView().setSaveButtonListener(new SaveButtonListener());
		getView().setDeleteButtonListener(new DeleteButtonListener());
		getView().setRefreshButtonListener(new RefreshButtonListener());
		getView().setPdfButtonListener(new createPdfButtonListener());
		
		//Set Model
		model = AusgangsrechnungModel.getInstance();
		
		//Get first Objects
		try {
			model.getAusgangsrechnungList();
		} catch (DALException ex) {
			viewErrorControl.setError(ex.getMessage());
		}
	}
	
	class NewButtonListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {	
			getView().setComponent(null);
		}
	}
	
	class SaveButtonListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {	
			 // Binden
            if(getView().BindFrom()) {      	
            	
                // Dem BL zum speichern čbergeben
            	try {
					model.saveAusgangsrechnung(getView().getComponent());
				} catch (DALException ex) {
					viewErrorControl.setError(ex.getMessage());
				}
                
                // Binden
            	getView().BindTo();

            }
		}
	}
	
	class DeleteButtonListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {	
			
			try {
				model.deleteAusgangsrechnung(getView().getComponent());
			} catch (DALException ex) {
				viewErrorControl.setError(ex.getMessage());
			}
			if(!viewErrorControl.hasError)
				getView().setComponent(null);
		}
	}
	
	class RefreshButtonListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {	
			
			try {
				model.getAusgangsrechnungList();
			} catch (DALException ex) {
				viewErrorControl.setError(ex.getMessage());
			}
			getView().setComponent(null);
		}
	}
	
	class createPdfButtonListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {	
			if(getView().BindFrom()) { 
				try {
					model.createPdf(getView().getComponent());
				} catch (DALException ex) {
					viewErrorControl.setError(ex.getMessage());
				} catch (Exception ex) {
					viewErrorControl.setError(ex.getMessage());
				}
			}
		}
	}

	@Override
	public void tabChange() {
		
		try {
			model.getKundeList();
		} catch (DALException ex) {
			viewErrorControl.setError(ex.getMessage());
		}
	}

}
