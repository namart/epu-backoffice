package at.namart.controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import at.namart.lib.Controller;
import at.namart.lib.binder.ErrorControl;
import at.namart.model.KundeModel;
import at.namart.model.services.DALException;
import at.namart.view.KundeView;

public class KundeController extends Controller<KundeView> {
	
	private KundeModel model;
	private ErrorControl viewErrorControl;

	public KundeController(KundeView view) {
		
		//Set View
		super(view);
		
		//Get ErrorControl of the View
		viewErrorControl = getView().getMyBinder().getErrorControl();
		
		//Set Handlers
		getView().setNewButtonListener(new NewButtonListener());
		getView().setSaveButtonListener(new SaveButtonListener());
		getView().setDeleteButtonListener(new DeleteButtonListener());
		getView().setRefreshButtonListener(new RefreshButtonListener());
		
		//Set Model
		model = KundeModel.getInstance();
		
		//Get first Objects
		try {
			model.getKundeList();
		} catch (DALException ex) {
			viewErrorControl.setError(ex.getMessage());
		}
	}
	
	class NewButtonListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {	
			getView().setComponent(null);
		}
	}
	
	class SaveButtonListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {	
			 // Binden
            if(getView().BindFrom()) {      	
            	
                // Dem BL zum speichern čbergeben
            	try {
					model.saveKunde(getView().getComponent());
				} catch (DALException ex) {
					viewErrorControl.setError(ex.getMessage());
				}
                
                // Binden
            	getView().BindTo();

            }
		}
	}
	
	class DeleteButtonListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {	
			
			try {
				model.deleteKunde(getView().getComponent());
			} catch (DALException ex) {
				viewErrorControl.setError(ex.getMessage());
			}
			if(!viewErrorControl.hasError)
				getView().setComponent(null);
		}
	}
	
	class RefreshButtonListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {	
			
			try {
				model.getKundeList();
			} catch (DALException ex) {
				viewErrorControl.setError(ex.getMessage());
			}

			getView().setComponent(null);
		}
	}
	
	@Override
	public void tabChange() {
		
		try {
			model.getKontaktList();
		} catch (DALException ex) {
			viewErrorControl.setError(ex.getMessage());
		}
	}

}
