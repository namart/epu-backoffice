package at.namart.controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import at.namart.lib.Controller;
import at.namart.lib.binder.ErrorControl;
import at.namart.model.AngebotModel;
import at.namart.model.services.DALException;
import at.namart.view.AngebotView;

public class AngebotController extends Controller<AngebotView> {
	
	private AngebotModel model;
	private ErrorControl viewErrorControl;

	public AngebotController(AngebotView view) {
		
		//Set View
		super(view);
		
		//Get ErrorControl of the View
		viewErrorControl = getView().getMyBinder().getErrorControl();
		
		//Set Handlers
		getView().setNewButtonListener(new NewButtonListener());
		getView().setSaveButtonListener(new SaveButtonListener());
		getView().setDeleteButtonListener(new DeleteButtonListener());
		getView().setRefreshButtonListener(new RefreshButtonListener());
		
		//Set Model
		model = AngebotModel.getInstance();
		
		//Get first Objects
		try {
			model.getAngebotList();
		} catch (DALException ex) {
			viewErrorControl.setError(ex.getMessage());
		}
	}
	
	class NewButtonListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {	
			getView().setComponent(null);
		}
	}
	
	class SaveButtonListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {	
			 // Binden
            if(getView().BindFrom()) {      	
            	
                // Dem BL zum speichern čbergeben
            	try {
					model.saveAngebot(getView().getComponent());
				} catch (DALException ex) {
					viewErrorControl.setError(ex.getMessage());
				}
                
                // Binden
            	getView().BindTo();

            }
		}
	}
	
	class DeleteButtonListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {	
			
			try {
				model.deleteAngebot(getView().getComponent());
			} catch (DALException ex) {
				viewErrorControl.setError(ex.getMessage());
			}
			if(!viewErrorControl.hasError)
				getView().setComponent(null);
		}
	}
	
	class RefreshButtonListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {	
			
			try {
				model.getAngebotList();
			} catch (DALException ex) {
				viewErrorControl.setError(ex.getMessage());
			}
			getView().setComponent(null);
		}
	}

	@Override
	public void tabChange() {
		
		try {
			model.getProjektList();
			model.getKundeList();
		} catch (DALException ex) {
			viewErrorControl.setError(ex.getMessage());
		}
	}
	
	

}
