package tests;


import static org.junit.Assert.*;

import javax.swing.JTextField;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import at.namart.lib.binder.MyBinder;
import at.namart.lib.binder.RequiredRule;
import at.namart.lib.logger.Logger;

public class BinderTest {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}
	
	@Test
	public void testBindFrom_String () {
		
		MyBinder myBinder = new MyBinder();
		JTextField fieldString = new JTextField();
		
		fieldString.setText("test");
		
		myBinder.BindFrom_String (fieldString, new RequiredRule());		
		assertFalse("expecting no error", myBinder.hasError());
		myBinder.resetErrorControl();
		
		fieldString.setText("");
		
		myBinder.BindFrom_String (fieldString, new RequiredRule());		
		assertTrue("expecting error", myBinder.hasError());
		myBinder.resetErrorControl();
	
	}
	
	@Test
	public void testBindFrom_Email () {
		
		MyBinder myBinder = new MyBinder();
		JTextField fieldString = new JTextField();
		
		fieldString.setText("asda@asd.at");
		
		myBinder.BindFrom_Email (fieldString, new RequiredRule());		
		assertFalse("expecting no error", myBinder.hasError());
		myBinder.resetErrorControl();
		
		fieldString.setText("asdf@asd");
		
		myBinder.BindFrom_Email (fieldString, new RequiredRule());		
		assertTrue("expecting error", myBinder.hasError());
		myBinder.resetErrorControl();
	
	}
	
	@Test
	public void testBindFrom_Telefon() {
		
		MyBinder myBinder = new MyBinder();
		JTextField fieldString = new JTextField();
		
		fieldString.setText("123124234");
		myBinder.BindFrom_Telefon(fieldString, new RequiredRule());		
		assertFalse("expecting no error", myBinder.hasError());
		myBinder.resetErrorControl();
		
		fieldString.setText("-1");	
		myBinder.BindFrom_Telefon(fieldString, new RequiredRule());		
		assertTrue("expecting error", myBinder.hasError());
		myBinder.resetErrorControl();
		
		fieldString.setText("1234");	
		myBinder.BindFrom_Telefon(fieldString, new RequiredRule());		
		assertTrue("expecting error", myBinder.hasError());
		myBinder.resetErrorControl();
	
	}
	
	@Test
	public void testBindFrom_Minus() {
		
		MyBinder myBinder = new MyBinder();
		JTextField fieldString = new JTextField();
		
		fieldString.setText("-11234");
		myBinder.BindFrom_Minus(fieldString, new RequiredRule());		
		assertTrue("expecting no error", myBinder.hasError());
		myBinder.resetErrorControl();
		
		fieldString.setText("123");	
		myBinder.BindFrom_Minus(fieldString, new RequiredRule());		
		assertFalse("expecting error", myBinder.hasError());
		myBinder.resetErrorControl();
	
	}
	
	@Test
	public void testBindFrom_Date() {
		
		MyBinder myBinder = new MyBinder();
		JTextField fieldString = new JTextField();
		
		fieldString.setText("5/5/1999");
		
		myBinder.BindFrom_Date(fieldString, new RequiredRule());		
		assertFalse("expecting no error", myBinder.hasError());
		myBinder.resetErrorControl();
		
		fieldString.setText("3/3/");
		
		myBinder.BindFrom_Date(fieldString, new RequiredRule());		
		assertTrue("expecting error", myBinder.hasError());
		myBinder.resetErrorControl();
	
	}
	
	@Test
	public void testBindFrom_Int() {
		
		MyBinder myBinder = new MyBinder();
		JTextField fieldString = new JTextField();
		
		fieldString.setText("123");
		
		myBinder.BindFrom_Int(fieldString, new RequiredRule());		
		assertFalse("expecting no error", myBinder.hasError());
		myBinder.resetErrorControl();
		
		fieldString.setText("23d");
		
		myBinder.BindFrom_Int(fieldString, new RequiredRule());		
		assertTrue("expecting error", myBinder.hasError());
		myBinder.resetErrorControl();
	
	}
	
	@Test
	public void testBindFrom_Float() {
		
		MyBinder myBinder = new MyBinder();
		JTextField fieldString = new JTextField();
		
		fieldString.setText("123.123");
		
		Float result1 = myBinder.BindFrom_Float(fieldString, new RequiredRule());
		Logger.trace(result1);
		assertTrue("should be 123.123",Float.toString(result1).equals("123.123"));
		myBinder.resetErrorControl();
	
	}
	
	@Test
	public void testBindFrom_Percent() {
		
		MyBinder myBinder = new MyBinder();
		JTextField fieldString = new JTextField();
		
		fieldString.setText("0");
		
		myBinder.BindFrom_Percent(fieldString, new RequiredRule());		
		assertFalse("expecting no error", myBinder.hasError());
		myBinder.resetErrorControl();
		
		fieldString.setText("100");
		
		myBinder.BindFrom_Percent(fieldString, new RequiredRule());		
		assertFalse("expecting no error", myBinder.hasError());
		myBinder.resetErrorControl();
		
		fieldString.setText("50");
		
		myBinder.BindFrom_Percent(fieldString, new RequiredRule());		
		assertFalse("expecting no error", myBinder.hasError());
		myBinder.resetErrorControl();
		
		fieldString.setText("23d");
		
		myBinder.BindFrom_Percent(fieldString, new RequiredRule());		
		assertTrue("expecting error", myBinder.hasError());
		myBinder.resetErrorControl();
		
		fieldString.setText("-1");
		
		myBinder.BindFrom_Percent(fieldString, new RequiredRule());		
		assertTrue("expecting error", myBinder.hasError());
		myBinder.resetErrorControl();
		
		fieldString.setText("99999");
		
		myBinder.BindFrom_Percent(fieldString, new RequiredRule());		
		assertTrue("expecting error", myBinder.hasError());
		myBinder.resetErrorControl();
	
	}

}
