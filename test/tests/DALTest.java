package tests;

import static org.junit.Assert.*;

import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import at.namart.lib.ConfigLoader;
import at.namart.lib.logger.Logger;
import at.namart.model.bo.Angebot;
import at.namart.model.bo.Ausgangsrechnung;
import at.namart.model.bo.Buchungszeile;
import at.namart.model.bo.Eingangsrechnung;
import at.namart.model.bo.Kategorie;
import at.namart.model.bo.Kontakt;
import at.namart.model.bo.Kunde;
import at.namart.model.bo.Projekt;
import at.namart.model.bo.Rechnungszeile;
import at.namart.model.bo.Zeit;
import at.namart.model.services.DALDatabase;
import at.namart.model.services.DALException;

public class DALTest {
	
	private static DALDatabase dal;
	private static ConfigLoader config = ConfigLoader.getInstance();
	
	@BeforeClass
	public static void setClassUp() {
		try {
			dal = new DALDatabase(config.getProperty("user"), config.getProperty("password"), config.getProperty("url-test"));
		} catch (DALException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}
	

	@Test
	public void testProjekt() {
		Projekt p = new Projekt(-1, "testProjekt");
		try {
			Projekt saveResult = dal.saveProjekt(p);
			
			ArrayList<Projekt> pl = dal.getProjektList();
			
			boolean deleteResult = dal.deleteProjekt(saveResult);
			
		    assertEquals("Wrong return value", p, saveResult);
		    assertEquals("Error in saving", pl.get(0).getName(), p.getName());
		    assertTrue("Expected non-null result", deleteResult);
		    
		    pl = dal.getProjektList();
		    
		    assertTrue("Should be empty", pl.isEmpty());
			
		} catch (DALException e) {
			fail("DALException: Error while saving");
			e.printStackTrace();
		}
	}

	@Test
	public void testZeiterfassung() {
		Date date = null;
		Projekt p = new Projekt(-1, "testProjekt");
		try {
			date = new Date(new SimpleDateFormat("dd/MM/yyyy").parse("5/5/2001").getTime());
		} catch (ParseException e1) {
			fail("ParseException: Error creating date format");
			e1.printStackTrace();
		}
		
		
		try {
			
			Projekt sp = dal.saveProjekt(p);
			
			Zeit z = new Zeit(-1, 22, date, sp.getId());
			
			Zeit saveResult = dal.saveZeiterfassung(z);
			
			ArrayList<Zeit> zl = dal.getZeiterfassungList();
			
			boolean deleteResult = dal.deleteZeiterfassung(saveResult);
			
		    assertEquals("Wrong return value", z, saveResult);
		    assertEquals("Error in saving", zl.get(0).getDauer(), z.getDauer());
		    assertTrue("Expected non-null result", deleteResult);
		    
		    zl = dal.getZeiterfassungList();
		    
		    assertTrue("Should be empty", zl.isEmpty());
		    
		    //aufr�umen
		    dal.deleteProjekt(sp);
			
		} catch (DALException e) {
			fail("DALException: Error while saving");
			e.printStackTrace();
		}
	}

	@Test
	public void testKunde() {
		Kunde k = new Kunde(-1, "testProjekt", 123, -1);
		try {
			Kunde saveResult = dal.saveKunde(k);
			
			ArrayList<Kunde> kl = dal.getKundeList();
			
			boolean deleteResult = dal.deleteKunde(saveResult);
			
		    assertEquals("Wrong return value", k, saveResult);
		    assertEquals("Error in saving", kl.get(0).getName(), k.getName());
		    assertTrue("Expected non-null result", deleteResult);
		    
		    kl = dal.getKundeList();
		    
		    assertTrue("Should be empty", kl.isEmpty());
			
		} catch (DALException e) {
			fail("DALException: Error while saving");
		}
	}

	@Test
	public void testAngebot() {
		
		Date date = null;
		try {
			date = new Date(new SimpleDateFormat("dd/MM/yyyy").parse("5/5/2001").getTime());
		} catch (ParseException e1) {
			fail("ParseException: Error creating date format");
			e1.printStackTrace();
		}
		
		Angebot a = new Angebot(-1, "testProjekt", 300, 23, date, 20, -1, -1);
		try {
			Angebot saveResult = dal.saveAngebot(a);
			
			ArrayList<Angebot> al = dal.getAngebotList();
			
			boolean deleteResult = dal.deleteAngebot(saveResult);
			
		    assertEquals("Wrong return value", a, saveResult);
		    assertEquals("Error in saving", al.get(0).getName(), a.getName());
		    assertEquals("Error in saving", al.get(0).getChance(), a.getChance());
		    assertTrue("Expected non-null result", deleteResult);
		    
		    al = dal.getAngebotList();
		    
		    assertTrue("Should be empty", al.isEmpty());
			
		} catch (DALException e) {
			Logger.trace(e.getMessage());
			fail("DALException: Error while saving");
			e.printStackTrace();
		}
	}

	@Test
	public void testKontakt() {
		Kontakt k = new Kontakt(-1, "Max", "Mustermann", "Testgasse 5", 8467234, "test@email.com");
		try {
			Kontakt saveResult = dal.saveKontakt(k);
			
			ArrayList<Kontakt> kl = dal.getKontaktList();
			
			boolean deleteResult = dal.deleteKontakt(saveResult);
			
		    assertEquals("Wrong return value", k, saveResult);
		    assertEquals("Error in saving", kl.get(0).getEmail(), k.getEmail());
		    assertTrue("Expected non-null result", deleteResult);
		    
		    kl = dal.getKontaktList();
		    
		    assertTrue("Should be empty", kl.isEmpty());
			
		} catch (DALException e) {
			fail("DALException: Error while saving");
		}
	}

	@Test
	public void testAusgangsrechnung() {
		
		try {
			Ausgangsrechnung a = new Ausgangsrechnung(-1, "eine Ausgangsrechnung", -1);
			Ausgangsrechnung saveResult = dal.saveAusgangsrechnung(a);
			
			ArrayList<Ausgangsrechnung> al = dal.getAusgangsrechnungList();
			
			boolean deleteResult = dal.deleteAusgangsrechnung(saveResult);
			
		    assertEquals("Wrong return value", a, saveResult);
		    assertEquals("Error in saving", al.get(0).getName(), a.getName());
		    assertTrue("Expected non-null result", deleteResult);
		    
		    al = dal.getAusgangsrechnungList();
		    
		    assertTrue("Should be empty", al.isEmpty());
			
		} catch (DALException e) {
			Logger.trace(e.getMessage());
			fail("DALException: Error while saving");
			e.printStackTrace();
		}
	}

	@Test
	public void testEingangsrechnung() {
		Eingangsrechnung e = new Eingangsrechnung(-1, "eine Eingangsrechnung", -1);
		try {
			Eingangsrechnung saveResult = dal.saveEingangsrechnung(e);
			
			ArrayList<Eingangsrechnung> el = dal.getEingangsrechnungList();
			
			boolean deleteResult = dal.deleteEingangsrechnung(saveResult);
			
		    assertEquals("Wrong return value", e, saveResult);
		    assertEquals("Error in saving", el.get(0).getName(), e.getName());
		    assertTrue("Expected non-null result", deleteResult);
		    
		    el = dal.getEingangsrechnungList();
		    
		    assertTrue("Should be empty", el.isEmpty());
			
		} catch (DALException ex) {
			fail("DALException: Error while saving");
			ex.printStackTrace();
		}
	}
	
	@Test
	public void testRechnungszeile() {
		
		Angebot a = new Angebot();
		Ausgangsrechnung ar = new Ausgangsrechnung();
		
		
		try {
			
			Angebot sa = dal.saveAngebot(a);
			Ausgangsrechnung sar = dal.saveAusgangsrechnung(ar);
			
			Rechnungszeile r = new Rechnungszeile(-1, "eine Eingangsrechnung", sa.getId(), sar.getId());
			
			Rechnungszeile saveResult = dal.saveRechnungszeile(r);
			
			ArrayList<Rechnungszeile> rl = dal.getRechnungszeileByIdList(sar.getId());
			
			boolean deleteResult = dal.deleteRechnungszeile(saveResult);
			
		    assertEquals("Wrong return value", r, saveResult);
		    assertEquals("Error in saving", rl.get(0).getName(), r.getName());
		    assertTrue("Expected non-null result", deleteResult);
		    
		    rl = dal.getRechnungszeileByIdList(sar.getId());
		    
		    assertTrue("Should be empty", rl.isEmpty());
		    
		    //aufr�umen
		    dal.deleteAngebot(sa);
		    dal.deleteAusgangsrechnung(sar);
			
		} catch (DALException ex) {
			fail("DALException: Error while saving");
			ex.printStackTrace();
		}
	}

	@Test
	public void testBuchungszeile() {
		
		try {
			Buchungszeile b = new Buchungszeile(-1, "eine Eingangsrechnung", 32);
			Buchungszeile saveResult = dal.saveBuchungszeile(b);
			
			ArrayList<Buchungszeile> bl = dal.getBuchungszeileList();
			
			boolean deleteResult = dal.deleteBuchungszeile(saveResult);
			
		    assertEquals("Wrong return value", b, saveResult);
		    assertEquals("Error in saving", bl.get(0).getName(), b.getName());
		    assertTrue("Expected non-null result", deleteResult);
		    
		    bl = dal.getBuchungszeileList();
		    
		    assertTrue("Should be empty", bl.isEmpty());
			
		} catch (DALException ex) {
			fail("DALException: Error while saving");
			ex.printStackTrace();
		}
	}

	@Test
	public void testKategorien() {
		
		try {
			
			Kategorie ka = new Kategorie(-1, "Testkat");
			Kategorie ska = dal.saveKategorie(ka);
			
			ArrayList<Integer> kategorien = new ArrayList<Integer>();
			kategorien.add(ska.getId());
			
			Buchungszeile b = new Buchungszeile(-1, "eine Eingangsrechnung", 32);
			b.setKategorienIds(kategorien);
			
			ArrayList<Kategorie> hatKategorien = dal.getKategorien();
			assertEquals("Wrong Category was saved", hatKategorien.get(0).getName(), "Testkat");
			
			Buchungszeile saveResult = dal.saveBuchungszeile(b);
			
			ArrayList<Buchungszeile> bl = dal.getBuchungszeileList();
			
			boolean deleteResult = dal.deleteBuchungszeile(saveResult);
			
		    assertEquals("Wrong return value", b, saveResult);
		    assertEquals("Error in saving", bl.get(0).getName(), b.getName());
		    assertEquals("Error in saving", ((int)(bl.get(0).getKategorienIds()).get(0)), ska.getId());
		    assertTrue("Expected non-null result", deleteResult);
		    
		    bl = dal.getBuchungszeileList();
		    
		    assertTrue("Should be empty", bl.isEmpty());
			
		} catch (DALException ex) {
			fail("DALException: Error while saving");
			ex.printStackTrace();
		}
	}

	@Test
	public void testBuchungszeileAusgangsrechnung() {
		
		try {
			
			Buchungszeile b = new Buchungszeile(-1, "eine Buchungszeile", 32);
			Buchungszeile sb = dal.saveBuchungszeile(b);
			
			Ausgangsrechnung ar = new Ausgangsrechnung(-1, "eine Ausgangsrechnung", -1);
			Ausgangsrechnung sar = dal.saveAusgangsrechnung(ar);
			
			Ausgangsrechnung sbar = dal.saveBuchungszeileAusgangsrechnung(sb.getId(), sar);

			
			ArrayList<Ausgangsrechnung> bl = dal.getBuchungszeileAusgangsrechnungList(sb.getId());
			
			boolean deleteResult = dal.deleteBuchungszeileAusgangsrechnung(sb.getId(), sar);
			
		    assertEquals("Wrong return value", sar, sbar);
		    assertEquals("Error in saving", bl.get(0).getName(), ar.getName());
		    assertTrue("Expected non-null result", deleteResult);
		    
		    bl = dal.getBuchungszeileAusgangsrechnungList(sb.getId());
		    
		    assertTrue("Should be empty", bl.isEmpty());
		    
		    //Aufr�umen
		    dal.deleteBuchungszeile(sb);
		    dal.deleteAusgangsrechnung(sar);
			
		} catch (DALException ex) {
			fail("DALException: Error while saving");
			ex.printStackTrace();
		}
	}

	@Test
	public void testGetNotUsedAusgangsrechnungList() {
		try {
			
			Buchungszeile b = new Buchungszeile(-1, "eine Buchungszeile", 32);
			Buchungszeile sb = dal.saveBuchungszeile(b);
			
			Ausgangsrechnung ar1 = new Ausgangsrechnung(-1, "eine Ausgangsrechnung", -1);
			Ausgangsrechnung sar1 = dal.saveAusgangsrechnung(ar1);
			
			Ausgangsrechnung ar2 = new Ausgangsrechnung(-1, "eine zweite Ausgangsrechnung", -1);
			Ausgangsrechnung sar2 = dal.saveAusgangsrechnung(ar2);
			
			Ausgangsrechnung sbar = dal.saveBuchungszeileAusgangsrechnung(sb.getId(), sar1);

			
			ArrayList<Ausgangsrechnung> nuar = dal.getNotUsedAusgangsrechnungList(sb.getId());
			
			boolean deleteResult = dal.deleteBuchungszeileAusgangsrechnung(sb.getId(), sar1);
			
			
		    assertEquals("Wrong return value", sar1, sbar);
		    assertEquals("Error in saving", nuar.get(0).getName(), ar2.getName());
		    assertTrue("Expected non-null result", deleteResult);
		    
		    nuar = dal.getBuchungszeileAusgangsrechnungList(sb.getId());
		    
		    assertTrue("Should be empty", nuar.isEmpty());
		    
		    //Aufr�umen
		    dal.deleteBuchungszeile(sb);
		    dal.deleteAusgangsrechnung(sar1);
		    dal.deleteAusgangsrechnung(sar2);
			
		} catch (DALException ex) {
			fail("DALException: Error while saving");
			ex.printStackTrace();
		}
	}

	@Test
	public void testBuchungszeileEingangsrechnung() {
		try {
			
			Buchungszeile b = new Buchungszeile(-1, "eine Buchungszeile", 32);
			Buchungszeile sb = dal.saveBuchungszeile(b);
			
			Eingangsrechnung er = new Eingangsrechnung(-1, "eine Eingangsrechnung", -1);
			Eingangsrechnung ser = dal.saveEingangsrechnung(er);
			
			Eingangsrechnung sber = dal.saveBuchungszeileEingangsrechnung(sb.getId(), ser);

			
			ArrayList<Eingangsrechnung> bl = dal.getBuchungszeileEingangsrechnungList(sb.getId());
			
			boolean deleteResult = dal.deleteBuchungszeileEingangsrechnung(sb.getId(), ser);
			
		    assertEquals("Wrong return value", ser, sber);
		    assertEquals("Error in saving", bl.get(0).getName(), er.getName());
		    assertTrue("Expected non-null result", deleteResult);
		    
		    bl = dal.getBuchungszeileEingangsrechnungList(sb.getId());
		    
		    assertTrue("Should be empty", bl.isEmpty());
		    
		    //Aufr�umen
		    dal.deleteBuchungszeile(sb);
		    dal.deleteEingangsrechnung(ser);
			
		} catch (DALException ex) {
			fail("DALException: Error while saving");
			ex.printStackTrace();
		}
	}

	@Test
	public void testGetNotUsedEingangsrechnungList() {
		try {
			
			Buchungszeile b = new Buchungszeile(-1, "eine Buchungszeile", 32);
			Buchungszeile sb = dal.saveBuchungszeile(b);
			
			Eingangsrechnung er1 = new Eingangsrechnung(-1, "eine Eingangsrechnung", -1);
			Eingangsrechnung ser1 = dal.saveEingangsrechnung(er1);
			
			Eingangsrechnung er2 = new Eingangsrechnung(-1, "eine zweite Eingangsrechnung", -1);
			Eingangsrechnung ser2 = dal.saveEingangsrechnung(er2);
			
			Eingangsrechnung sber = dal.saveBuchungszeileEingangsrechnung(sb.getId(), ser1);

			
			ArrayList<Eingangsrechnung> nuar = dal.getNotUsedEingangsrechnungList(sb.getId());
			
			boolean deleteResult = dal.deleteBuchungszeileEingangsrechnung(sb.getId(), ser1);
			
			
		    assertEquals("Wrong return value", ser1, sber);
		    assertEquals("Error in saving", nuar.get(0).getName(), er2.getName());
		    assertTrue("Expected non-null result", deleteResult);
		    
		    nuar = dal.getBuchungszeileEingangsrechnungList(sb.getId());
		    
		    assertTrue("Should be empty", nuar.isEmpty());
		    
		    //Aufr�umen
		    dal.deleteBuchungszeile(sb);
		    dal.deleteEingangsrechnung(ser1);
		    dal.deleteEingangsrechnung(ser2);
			
		} catch (DALException ex) {
			fail("DALException: Error while saving");
			ex.printStackTrace();
		}
	}

}
