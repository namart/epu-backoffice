package tests;


import static org.junit.Assert.*;
import static org.junit.Assert.assertTrue;

import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import javax.swing.JTextField;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import at.namart.lib.binder.MyBinder;
import at.namart.lib.binder.RequiredRule;
import at.namart.lib.logger.Logger;
import at.namart.model.bo.Angebot;
import at.namart.model.bo.Kontakt;
import at.namart.model.bo.Kunde;
import at.namart.model.bo.Projekt;
import at.namart.model.bo.Zeit;
import at.namart.view.AngebotView;
import at.namart.view.AusgangsrechnungView;
import at.namart.view.BuchungszeileView;
import at.namart.view.EingangsrechnungView;
import at.namart.view.KontaktView;
import at.namart.view.KundeView;
import at.namart.view.ProjektView;
import at.namart.view.ZeiterfassungView;

public class ViewTest {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}
	
	/*---------------------------------------------------------|
	 *---------------------- ProjektView ----------------------|
	 *---------------------------------------------------------|
	 */
	@Test
	public void testProjektViewInputName() {
		
		ProjektView pview = new ProjektView();
		
		Projekt p = new Projekt(-1, "Mein Projekt");
		pview.setComponent(p);		
		boolean result1 = pview.BindFrom();
		assertTrue("should be true", result1);
		
		p.setName("");
		pview.setComponent(p);		
		boolean result2 = pview.BindFrom();
		assertFalse("should be false", result2);
		
		p.setName(null);
		pview.setComponent(p);		
		boolean result3 = pview.BindFrom();
		assertFalse("should be false", result3);
	}
	
	/*--------------------------------------------------------|
	 *------------------- ZeiterfassungView -------------------|
	 *---------------------------------------------------------|
	 */
	@Test
	public void testZeiterfassungViewInputDauer() {
		
		ZeiterfassungView zview = new ZeiterfassungView();
		ArrayList<Projekt> ap = new ArrayList<Projekt>();
		ap.add(new Projekt(1, "Mein Projekt"));
		zview.getFieldProjektCombo().setList(ap);
		zview.getFieldProjektCombo().setSelectedItemById(1);
		zview.setFieldDatum("5/5/1999");

		zview.setFieldDauer("123");
		boolean result1 = zview.BindFrom();
		assertTrue("should be true", result1);
		
		zview.setFieldDauer("");	
		boolean result2 = zview.BindFrom();
		assertFalse("should be false", result2);
		
		zview.setFieldDauer(null);	
		boolean result3 = zview.BindFrom();
		assertFalse("should be false", result3);

	}
	
	@Test
	public void testZeiterfassungViewInputDauerMinus() {
		
		ZeiterfassungView zview = new ZeiterfassungView();
		ArrayList<Projekt> ap = new ArrayList<Projekt>();
		ap.add(new Projekt(1, "Mein Projekt"));
		zview.getFieldProjektCombo().setList(ap);
		zview.getFieldProjektCombo().setSelectedItemById(1);
		zview.setFieldDatum("5/5/1999");

		zview.setFieldDauer("123");
		boolean result1 = zview.BindFrom();
		assertTrue("should be true", result1);
		
		zview.setFieldDauer("");	
		boolean result2 = zview.BindFrom();
		assertFalse("should be false", result2);
		
		zview.setFieldDauer(null);	
		boolean result3 = zview.BindFrom();
		assertFalse("should be false", result3);
		
		zview.setFieldDauer("-1234");	
		boolean result4 = zview.BindFrom();
		assertFalse("should be false", result4);

	}
	
	@Test
	public void testZeiterfassungViewInputDatum() {
		
		ZeiterfassungView zview = new ZeiterfassungView();
		ArrayList<Projekt> ap = new ArrayList<Projekt>();
		ap.add(new Projekt(1, "Mein Projekt"));
		zview.getFieldProjektCombo().setList(ap);
		zview.getFieldProjektCombo().setSelectedItemById(1);
		zview.setFieldDauer("123");
				
		zview.setFieldDatum("5/5/1999");
		
		boolean result1 = zview.BindFrom();
		assertTrue("should be true", result1);
		
		zview.setFieldDatum("");	
		boolean result2 = zview.BindFrom();
		assertFalse("should be false", result2);
		
		zview.setFieldDatum(null);	
		boolean result3 = zview.BindFrom();
		assertFalse("should be false", result3);

	}

	@Test
	public void testZeiterfassungViewInputProjekt() {
		
		ZeiterfassungView zview = new ZeiterfassungView();
		ArrayList<Projekt> ap = new ArrayList<Projekt>();
		ap.add(new Projekt(1, "Mein Projekt"));
		zview.getFieldProjektCombo().setList(ap);
		zview.setFieldDauer("123");
		zview.setFieldDatum("5/5/1999");
		
		zview.getFieldProjektCombo().setSelectedItemById(1);
		
		boolean result1 = zview.BindFrom();
		assertTrue("should be true", result1);
		
		zview.getFieldProjektCombo().setSelectedItem(null);
		boolean result2 = zview.BindFrom();
		assertFalse("should be false", result2);

	}
	
	/*--------------------------------------------------------|
	 *----------------------- KundeView ----------------------|
	 *--------------------------------------------------------|
	 */
	@Test
	public void testKundeViewInputName() {
		
		KundeView kview = new KundeView();
		ArrayList<Kontakt> ak = new ArrayList<Kontakt>();
		Kontakt ko = new Kontakt();
		ko.setId(1);
		ak.add(ko);
		kview.getFieldKontaktCombo().setList(ak);
		kview.setFieldUID("123");	

		kview.setFieldName("Hans Peter");
		boolean result1 = kview.BindFrom();
		assertTrue("should be true", result1);
		
		kview.setFieldName("");	
		boolean result2 = kview.BindFrom();
		assertFalse("should be false", result2);
		
		kview.setFieldName(null);	
		boolean result3 = kview.BindFrom();
		assertFalse("should be false", result3);

	}
	
	@Test
	public void testKundeViewInputUID() {
		
		KundeView kview = new KundeView();
		ArrayList<Kontakt> ak = new ArrayList<Kontakt>();
		Kontakt ko = new Kontakt();
		ko.setId(1);
		ak.add(ko);
		kview.getFieldKontaktCombo().setList(ak);
		kview.setFieldName("Hans Peter");
		
		kview.setFieldUID("123");
		
		boolean result1 = kview.BindFrom();
		assertTrue("should be true", result1);
		
		kview.setFieldUID("");	
		boolean result2 = kview.BindFrom();
		assertFalse("should be false", result2);
		
		kview.setFieldUID(null);	
		boolean result3 = kview.BindFrom();
		assertFalse("should be false", result3);

	}
	
	@Test
	public void testKundeViewInputUIDMinus() {
		
		KundeView kview = new KundeView();
		ArrayList<Kontakt> ak = new ArrayList<Kontakt>();
		Kontakt ko = new Kontakt();
		ko.setId(1);
		ak.add(ko);
		kview.getFieldKontaktCombo().setList(ak);
		kview.setFieldName("Hans Peter");		
		
		kview.setFieldUID("-123");	
		boolean result4 = kview.BindFrom();
		assertFalse("should be false", result4);

	}

	@Test
	public void testKundeViewInputKontakt() {
		
		KundeView kview = new KundeView();
		ArrayList<Kontakt> ak = new ArrayList<Kontakt>();
		Kontakt ko = new Kontakt();
		ko.setId(1);
		ak.add(ko);
		kview.getFieldKontaktCombo().setList(ak);
		kview.setFieldName("Hans Peter");
		kview.setFieldUID("123");
		
		kview.getFieldKontaktCombo().setSelectedItemById(1);
		boolean result1 = kview.BindFrom();
		assertTrue("should be true", result1);

	}
	
	/*--------------------------------------------------------|
	 *---------------------- AngebotView ---------------------|
	 *--------------------------------------------------------|
	 */
	@Test
	public void testAngebotViewInputName() {
		
		AngebotView aview = new AngebotView();
		ArrayList<Projekt> pa = new ArrayList<Projekt>();
		Projekt po = new Projekt();
		po.setId(1);
		pa.add(po);
		ArrayList<Kunde> ka = new ArrayList<Kunde>();
		Kunde ko = new Kunde();
		ko.setId(1);
		ka.add(ko);
		aview.getFieldProjektComboModel().setList(pa);	
		aview.getFieldProjektComboModel().setSelectedItemById(1);
		aview.getFieldKundeComboModel().setList(ka);	
		aview.getFieldKundeComboModel().setSelectedItemById(1);	
		aview.getFieldSummer().setText("123");
		aview.getFieldDauer().setText("32");
		aview.getFieldDatum().setText("6/4/2001");
		aview.getFieldChance().setText("20");
		
		aview.getFieldName().setText("Angebot Name");
		boolean result1 = aview.BindFrom();
		assertTrue("should be true", result1);
		
		aview.getFieldName().setText("");	
		boolean result2 = aview.BindFrom();
		assertFalse("should be false", result2);
		
		aview.getFieldName().setText(null);	
		boolean result3 = aview.BindFrom();
		assertFalse("should be false", result3);

	}
	
	@Test
	public void testAngebotViewInputSumme() {
		
		AngebotView aview = new AngebotView();
		ArrayList<Projekt> pa = new ArrayList<Projekt>();
		Projekt po = new Projekt();
		po.setId(1);
		pa.add(po);
		ArrayList<Kunde> ka = new ArrayList<Kunde>();
		Kunde ko = new Kunde();
		ko.setId(1);
		ka.add(ko);
		aview.getFieldProjektComboModel().setList(pa);	
		aview.getFieldProjektComboModel().setSelectedItemById(1);
		aview.getFieldKundeComboModel().setList(ka);	
		aview.getFieldKundeComboModel().setSelectedItemById(1);	
		aview.getFieldName().setText("Angebot Name");
		aview.getFieldDauer().setText("32");
		aview.getFieldDatum().setText("6/4/2001");
		aview.getFieldChance().setText("20");	
		
		aview.getFieldSummer().setText("123");
		boolean result1 = aview.BindFrom();
		assertTrue("should be true", result1);
		
		aview.getFieldSummer().setText("");	
		boolean result2 = aview.BindFrom();
		assertFalse("should be false", result2);
		
		aview.getFieldSummer().setText("ah123");	
		boolean result4 = aview.BindFrom();
		assertFalse("should be false", result4);
		
		aview.getFieldSummer().setText(null);	
		boolean result3 = aview.BindFrom();
		assertFalse("should be false", result3);

	}
	
	@Test
	public void testAngebotViewInputDauer() {
		
		AngebotView aview = new AngebotView();
		ArrayList<Projekt> pa = new ArrayList<Projekt>();
		Projekt po = new Projekt();
		po.setId(1);
		pa.add(po);
		ArrayList<Kunde> ka = new ArrayList<Kunde>();
		Kunde ko = new Kunde();
		ko.setId(1);
		ka.add(ko);
		aview.getFieldProjektComboModel().setList(pa);	
		aview.getFieldProjektComboModel().setSelectedItemById(1);
		aview.getFieldKundeComboModel().setList(ka);	
		aview.getFieldKundeComboModel().setSelectedItemById(1);	
		aview.getFieldName().setText("Angebot Name");
		aview.getFieldDatum().setText("6/4/2001");
		aview.getFieldChance().setText("20");	
		aview.getFieldSummer().setText("123");
		
		aview.getFieldDauer().setText("1");
		boolean result1 = aview.BindFrom();
		assertTrue("should be true", result1);
		
		aview.getFieldDauer().setText("99999999");
		boolean result2 = aview.BindFrom();
		assertTrue("should be true", result2);
		
		aview.getFieldDauer().setText("");	
		boolean result3 = aview.BindFrom();
		assertFalse("should be false", result3);
		
		aview.getFieldDauer().setText("ah123");	
		boolean result4 = aview.BindFrom();
		assertFalse("should be false", result4);
		
		aview.getFieldDauer().setText(null);	
		boolean result5 = aview.BindFrom();
		assertFalse("should be false", result5);

	}
	
	@Test
	public void testAngebotViewInputDauerMinus() {
		
		AngebotView aview = new AngebotView();
		ArrayList<Projekt> pa = new ArrayList<Projekt>();
		Projekt po = new Projekt();
		po.setId(1);
		pa.add(po);
		ArrayList<Kunde> ka = new ArrayList<Kunde>();
		Kunde ko = new Kunde();
		ko.setId(1);
		ka.add(ko);
		aview.getFieldProjektComboModel().setList(pa);	
		aview.getFieldProjektComboModel().setSelectedItemById(1);
		aview.getFieldKundeComboModel().setList(ka);	
		aview.getFieldKundeComboModel().setSelectedItemById(1);	
		aview.getFieldName().setText("Angebot Name");
		aview.getFieldDatum().setText("6/4/2001");
		aview.getFieldChance().setText("20");	
		aview.getFieldSummer().setText("123");
		
		aview.getFieldDauer().setText("-1231");
		boolean result1 = aview.BindFrom();
		assertFalse("should be true", result1);
		

	}
	
	@Test
	public void testAngebotViewInputDatum() {
		
		AngebotView aview = new AngebotView();
		ArrayList<Projekt> pa = new ArrayList<Projekt>();
		Projekt po = new Projekt();
		po.setId(1);
		pa.add(po);
		ArrayList<Kunde> ka = new ArrayList<Kunde>();
		Kunde ko = new Kunde();
		ko.setId(1);
		ka.add(ko);
		aview.getFieldProjektComboModel().setList(pa);	
		aview.getFieldProjektComboModel().setSelectedItemById(1);
		aview.getFieldKundeComboModel().setList(ka);	
		aview.getFieldKundeComboModel().setSelectedItemById(1);	
		aview.getFieldName().setText("Angebot Name");
		aview.getFieldDauer().setText("23");
		aview.getFieldChance().setText("20");	
		aview.getFieldSummer().setText("123");
		
		
		aview.getFieldDatum().setText("6/4/2001");
		boolean result1 = aview.BindFrom();
		assertTrue("should be true", result1);
		
		aview.getFieldDatum().setText("99999999");
		boolean result2 = aview.BindFrom();
		assertFalse("should be true", result2);
		
		aview.getFieldDatum().setText("");	
		boolean result3 = aview.BindFrom();
		assertFalse("should be false", result3);
		
		aview.getFieldDatum().setText(null);	
		boolean result4 = aview.BindFrom();
		assertFalse("should be false", result4);

	}
	
	@Test
	public void testAngebotViewInputChance() {
		
		AngebotView aview = new AngebotView();
		ArrayList<Projekt> pa = new ArrayList<Projekt>();
		Projekt po = new Projekt();
		po.setId(1);
		pa.add(po);
		ArrayList<Kunde> ka = new ArrayList<Kunde>();
		Kunde ko = new Kunde();
		ko.setId(1);
		ka.add(ko);
		aview.getFieldProjektComboModel().setList(pa);	
		aview.getFieldProjektComboModel().setSelectedItemById(1);
		aview.getFieldKundeComboModel().setList(ka);	
		aview.getFieldKundeComboModel().setSelectedItemById(1);
		aview.getFieldName().setText("Angebot Name");
		aview.getFieldDauer().setText("23");
		aview.getFieldDatum().setText("6/4/2001");	
		aview.getFieldSummer().setText("123");
		
		aview.getFieldChance().setText("0");	
		boolean result1 = aview.BindFrom();
		assertTrue("should be true", result1);
		
		aview.getFieldDatum().setText("50");
		boolean result2 = aview.BindFrom();
		assertFalse("should be true", result2);
		
		aview.getFieldDatum().setText("100");
		boolean result3 = aview.BindFrom();
		assertFalse("should be true", result3);
		
		aview.getFieldDatum().setText("999999");	
		boolean result4 = aview.BindFrom();
		assertFalse("should be false", result4);
		
		aview.getFieldDatum().setText("");	
		boolean result5 = aview.BindFrom();
		assertFalse("should be false", result5);
		
		aview.getFieldDatum().setText(null);	
		boolean result6 = aview.BindFrom();
		assertFalse("should be false", result6);

	}
	
	@Test
	public void testAngebotViewInputProjekt() {
		
		AngebotView aview = new AngebotView();
		ArrayList<Projekt> pa = new ArrayList<Projekt>();
		Projekt po = new Projekt();
		po.setId(1);
		
		ArrayList<Kunde> ka = new ArrayList<Kunde>();
		Kunde ko = new Kunde();
		ko.setId(1);
		ka.add(ko);
		
		aview.getFieldProjektComboModel().setList(pa);	
		aview.getFieldKundeComboModel().setList(ka);	
		aview.getFieldKundeComboModel().setSelectedItemById(1);	
		aview.getFieldName().setText("Angebot Name");
		aview.getFieldDauer().setText("23");
		aview.getFieldDatum().setText("6/4/2001");	
		aview.getFieldSummer().setText("123");
		aview.getFieldChance().setText("0");	
		
		aview.getFieldProjektComboModel().setSelectedItemById(1);
		boolean result1 = aview.BindFrom();
		assertTrue("should be true", result1);
		
		aview.getFieldProjektComboModel().setSelectedItem(null);	
		boolean result2 = aview.BindFrom();
		assertTrue("should be true", result2);

	}
	
	@Test
	public void testAngebotViewInputKunde() {
		
		AngebotView aview = new AngebotView();
		ArrayList<Projekt> pa = new ArrayList<Projekt>();
		Projekt po = new Projekt();
		po.setId(1);
		
		ArrayList<Kunde> ka = new ArrayList<Kunde>();
		Kunde ko = new Kunde();
		ko.setId(1);
		ka.add(ko);
		aview.getFieldProjektComboModel().setList(pa);
		aview.getFieldProjektComboModel().setSelectedItemById(1);
		aview.getFieldKundeComboModel().setList(ka);	
			
		aview.getFieldName().setText("Angebot Name");
		aview.getFieldDauer().setText("23");
		aview.getFieldDatum().setText("6/4/2001");	
		aview.getFieldSummer().setText("123");
		aview.getFieldChance().setText("0");	
		
		aview.getFieldKundeComboModel().setSelectedItemById(1);	
		boolean result1 = aview.BindFrom();
		assertTrue("should be true", result1);
		
		aview.getFieldKundeComboModel().setSelectedItem(null);	
		boolean result2 = aview.BindFrom();
		assertTrue("should be true", result2);

	}
	
	/*--------------------------------------------------------|
	 *---------------------- KontaktView ---------------------|
	 *--------------------------------------------------------|
	 */
	
	@Test
	public void testKontaktViewInputVorname() {
		
		KontaktView kview = new KontaktView();	
		kview.getFieldNachname().setText("Peter");
		kview.getFieldAdresse().setText("Stra�e");
		kview.getFieldEmail().setText("test@test.at");
		kview.getFieldTelefon().setText("4234234");
		
		kview.getFieldVorname().setText("Hans");
		boolean result1 = kview.BindFrom();
		assertTrue("should be true", result1);
		
		kview.getFieldVorname().setText("");	
		boolean result2 = kview.BindFrom();
		assertFalse("should be false", result2);
		
		kview.getFieldVorname().setText(null);	
		boolean result3 = kview.BindFrom();
		assertFalse("should be false", result3);

	}
	
	@Test
	public void testKontaktViewInputNachname() {
		
		KontaktView kview = new KontaktView();	
		kview.getFieldVorname().setText("Hans");
		kview.getFieldAdresse().setText("Stra�e");
		kview.getFieldEmail().setText("test@test.at");
		kview.getFieldTelefon().setText("4234234");
				
		kview.getFieldNachname().setText("Peter");
		boolean result1 = kview.BindFrom();
		assertTrue("should be true", result1);
		
		kview.getFieldNachname().setText("");	
		boolean result2 = kview.BindFrom();
		assertFalse("should be false", result2);
		
		kview.getFieldNachname().setText(null);	
		boolean result3 = kview.BindFrom();
		assertFalse("should be false", result3);

	}
	
	@Test
	public void testKontaktViewInputAdresse() {
		
		KontaktView kview = new KontaktView();	
		kview.getFieldVorname().setText("Hans");
		kview.getFieldNachname().setText("Peter");
		kview.getFieldEmail().setText("test@test.at");
		kview.getFieldTelefon().setText("4234234");
				
		kview.getFieldAdresse().setText("Stra�e");
		boolean result1 = kview.BindFrom();
		assertTrue("should be true", result1);
		
		kview.getFieldAdresse().setText("");	
		boolean result2 = kview.BindFrom();
		assertFalse("should be false", result2);
		
		kview.getFieldAdresse().setText(null);	
		boolean result3 = kview.BindFrom();
		assertFalse("should be false", result3);

	}
	
	@Test
	public void testKontaktViewInputTelefon() {
		
		KontaktView kview = new KontaktView();	
		kview.getFieldVorname().setText("Hans");
		kview.getFieldNachname().setText("Peter");
		kview.getFieldEmail().setText("test@test.at");
		kview.getFieldAdresse().setText("Stra�e");
					
		kview.getFieldTelefon().setText("4234234");
		boolean result1 = kview.BindFrom();
		assertTrue("should be true", result1);
		
		kview.getFieldTelefon().setText("");	
		boolean result2 = kview.BindFrom();
		assertFalse("should be false", result2);
		
		kview.getFieldTelefon().setText(null);	
		boolean result3 = kview.BindFrom();
		assertFalse("should be false", result3);
		
		kview.getFieldTelefon().setText("-1");	
		boolean result4 = kview.BindFrom();
		assertFalse("should be false", result4);
		
		//weniger als 5 zahlen
		kview.getFieldTelefon().setText("1234");	
		boolean result5 = kview.BindFrom();
		assertFalse("should be false", result5);

	}
	
	@Test
	public void testKontaktViewInputEmail() {
		
		KontaktView kview = new KontaktView();	
		kview.getFieldVorname().setText("Hans");
		kview.getFieldNachname().setText("Peter");
		kview.getFieldTelefon().setText("4234234");
		kview.getFieldAdresse().setText("Stra�e");
						
		kview.getFieldEmail().setText("test@test.at");
		boolean result1 = kview.BindFrom();
		assertTrue("should be true", result1);
		
		kview.getFieldEmail().setText("");	
		boolean result2 = kview.BindFrom();
		assertFalse("should be false", result2);
		
		kview.getFieldEmail().setText(null);	
		boolean result3 = kview.BindFrom();
		assertFalse("should be false", result3);
		
		kview.getFieldEmail().setText("asd@asd");	
		boolean result4 = kview.BindFrom();
		assertFalse("should be false", result4);
		
		kview.getFieldEmail().setText("asd.asd");	
		boolean result5 = kview.BindFrom();
		assertFalse("should be false", result5);
		
		kview.getFieldEmail().setText("asdaf");	
		boolean result6 = kview.BindFrom();
		assertFalse("should be false", result6);

	}
	
	/*--------------------------------------------------------|
	 *------------------ AusgangsrechnungView ----------------|
	 *--------------------------------------------------------|
	 */
	@Test
	public void testAusgangsrechnungViewInputName() {
		
		AusgangsrechnungView aview = new AusgangsrechnungView();
		
		ArrayList<Kunde> ka = new ArrayList<Kunde>();
		Kunde ko = new Kunde();
		ko.setId(1);
		ka.add(ko);
		aview.getFieldKundeComboModel().setList(ka);
		aview.getFieldKundeComboModel().setSelectedItemById(1);

		aview.getFieldName().setText("Eine Ausgangsrechnung");
		boolean result1 = aview.BindFrom();
		assertTrue("should be true", result1);
		
		aview.getFieldName().setText("");
		boolean result2 = aview.BindFrom();
		assertFalse("should be false", result2);
		
		aview.getFieldName().setText(null);	
		boolean result3 = aview.BindFrom();
		assertFalse("should be false", result3);

	}
	
	@Test
	public void testAusgangsrechnungViewInputKunde() {
		
		AusgangsrechnungView aview = new AusgangsrechnungView();
		aview.getFieldName().setText("Eine Ausgangsrechnung");
		ArrayList<Kunde> ka = new ArrayList<Kunde>();
		Kunde ko = new Kunde();
		ko.setId(1);
		ka.add(ko);
		aview.getFieldKundeComboModel().setList(ka);
		

		aview.getFieldKundeComboModel().setSelectedItemById(1);
		boolean result1 = aview.BindFrom();
		assertTrue("should be true", result1);
		
		aview.getFieldKundeComboModel().setSelectedItem(null);
		boolean result2 = aview.BindFrom();
		assertTrue("should be true", result2);

	}
	
	/*--------------------------------------------------------|
	 *------------------ EingangsrechnungView ----------------|
	 *--------------------------------------------------------|
	 */
	@Test
	public void testEingangsrechnungViewInputName() {
		
		EingangsrechnungView aview = new EingangsrechnungView();
		
		ArrayList<Kontakt> ka = new ArrayList<Kontakt>();
		Kontakt ko = new Kontakt();
		ko.setId(1);
		ka.add(ko);
		aview.getFieldKontaktComboModel().setList(ka);
		aview.getFieldKontaktComboModel().setSelectedItemById(1);

		aview.getFieldName().setText("Eine Eingangsrechnung");
		boolean result1 = aview.BindFrom();
		assertTrue("should be true", result1);
		
		aview.getFieldName().setText("");
		boolean result2 = aview.BindFrom();
		assertFalse("should be false", result2);
		
		aview.getFieldName().setText(null);	
		boolean result3 = aview.BindFrom();
		assertFalse("should be false", result3);

	}
	
	@Test
	public void testEingangsrechnungViewInputKontakt() {
		
		EingangsrechnungView aview = new EingangsrechnungView();
		
		ArrayList<Kontakt> ka = new ArrayList<Kontakt>();
		Kontakt ko = new Kontakt();
		ko.setId(1);
		ka.add(ko);
		aview.getFieldKontaktComboModel().setList(ka);
		aview.getFieldName().setText("Eine Eingangsrechnung");
	
		aview.getFieldKontaktComboModel().setSelectedItemById(1);
		boolean result1 = aview.BindFrom();
		assertTrue("should be true", result1);
		
		aview.getFieldKontaktComboModel().setSelectedItem(null);
		boolean result2 = aview.BindFrom();
		assertTrue("should be false", result2);

	}
	
	/*--------------------------------------------------------|
	 *------------------- BuchungszeilenView -----------------|
	 *--------------------------------------------------------|
	 */
	@Test
	public void testBuchungszeileViewInputName() {
		
		BuchungszeileView bview = new BuchungszeileView();
		bview.getFieldBetrag().setText("123");
		
		bview.getFieldName().setText("Eine Eingangsrechnung");
		boolean result1 = bview.BindFrom();
		assertTrue("should be true", result1);
		
		bview.getFieldName().setText("");
		boolean result2 = bview.BindFrom();
		assertFalse("should be false", result2);

	}
	
	@Test
	public void testBuchungszeileViewInputBetrag() {
		
		BuchungszeileView bview = new BuchungszeileView();
		bview.getFieldName().setText("Eine Eingangsrechnung");
		
		bview.getFieldBetrag().setText("123");
		boolean result1 = bview.BindFrom();
		assertTrue("should be true", result1);
		
		bview.getFieldBetrag().setText("");
		boolean result2 = bview.BindFrom();
		assertFalse("should be false", result2);

	}
	
}
